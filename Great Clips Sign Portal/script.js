(function ($) {
    $(document).ready(function () {
        $('#hamburger-button-portal').on('click', function () {
            $('.nav-list').toggleClass('active', 150, 'easeOutSine');
            $('#hamburger-button-portal').toggleClass('active', 10, 'easeOutSine', function () {
                $('body').css({ 'overflow': 'auto' });
            });
        });
    });
})(jQuery);