//#region signsSite object
/*jslint browser: true, devel: true, continue: true, sub: false, vars: true, white: true, plusplus: true, newcap: false, eqeq: false, nomen: true, unparam: true */
/*global jQuery, _gaq */
var openTrigger;
var globalSource;
var signsSite = (function ($) {
    'use strict';
    var typingTimer = null;
    var typingTimeout = 1000;
    var native_width = 0;
    var native_height = 0;
    var openTrigger;
    var addedOverlayTimeout;
    var globalSource = null;

    var app = {
        cart: null,
        checkout: null, //this is set up in /Content/assets/scripts/checkout.js
        clickTimer: {
            timer: null,
            timeoutms: 500
        },
        customerServiceNumber: null,
        designTool: null, //this is set up in /Content/assets/designLib/js/signsDesignTool.js
        headerHeight: null,
        //#region Functions

        highlightText: function (sourceText, highlightString) {
            highlightString = highlightString.replace(/(\s+)/, "(<[^>]+>)*$1(<[^>]+>)*");
            var pattern = new RegExp("(" + highlightString + ")", "gi");

            sourceText = sourceText.replace(pattern, "<mark>$1</mark>");
            sourceText = sourceText.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/, "$1</mark>$2<mark>$4");

            return sourceText;
        },



        //#region init
        //everything in here runs on document ready on EVERY page of the site!
        //some of this should be cleaned up and moved to minimized bundles only on pages where it is needed.
        init: function () {
            app.headerHeight = $('#staticHeader').outerHeight();

            app.initAjaxSettings();
            app.templateSearchManager.init();
            app.initEventBindings();
            app.initFancyBox();
            app.initPageComponents();
            app.slider.init();
            app.mediaMatch.init();
            app.prompts.init();
            app.addressModals.init();

            //Creates the autocomplete dropdown with an on change event
            $(".chosen-select").chosen().on('change', function (event) {
                var selectedSignType = event.currentTarget.selectedOptions["0"].innerText;

                $('.chosen-single').find("span").each(function () {
                    this.innerHTML = selectedSignType;
                });
            });

            //this really only exists to contain code that exists on both home pages...it could probably be moved.
            app.initHomePage();
            //check what device the page loaded in
            $('#deviceType').html(app.deviceType());

            //move hamburger menu down if impersonation warning is visible
            if ($('#hamburger-button').is(':visible') && $('#userImpersonationWarning').is(':visible')) {
                if ($(window).width() <= 1060 && $(window).width() > 768) {
                    $('#userImpersonationWarning').css('height', '170px');
                    $('.main-nav').css('top', '185px');
                    $('#hamburger-button').css('top', '185px');
                    $('#nav-items-container').css('top', '220px');
                }
                if ($(window).width() <= 768) {
                    $('#userImpersonationWarning').css('height', '170px');
                    $('.main-nav').css('top', '185px');
                }

            }

        },

        initAjaxSettings: function () {
            // Setup CSRF safety for AJAX:
            $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
                if (options.type.toUpperCase() === "POST") {
                    // We need to add the verificationToken to all POSTs
                    var token = $("input[name^=__RequestVerificationToken]").first();
                    if (!token.length) {
                        return;
                    }

                    var tokenName = token.attr("name");

                    // If the data is JSON, then we need to put the token in the QueryString:
                    if (!options.contentType || options.contentType.indexOf('application/json') === 0) {
                        // Add the token to the URL, because we can't add it to the JSON data:
                        options.url += ((options.url.indexOf("?") === -1) ? "?" : "&") + token.serialize();
                    } else if (!options.data) {
                        options.data = token.serialize();
                    } else if (typeof options.data === 'string' && options.data.indexOf(tokenName) === -1) {
                        // Append to the data string:
                        options.data += (options.data ? "&" : "") + token.serialize();
                    }
                }
            });
        },

        initHomePage: function () {

        },
        deviceType: function () {
            var Return_Device;
            if (/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile|w3c|acs\-|alav|alca|amoi|audi|avan|benq|bird|blac|blaz|brew|cell|cldc|cmd\-|dang|doco|eric|hipt|inno|ipaq|java|jigs|kddi|keji|leno|lg\-c|lg\-d|lg\-g|lge\-|maui|maxo|midp|mits|mmef|mobi|mot\-|moto|mwbp|nec\-|newt|noki|palm|pana|pant|phil|play|port|prox|qwap|sage|sams|sany|sch\-|sec\-|send|seri|sgh\-|shar|sie\-|siem|smal|smar|sony|sph\-|symb|t\-mo|teli|tim\-|tosh|tsm\-|upg1|upsi|vk\-v|voda|wap\-|wapa|wapi|wapp|wapr|webc|winw|winw|xda|xda\-) /i.test(navigator.userAgent)) {
                if (/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i.test(navigator.userAgent)) {
                    Return_Device = 'Tablet';
                }
                else {
                    Return_Device = 'Smartphone';
                }
            }
            else if (/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i.test(navigator.userAgent)) {
                Return_Device = 'Tablet';
            }
            else {
                Return_Device = 'Desktop';
            }

            return Return_Device;
        },

        initPageComponents: function () {
            var tabs = $("#my-account-tabs");
            if (tabs.length) {
                tabs.tabs();
            }

            if ($("#testimonialWidget").length) {
                app.loadTestimonial();
            }

            if (!window.ChatButton0 || window.ChatButton0.isClosed) {
                $('#helpHeaderLink').hide();
            } else {
                $('#helpHeaderLink').show();
                //$('#contactUsHeaderLink').hide();
            }

            //hide quickstart pricing for cut vinyl products
            var form = $('div.product-quick-start-form-container');
            //if (form != undefined && form !== null) {
            //    var productID = form.data('productid');
            //    if (productID == 39 || productID == 61 || productID == 74 || productID == 88 || productID == 91) {
            //        if (productID == 39) { //vinyl lettering
            //            $(".quickStartSizeLabel").html("Create your own custom vinyl lettering in seconds using our online design tool.");
            //        }
            //        else if (productID == 61) { //wall quote
            //            $(".quickStartSizeLabel").html("Create your own custom wall quote in seconds using our design tool.");
            //        }
            //        else if (productID == 74) { //boat lettering
            //            $(".quickStartSizeLabel").html("Create your own custom boat lettering in seconds using our design tool.");
            //        }
            //        else if (productID == 88) { //car lettering
            //            $(".quickStartSizeLabel").html("Create your own custom car and truck lettering in seconds using our design tool.");
            //        }
            //        else if (productID == 91) { //car lettering
            //            $(".quickStartSizeLabel").html("Create your own custom fleet lettering in seconds using our design tool.");
            //        }

            //        $(".quantityLabel").hide();

            //        $(".availableDateLabel").html("Want it:");
            //        $("#widthSizeInput-2").hide();
            //        $("#heightSizeInput-2").hide();
            //        $(".sizeInput.qty").hide();
            //        //$(".sizeInput").hide();
            //        $(".input-group").hide();
            //        $(".sizeInput").css('display', 'none !important');
            //        $(".priceLabel").hide();
            //        // $(".unitPrice").hide();
            //        $(".unitPrice").parent().hide();
            //        $(".quick-start-group.quick-start-qty").hide();
            //        $('.dropdown.ddl-dynamic-app-homeStandardSizes').hide();
            //    }
            //}

            app.nextDayShippingBadge.init();
        },

        initFancyBox: function () {
            var dsLinks = $("a.dsLink");
            if (dsLinks.length) {
                dsLinks.fancybox({
                    'autoSize': true,
                    'openEffect': 'elastic',
                    'closeEffect': 'elastic',
                    'openSpeed': 500,
                    'closeSpeed': 100,
                    'padding': 0,
                    'afterShow': function () {
                        $("#fancybox-wrap").mousedown(function () {
                            return false;
                        });
                    },
                    helpers: {
                        overlay: {
                            locked: false
                        }
                    }
                });
            }

            var cartProofLinks = $("a.cartProofLink");
            if (cartProofLinks.length) {
                cartProofLinks.fancybox({
                    'autoSize': false,
                    'openEffect': 'elastic',
                    'closeEffect': 'elastic',
                    'openSpeed': 500,
                    'closeSpeed': 100,
                    'padding': 0,
                    'afterShow': function () {
                        $('<div class="large"></div>').appendTo('#fancybox-content');
                        $("#fancybox-wrap").mousedown(function () {
                            return false;
                        });
                    },
                    helpers: {
                        overlay: {
                            locked: false
                        }
                    }
                });
            }

            var pwdChangeLink = $("a#pwdChangeLink");
            if (pwdChangeLink.length) {
                pwdChangeLink.fancybox({
                    'autoSize': false,
                    'openEffect': 'elastic',
                    'closeEffect': 'elastic',
                    'openSpeed': 500,
                    'closeSpeed': 100,
                    'scrolling': 'no',
                    'padding': '0',
                    'width': 340,
                    'height': 350,
                    'closeBtn': false,

                    'afterClose': function () {
                        $("#passwordChangeForm").show();
                        $("#passwordChangeForm")[0].reset();
                        $("#passwordRequestSaved").hide();
                    },
                    helpers: {
                        title: null
                    },
                });
            }

            var loginLink = $("a#loginLink");
            if (loginLink.length) {
                loginLink.fancybox({
                    'autoSize': false,
                    'width': 340,
                    'height': 400,
                    'closeBtn': false,
                    'scrolling': 'no',
                    'padding': '0',

                    helpers: {
                        title: null,
                    },
                });
            }

            var loginLink_RegisterPage = $("a#loginLink_RegisterPage");
            if (loginLink_RegisterPage.length) {
                loginLink_RegisterPage.fancybox({
                    'autoSize': false,
                    'width': 340,
                    'height': 400,
                    'closeBtn': false,
                    'scrolling': 'no',
                    'padding': '0',

                    helpers: {
                        title: null,
                    },
                });
            }

            var createAnAccountLink = $("a#createAnAccountLink");
            if (createAnAccountLink.length) {
                createAnAccountLink.fancybox({
                    'autoSize': false,
                    'width': 340,
                    'height': 640,
                    'closeBtn': false,
                    'scrolling': 'no',
                    'padding': '0',

                    helpers: {
                        title: null,
                    },

                    afterClose: function () {
                        jQuery("[data-country-code=us]").trigger('click'); // this closes the phone country dropdown when the fancybox closed
                    }
                });
            }

            var createAnAccountLinkPortal = $("a#createAnAccountLinkPortal");
            if (createAnAccountLinkPortal.length) {
                createAnAccountLinkPortal.fancybox({
                    'autoSize': false,
                    'width': 340,
                    'height': 640,
                    'closeBtn': false,
                    'scrolling': 'no',
                    'padding': '0',

                    helpers: {
                        title: null,
                    },
                });
            }

            var createAnAccountLink_LoginPage = $("a#createAnAccountLink_LoginPage");
            if (createAnAccountLink_LoginPage.length) {
                createAnAccountLink_LoginPage.fancybox({
                    'autoSize': false,
                    'width': 340,
                    'height': 640,
                    'closeBtn': false,
                    'scrolling': 'no',
                    'padding': '0',

                    helpers: {
                        title: null,
                    },
                });
            }

            var forgotPasswordLink = $("a#forgotPasswordLink");
            if (forgotPasswordLink.length) {
                forgotPasswordLink.fancybox({
                    'autoSize': false,
                    'width': 340,
                    'height': 280,
                    'closeBtn': false,
                    'scrolling': 'no',
                    'padding': '0',
                    'beforeShow': function () {
                        $("#resetPasswordForm").show();
                        $("#resetPasswordSaved").hide();
                    },
                    helpers: {
                        title: null,
                    },
                });
            }
        },

        initEventBindings: function () {
            $(document)
            //.on('click', '.addToCartNoRedirect', function (event) {
            //    addSavedDesignToCart: function (id) {
            //        app.spinner.show({ container: '#saved-designs' });
            //        $.ajax({
            //            url: '/design/addToCart',
            //            type: 'POST',
            //            data: { designID: id, quantity: 1, checkDefaultOptions: true },
            //            success: function (data) {

            //                signsSite.updateCartCount();
            //                location.href = "/shopping-cart/";
            //            }
            //        });
            //    }
            //})
                .on('change', '.countryDDL', function (event) {
                    app.getStatesDDL(this);
                })
                .on('submit', '.request-design-form', function (event) {
                    event.preventDefault();
                    var form = this;
                    var messageContainer = $(form).next('div.messageContainer');

                    var formData = $(form).serialize();
                    var selectedOptions = "";
                    // signsSite.spinner.show({ delay: 1, timeOut: 90000, container: '#designServicesFormContainer', text: 'Submitting form...' });
                    $('option:selected', '.optionConfigurationDDL').each(function (i, o) {
                        selectedOptions += ($(o).val()) + ",";
                    });
                    // alert(selectedOptions);
                    formData += '&SelectedOptions=' + selectedOptions;

                    app.spinner.show({ delay: 1, timeOut: 90000, container: $(form).closest('.request-design-form'), text: 'Submitting form...' });
                    $.post(form.action, formData, function (data) {
                        app.spinner.hide();

                        if (data.status === "Success") {
                            $(form).hide();
                            messageContainer.show();
                            $('.form-success', messageContainer).show();
                        } else if (data.status === "Error") {
                            $(form).hide();
                            messageContainer.show();
                            $('.form-fail', messageContainer).show();
                        } else if (data.status === "Invalid") {
                            $('div.error', form).empty();
                            $('.input-validation-error', form).removeClass('input-validation-error');

                            for (var errorName in data.errors) {
                                if (data.errors.hasOwnProperty(errorName)) {
                                    var modelStateError = data.errors[errorName];
                                    var input = $('input[name="' + modelStateError['Key'] + '"]', form);
                                    var errorString = modelStateError['Value'][0];

                                    input.addClass('input-validation-error');
                                    input.prev('div.error').html(errorString);
                                }
                            }
                        }
                    });
                })
                .on('submit', '#contactForm', function (event) {
                    event.preventDefault();
                    var form = this;
                    var messageContainer = $(form).next('div.messageContainer');

                    var formData = $(form).serialize();


                    app.spinner.show({ delay: 1, timeOut: 90000, container: $(form).closest('#contactFormContainer'), text: 'Submitting form...' });
                    $.post(form.action, formData, function (data) {
                        app.spinner.hide();

                        if (data.status === "Success") {
                            $(form).hide();
                            messageContainer.show();
                            $('.form-success', messageContainer).show();
                        } else if (data.status === "Error") {
                            $(form).hide();
                            messageContainer.show();
                            $('.form-fail', messageContainer).show();
                        } else if (data.status === "Invalid") {
                            $('div.error', form).empty();
                            $('.input-validation-error', form).removeClass('input-validation-error');

                            for (var errorName in data.errors) {
                                if (data.errors.hasOwnProperty(errorName)) {
                                    var modelStateError = data.errors[errorName];
                                    var input = $('input[name="' + modelStateError['Key'] + '"]', form);
                                    var errorString = modelStateError['Value'][0];

                                    input.addClass('input-validation-error');
                                    input.prev('div.error').html(errorString);
                                }
                            }
                        }
                    });
                })
                .on('submit', '#resetPasswordForm', function (event) {
                    event.preventDefault();

                    var resetPasswordForm = $(event.currentTarget);
                    var email = $('#SignsUser_Email2', resetPasswordForm).val();

                    signsSite.spinner.show({ text: 'Generating password reset request.  Please wait.<br/>&nbsp;<br/>', delay: 1 });

                    $.ajax({
                        type: 'POST',
                        url: resetPasswordForm.attr("action"),
                        data: { email: $('#SignsUser_Email2', resetPasswordForm).val() },
                        success: function (data) {
                            if (data.error) {
                                var message = data.message ? data.message : 'An error occured while attempting to generate the password reset request. Please try again. If the problem persists, please contact us at ' + app.customerServiceNumber + '.';

                                app.prompts.alert(message);
                            }
                            else {
                                resetPasswordForm.hide();
                                $("#resetPasswordSaved").show();
                            }
                        },
                        complete: function () {
                            signsSite.spinner.hide();
                        }
                    });
                })

                .on('submit', '#registrationForm', function (event) {
                    event.preventDefault();

                    var registrationForm = $(event.currentTarget);

                    $('#formErrors').hide();
                    $('#formErrors').html();
                    var url = window.location.href;

                    signsSite.spinner.show({ container: '#registrationInnerDiv', text: 'Creating your account.  Please wait.<br/>&nbsp;<br/>', delay: 1 });

                    $.ajax({
                        type: 'POST',
                        url: registrationForm.attr("action"),
                        data: $(registrationForm).serialize(),
                        success: function (data) {
                            if (data.status == "Error") {
                                var message = data.message ? data.message : 'An error occured while attempting to create your account Please try again. If the problem persists, please contact us at ' + app.customerServiceNumber + '.';

                                $('#formErrors').html(message);
                                $('#formErrors').show();
                            }
                            else {
                                $('#formErrors').hide();
                                $('#formErrors').html();

                                location.reload(true);


                            }
                        },
                        complete: function () {
                            signsSite.spinner.hide();
                        }
                    });
                })


                .on('submit', '#widgetRegistrationForm', function (event) {
                    event.preventDefault();

                    var registrationForm = $(event.currentTarget);

                    $('#widgetFormErrors').hide();
                    $('#widgetFormErrors').html();
                    var url = window.location.href;

                    var hadErrors = false;

                    signsSite.spinner.show({ container: '#widgetRegistrationInnerDiv', text: 'Creating your account.  Please wait.<br/>&nbsp;<br/>', delay: 1 });

                    $.ajax({
                        type: 'POST',
                        url: registrationForm.attr("action"),
                        data: $(registrationForm).serialize(),
                        success: function (data) {
                            if (data.status == "Error") {
                                var message = data.message ? data.message : 'An error occured while attempting to create your account Please try again. If the problem persists, please contact us at ' + app.customerServiceNumber + '.';

                                $('#widgetFormErrors').html(message);
                                $('#widgetFormErrors').show();
                                hadErrors = true;
                            }
                            else {
                                $('#widgetFormErrors').hide();
                                $('#widgetFormErrors').html();

                                // location.reload(true);


                            }
                        },
                        complete: function () {
                            signsSite.spinner.hide();
                            if (hadErrors == false) {
                                if ('referrer' in document) {
                                    if (document.referrer.length > 1) {
                                        window.location.href = document.referrer;
                                    }
                                    else {
                                        window.location.href = '/';
                                    }

                                } else {
                                    window.history.back();

                                }
                            }


                        }
                    });
                })



                .on('click', '#savedDesignsRepeater .showReverseSide', function (event) {
                    var designId = $(this).attr('data-designid');
                    var designRowClass = designId + '_designImage';
                    var frontProof = designId + '_frontProof';
                    var backProof = designId + '_backProof';
                    //designRowClass = 'DesignImage.' + designRowClass;
                    $('#' + designRowClass).quickFlipper();
                    $('.front').css({ position: '' })
                    $('.back').css({ position: '' })
                    $('.front').css({ height: '' })
                    $('.back').css({ height: '' })
                    if ($(this).text() == "back side") {
                        $(this).text("front side");
                        $('#' + frontProof).addClass('hidden');
                        $('#' + backProof).removeClass('hidden');
                    }
                    else {
                        $(this).text("back side");
                        $('#' + backProof).addClass('hidden');
                        $('#' + frontProof).removeClass('hidden');
                    }
                })
                .on('click', '#savedDesignsRepeater .discount', function (e) {
                    app.myAccount.savedDesigns.quantity.update(e);
                })
                .on('click', '#savedDesignsRepeater .spinBtn', function (e) {
                    var elem = $(e.target),
                        row = elem.closest('.Row'),
                        designId = row.data('designid'),
                        txtBox = $('#' + designId + '_qty', row),
                        revert = txtBox.data('revert'),
                        quantity;

                    var prev = txtBox.val();

                    if (elem.is('.up')) {
                        quantity = parseInt(txtBox.val(), 10) + 1;
                    } else if (elem.is('.down')) {
                        quantity = parseInt(txtBox.val(), 10) - 1;
                    }

                    if (quantity < 1 || isNaN(quantity)) {
                        txtBox.val(prev);
                    } else {
                        txtBox.val(quantity);
                    }

                    clearTimeout(app.clickTimer.timer);
                    app.clickTimer.timer = setTimeout(function () {
                        app.myAccount.savedDesigns.quantity.update(e);
                    }, app.clickTimer.timeoutms);
                })
                .on('keydown', '#savedDesignsRepeater .txtQuantity', function (event) {
                    if (event.which == 13 || event.which == 9) {
                        app.myAccount.savedDesigns.quantity.update(event);
                    }
                })
                .on('change', '#savedDesignsRepeater .cartQuantityDropDown', function (e) {
                    var elem = $(e.target),
                        row = elem.closest('.Row'),
                        designId = row.data('designid'),
                        txtBox = $('#' + designId + '_qty', row),
                        revert = txtBox.data('revert'),
                        quantity = $(this).val();

                    txtBox.val(quantity);
                    app.myAccount.savedDesigns.quantity.update(e);

                })

                .on('click', '#requestPageAccess', function (event) {

                    var pageID = $("#pageID").val();

                    $.ajax({
                        type: 'POST',
                        url: '/Pages/RequestPageAccess/',
                        data: {
                            pageId: pageID
                        },

                        success: function (data) {

                            if (data.success) {
                                $('#requestAccessMessage').hide();
                                $('#requestAccessSuccess').show();
                            }
                        }
                    });

                })

                .on('change', '#uploadpfkPrint', function (e) {
                    //don't delete this.  It is being used by the prints-for-kids dynamic order page.
                    var file = this.files[0];
                    var fileName = file.name;
                    var fd = new FormData();
                    fd.append('UploadedImage', file);
                    $.ajax({
                        type: 'post',
                        url: '/Default/UploadPrintsForKidsImage',
                        data: fd,
                        //cache: false,
                        //enctype: 'multipart/form-data',
                        success: function (data) {
                            $('#uploadedFile').html(fileName).show();
                        },
                        xhrFields: {
                            // add listener to XMLHTTPRequest object directly for progress (jquery doesn't have this yet)
                            onprogress: function (progress) {
                                // calculate upload progress
                                var percentage = Math.floor((progress.total / progress.totalSize) * 100);
                                // log upload progress to console
                                console.log('progress', percentage);
                                if (percentage === 100) {
                                    console.log('DONE!');
                                }
                            }
                        },
                        processData: false,
                        contentType: false
                    });
                })
                .on('keyup', '#savedDesignsRepeater .txtQuantity', function (event) {
                    var txtBox = $(event.target),
                        revert = txtBox.data('revert'),
                        quantity;

                    if (txtBox.val() != '') { //allow it to be empty but do nothing (ie backspace ver qty to enter new number)
                        quantity = parseInt(txtBox.val());

                        if (quantity < 1 || isNaN(quantity)) {
                            txtBox.val(revert);
                        } else {
                            clearTimeout(typingTimer);
                            typingTimer = setTimeout(function () {
                                app.myAccount.savedDesigns.quantity.update(event);
                            }, typingTimeout);
                        }
                    }
                })
                .on('change', '#checkAllAccountItems', function (event) {
                    if ($(this).prop("checked")) $('#savedDesignsRepeater input:checkbox').prop('checked', true);
                    else $('#savedDesignsRepeater input:checkbox').prop('checked', false);
                })
                .on('change', '#checkAllCartItems', function (event) {
                    if ($(this).prop("checked")) $('#cartItemsTableContainer input:checkbox').prop('checked', true);
                    else $('#cartItemsTableContainer input:checkbox').prop('checked', false);
                })
                .on('change', '#cartItemsTableContainer input:checkbox', function (event) {
                    if ($('#cartItemsTableContainer input:checked').length > 0) {
                        $('#removeSelectedCartItems').show();
                    } else {
                        $('#removeSelectedCartItems').hide();
                    }
                })
                .on('change', '#checkAllOrderItems', function (event) {
                    if ($(this).prop("checked")) $('#orderHistoryContainer input:checkbox').prop('checked', true);
                    else $('#orderHistoryContainer input:checkbox').prop('checked', false);
                })

                .on('click', '.bulkActions', function (e) {
                    var items = $(this).closest('div').find('.bulkActionItems');
                    items.toggle()

                })

                .on('click', '.rowCheck', function (e) {
                    if ($('#checkAllAccountItems').prop("checked")) {
                        $('#checkAllAccountItems').prop("checked", false);
                    }
                })

                .on('click', '.orderRowCheck', function (e) {
                    if ($('#checkAllOrderItems').prop("checked")) {
                        $('#checkAllOrderItems').prop("checked", false);
                    }
                })



                .mouseup(function (e) {
                    var container = $('.bulkActionItems');
                    var header = $('.bulkActions');
                    if (!container.is(e.target) && !header.is(e.target)) // if the target of the click isn't the container...
                    //&& container.has(e.target).length === 0) // ... nor a descendant of the container
                    {
                        container.hide();
                    }

                })

                .on('click', '.addOrderHistorySelected', function (e) {
                    var selectedDesign;
                    var designIDs = [];
                    $("#orderHistoryContainer input:checked").each(function () {
                        var id = $(this).data('dbid');
                        //designIDs.push({ UserDesignID: id });
                        if ($(this).is(':visible')) {
                            designIDs.push(id);
                        }
                        //alert("Do something for: " + id + ", " + answer);
                    });

                    if (designIDs.length < 1)
                        return;

                    app.spinner.show({ container: '#orderHistoryContainer' });
                    $.ajax(
                        {
                            type: "POST",
                            url: "/myAccount/BulkAddToCartFromOrderDetail/",
                            data: JSON.stringify({ designIDs: designIDs }),
                            contentType: 'application/json',
                            success: function (data) {
                                signsSite.updateCartCount();
                                location.href = data.redirectUrl;
                            }
                        });
                })

                .on('click', '.addSelected', function (e) {
                    var selectedDesign;
                    var designIDs = [];
                    $("#savedDesignsRepeater input:checked").each(function () {
                        var id = $(this).data('dbid');
                        //designIDs.push({ UserDesignID: id });
                        if ($(this).is(':visible') && id != null) {
                            designIDs.push(id);
                        }
                        //alert("Do something for: " + id + ", " + answer);
                    });
                    if (designIDs.length < 1)
                        return;

                    //app.spinner.show({ container: '#savedDesignsRepeater' });
                    app.spinner.show();

                    $.ajax(
                        {
                            type: "POST",
                            url: "/myAccount/BulkAddToCart/",
                            data: JSON.stringify({ designIDs: designIDs }),
                            contentType: 'application/json',
                            success: function (data) {
                                if (data.designInCart) {
                                    app.spinner.hide();
                                    signsSite.prompts.alert("One or more of your designs is already in your cart.");
                                }
                                else {
                                    signsSite.updateCartCount();
                                    location.href = data.redirectUrl;
                                }
                            }
                        });
                })

                .on('click', '.removeSelected', function (e) {
                    var designIDs = [];
                    $("#savedDesignsRepeater input:checked").each(function () {
                        var id = $(this).data('dbid');
                        //designIDs.push({ UserDesignID: id });

                        if ($(this).is(':visible') && id != null) {
                            designIDs.push(id);
                        }
                        //alert("Do something for: " + id + ", " + answer);
                    });
                    if (designIDs.length < 1)
                        return;
                    // designIDs = JSON.stringify(designIDs);
                    signsSite.prompts.confirm('Are you sure you want to permanently remove these designs?', function () {
                        //app.spinner.show({ container: '#savedDesignsRepeater' });
                        app.spinner.show();
                        $.ajax(
                            {
                                type: "POST",
                                url: "/myAccount/BulkRemoveUserDesign/",
                                data: JSON.stringify({ designIDs: designIDs }),

                                contentType: 'application/json',
                                success: function (data) {
                                    if (data.redirect) {
                                        window.location = data.redirect;
                                    }
                                    if (data.designShared) {
                                        app.spinner.hide();
                                        //signsSite.prompts.alert("One or more of your designs is already added to your cart and cannot be removed. Remove the design from the cart first.");
                                        signsSite.prompts.confirm("This design has been shared. Do you want to continue?", function () {


                                            $.ajax(
                                                {
                                                    type: "POST",
                                                    url: "/myAccount/BulkRemoveUserDesign/",
                                                    data: { designIDs: designIDs, alsoRemoveCart: false, removeSharedDesign: true },
                                                    success: function (data) {
                                                        for (var i = 0, data_len = data.designIDs.length; i < data_len; i++) {

                                                            var removedRow = $('.Row[data-dbid=' + data.designIDs[i] + ']');
                                                            removedRow.parent('.Table').hide();
                                                            $('.HorizontalSplitter[data-dbid=' + data.designIDs[i] + ']').hide();

                                                        }
                                                        app.updateDesignCount();
                                                        signsSite.updateCartCount();
                                                        app.spinner.hide();

                                                        //if all items are removed, reload the page
                                                        if (!$('.Row').is(':visible')) {
                                                            location.reload();
                                                        }
                                                    }
                                                });

                                        }, function () { });
                                    }
                                    if (data.failure) {
                                        app.spinner.hide();
                                        //signsSite.prompts.alert("One or more of your designs is already added to your cart and cannot be removed. Remove the design from the cart first.");
                                        signsSite.prompts.confirm("One or more of your designs is currently in your cart. Deleting your design will also remove it from your cart. Do you want to continue?", function () {


                                            $.ajax(
                                                {
                                                    type: "POST",
                                                    url: "/myAccount/BulkRemoveUserDesign/",
                                                    data: { designIDs: designIDs, alsoRemoveCart: true },
                                                    success: function (data) {
                                                        for (var i = 0, data_len = data.designIDs.length; i < data_len; i++) {

                                                            var removedRow = $('.Row[data-dbid=' + data.designIDs[i] + ']');
                                                            removedRow.parent('.Table').hide();
                                                            $('.HorizontalSplitter[data-dbid=' + data.designIDs[i] + ']').hide();

                                                        }
                                                        app.updateDesignCount();
                                                        signsSite.updateCartCount();
                                                        app.spinner.hide();

                                                        //if all items are removed, reload the page
                                                        if (!$('.Row').is(':visible')) {
                                                            location.reload();
                                                        }
                                                    }
                                                });

                                        }, function () { });

                                    }
                                    else {
                                        //data.designIDs.each(function () {
                                        for (var i = 0, data_len = data.designIDs.length; i < data_len; i++) {


                                            //$('#savedDesignsRepeater').html(data);
                                            var removedRow = $('.Row[data-dbid=' + data.designIDs[i] + ']');
                                            removedRow.parent('.Table').hide();
                                            $('.HorizontalSplitter[data-dbid=' + data.designIDs[i] + ']').hide();
                                            //removedRow.parent('.Table').parent().find('.HorizontalSplitter').hide();
                                            //$('.DesignImage').quickFlip();
                                            //$('.front').css({ position: '' })
                                            //$('.back').css({ position: '' })
                                            //$('.front').css({ height: '' })
                                            //$('.back').css({ height: '' })

                                        }
                                        app.updateDesignCount();
                                        app.spinner.hide();

                                        //if all items are removed, reload the page
                                        if (!$('.Row').is(':visible')) {
                                            location.reload();
                                        }
                                    }
                                }
                            });
                    });

                })

                .on('click', '.shareSelected', function (e) {
                    var designIDs = [];
                    $("#savedDesignsRepeater input:checked").each(function () {
                        var id = $(this).data('dbid');
                        //designIDs.push({ UserDesignID: id });
                        if ($(this).is(':visible') && id != null) {
                            designIDs.push(id);
                        }
                        //alert("Do something for: " + id + ", " + answer);
                    });
                    if (designIDs.length < 1)
                        return;
                    if ($('.account-greeting').html() === '&nbsp;(Sign In)') {
                        signsSite.prompts.alert("You must be signed in to use this feature.");
                        return;
                    }
                    // designIDs = JSON.stringify(designIDs);
                    // signsSite.prompts.confirm('These items will be shared as a single link to the shopping cart of the person you share them with. '
                    //+'Once they click on the link, all of the items you have selected will be added to their shopping cart. If you edit any items after you\'ve shared them, you\'ll need to share those items again. This link will be active for 7 days.', function () {
                    //app.spinner.show({ container: '#savedDesignsRepeater' });
                    app.spinner.show();
                    $.ajax(
                        {
                            type: "POST",
                            url: "/myAccount/ShareMyDesigns/",
                            data: JSON.stringify({ designIDs: designIDs }),

                            contentType: 'application/json',
                            success: function (data) {
                                if (data.error) {
                                    app.spinner.hide();
                                    signsSite.prompts.alert("There was an error creating the shareable link. Please try again.");

                                }
                                if (data.shareUrl) {
                                    app.spinner.hide();
                                    $("#shareDesignURL").val(data.shareUrl);
                                    $('#shareDesignsLinkModal').dialog({
                                        modal: true,
                                        title: 'Share Your Designs',
                                        draggable: false,
                                        resizable: false,
                                        width: 500,
                                        buttons: {
                                            'Copy Link': function () {
                                                copyToClipboard($("#shareDesignURL"));
                                            },
                                            Cancel: function () {
                                                $(this).dialog("close");
                                            }
                                        }
                                    }).dialog('open');
                                }
                            }
                        });
                    // });

                })

                .on('dialogclose', '#shareDesignsLinkModal', function () {
                    $('#sharedDesignURLPermanentToggle').prop("checked", false);
                })

                .on('click', '#sharedDesignURLPermanentToggle', function () {
                    var urlIndex = $('#shareDesignURL').val().indexOf("key=");
                    var urlKey = $('#shareDesignURL').val().substring(urlIndex + 4, urlIndex + 12)
                    var isPermanent = this.checked;
                    //alert(urlKey + " is now " + this.checked);

                    $.ajax(
                        {
                            type: "POST",
                            url: "/myAccount/TogglePermanentShareDesign/",
                            data: JSON.stringify({ urlKey: urlKey, isPermanent: isPermanent }),

                            contentType: 'application/json',
                            success: function (data) {
                                if (data.error) {
                                    app.spinner.hide();

                                    if (isPermanent) {
                                        signsSite.prompts.alert("There was an error making this link permanent. Please try again.");
                                        $('#sharedDesignURLPermanentToggle').prop("checked", false);
                                    }
                                    else {
                                        signsSite.prompts.alert("There was an error making this link not permanent. Please try again.");
                                        $('#sharedDesignURLPermanentToggle').prop("checked", true);
                                    }

                                }
                                if (data.isPermanent) {
                                    app.spinner.hide();
                                }
                            }
                        });
                })

                //.on('click', '#copyDesignShareUrl', function (e) {
                //    copyToClipboard($("#shareDesignURL"));
                //})

                .on('change', 'input[name="AddressLine1"]', function (e) {
                    var poBoxPattern = /[p|P][\s]?\.?\*[o|O][\s]*[b|B][\s]*[o|O][\s]*[x|X][\s]*[a-zA-Z0-9]*|\b[P|p]+(OST|ost|o|O)?\.?\s*[O|o|0]+(ffice|FFICE)?\.?\s*[B|b][O|o|0]?[X|x]+\.?\s+[#]?(\d+)*(\D+)*\b/i;
                    var testPO = $("#AddressLine1").val();
                    var poTest = testPO.toUpperCase().search("P.O.BOX");
                    if ($('#IsDefaultShipping').is(':checked')) {
                        if (($("#AddressLine1").val().toUpperCase().match(poBoxPattern)) || poTest == 0) {
                            $(".addressMessage").html("PO BOX is not allowed")
                            $(".addressMessage").css("color", "Red");
                            $(".addressMessage").show();
                            $("#AddressLine1").css("border", "solid 1px Red");
                            $("#AddressLine1").css("background", "none");
                            $("#AddressLine1").css("background-color", "#ffeeee");
                            $("#AddressLine1").focus();
                        } else {
                            $("#AddressLine1").css("border", "");
                            $("#AddressLine1").css("background-color", "");
                            $(".addressMessage").html("");
                            $(".addressMessage").hide();
                        }
                    }
                    else {
                        $("#AddressLine1").css("border", "");
                        $("#AddressLine1").css("background-color", "");
                        $(".addressMessage").hide();
                    }
                })
                .on('change', 'input[name="AddressLine2"]', function (e) {
                    var poBoxPattern = /[p|P][\s]?\.?\*[o|O][\s]*[b|B][\s]*[o|O][\s]*[x|X][\s]*[a-zA-Z0-9]*|\b[P|p]+(OST|ost|o|O)?\.?\s*[O|o|0]+(ffice|FFICE)?\.?\s*[B|b][O|o|0]?[X|x]+\.?\s+[#]?(\d+)*(\D+)*\b/i;
                    var testPO = $("#AddressLine2").val();
                    var poTest = testPO.toUpperCase().search("P.O.BOX");
                    if ($('#IsDefaultShipping').is(':checked')) {
                        if (($("#AddressLine2").val().toUpperCase().match(poBoxPattern)) || poTest == 0) {
                            $(".addressMessage2").html("PO BOX is not allowed")
                            $(".addressMessage2").css("color", "Red");
                            $(".addressMessage2").show();
                            $("#AddressLine2").css("border", "solid 1px Red");
                            $("#AddressLine2").css("background", "none");
                            $("#AddressLine2").css("background-color", "#ffeeee");
                            $("#AddressLine2").focus();
                        } else {
                            $("#AddressLine2").css("border", "");
                            $("#AddressLine2").css("background-color", "");
                            $(".addressMessage2").html("");
                            $(".addressMessage2").hide();
                        }
                    }
                    else {
                        $("#AddressLine2").css("border", "");
                        $("#AddressLine2").css("background-color", "");
                        $(".addressMessage2").hide();
                    }
                })
                .on('click', '.UpdateCustomerAddress', function (event) {
                    event.preventDefault();
                    var title = 'Update Address';
                    var addressID = $(this).data('addressid');
                    var prevDefaultBillID = $('#updateDefaultBilling').data('addressid');
                    var prevDefaultShipID = $('#updateDefaultShipping').data('addressid');


                    var data = {
                        prevDefaultBillID: prevDefaultBillID,
                        prevDefaultShipID: prevDefaultShipID,
                        addressID: addressID

                    }

                    $.ajax({
                        type: "POST",
                        url: "/MyAccount/LoadCustomerAddAddress/",
                        data: data,
                        success: function (data, textStatus, XMLHttpRequest) {
                            if (data.error) {
                                alert(data.message);
                            } else {
                                $('#addCustomerAddress').html(data);
                                $('#addCustomerAddress').dialog({ title: title }).dialog('open');
                            }
                        },
                        error: function (error) {
                            alert(error.status + "<--and--> " + error.statusText);
                        }
                    });

                })
                .on('click', '#addNewAddressLink', function (event) {
                    event.preventDefault();
                    var title = 'Add new Address';
                    var prevDefaultBillID = $('#updateDefaultBilling').data('addressid');
                    var prevDefaultShipID = $('#updateDefaultShipping').data('addressid');

                    var data = {
                        prevDefaultBillID: prevDefaultBillID,
                        prevDefaultShipID: prevDefaultShipID,

                    }

                    $.ajax({
                        type: "POST",
                        url: "/MyAccount/LoadCustomerAddAddress/",
                        data: data,
                        success: function (data, textStatus, XMLHttpRequest) {
                            if (data.error) {
                                alert(data.message);
                            } else {
                                $('#addCustomerAddress').html(data);
                                $('#addCustomerAddress').dialog({ title: title }).dialog('open');
                            }
                        },
                        error: function (error) {
                            alert(error.status + "<--and--> " + error.statusText);
                        }
                    });


                })
                .on('click', '#changeDefaultShipAddress', function (event) {
                    event.preventDefault();
                    var title = 'Update Default Ship';

                    $.ajax({
                        type: "POST",
                        url: "/MyAccount/LoadDefaultCustomerShipAddress/",
                        success: function (data, textStatus, XMLHttpRequest) {
                            if (data.error) {
                                alert(data.message);
                            } else {
                                $('#defaultCustomerShippingAddress').html(data);
                                $('#defaultCustomerShippingAddress').dialog({ title: title }).dialog('open');
                            }
                        },
                        error: function (error) {
                            alert(error.status + "<--and--> " + error.statusText);
                        }
                    });


                })
                .on('click', '#changeDefaultBillAddress', function (event) {
                    event.preventDefault();
                    var title = 'Update Default Billing';

                    $.ajax({
                        type: "POST",
                        url: "/MyAccount/LoadDefaultCustomerBillingAddress/",
                        success: function (data, textStatus, XMLHttpRequest) {
                            if (data.error) {
                                alert(data.message);
                            } else {
                                $('#defaultCustomerBillingAddress').html(data);
                                $('#defaultCustomerBillingAddress').dialog({ title: title }).dialog('open');
                            }
                        },
                        error: function (error) {
                            alert(error.status + "<--and--> " + error.statusText);
                        }
                    });

                })
                .on('submit', '#addNewAddressForm', function (event) {
                    event.preventDefault();

                    var data = $(this).serialize();

                    var allowUpdate = true;
                    $('input[name="AddressLine1"]').change();
                    $('input[name="AddressLine2"]').change();

                    if ($('#IsDefaultShipping').is(':checked')) {
                        if (($(".addressMessage").html() != "") || $(".addressMessage2").html() != "") {
                            allowUpdate = false;
                        }
                    }

                    if (allowUpdate) {
                        $.ajax({
                            type: "POST",
                            url: "/MyAccount/AddNewCustomerAddress/",
                            data: data,
                            success: function (data, textStatus, XMLHttpRequest) {
                                if (data.error) {
                                    alert(data.message);
                                } else {
                                    $('#userAddresses').html(data);
                                    $('#addCustomerAddress').dialog('close');
                                }
                            },
                            error: function (error) {
                                alert(error.status + "<--and--> " + error.statusText);
                            }
                        });
                    }

                })
                .on('click', '#RemoveUserAddress', function (event) {
                    event.preventDefault();

                    var userAddressID = $('#UserAddressID').val();
                    var data = {
                        addressID: userAddressID
                    }

                    $.ajax({
                        type: "POST",
                        url: "/MyAccount/RemoveUserAddress/",
                        data: data,
                        success: function (data, textStatus, XMLHttpRequest) {
                            if (data.error) {
                                alert(data.message);
                            } else {
                                $('#userAddresses').html(data);
                                $('#addCustomerAddress').dialog('close');
                            }
                        },
                        error: function (error) {
                            alert(error.status + "<--and--> " + error.statusText);
                        }
                    });
                })
                .on('click', '.DefaultBillAddressRadio', function (event) {
                    event.preventDefault();

                    var prevID = $(this).data('prevdefault');
                    var selectedID = $(this).val();
                    var data = {
                        prevID: prevID,
                        selectedID: selectedID
                    }

                    $.ajax({
                        type: "POST",
                        url: "/MyAccount/UpdateDefaultCustomerBilling/",
                        data: data,
                        success: function (data, textStatus, XMLHttpRequest) {
                            if (data.error) {
                                alert(data.message);
                            } else {
                                $('#userAddresses').html(data);
                                $('#defaultCustomerBillingAddress').dialog('close');
                            }
                        },
                        error: function (error) {
                            alert(error.status + "<--and--> " + error.statusText);
                        }
                    });
                })
                .on('click', '.DefaultShipAddressRadio', function (event) {
                    event.preventDefault();

                    var prevID = $(this).data('prevdefault');
                    var selectedID = $(this).val();
                    var data = {
                        prevID: prevID,
                        selectedID: selectedID
                    }

                    $.ajax({
                        type: "POST",
                        url: "/MyAccount/UpdateDefaultCustomerShipping/",
                        data: data,
                        success: function (data, textStatus, XMLHttpRequest) {
                            if (data.error) {
                                alert(data.message);
                            } else {
                                $('#userAddresses').html(data);
                                $('#defaultCustomerShippingAddress').dialog('close');
                            }
                        },
                        error: function (error) {
                            alert(error.status + "<--and--> " + error.statusText);
                        }
                    });
                })
                .on('click', ".savedDesignsTab", function (e) {
                    e.preventDefault();
                    app.myAccount.savedDesigns.pager.getPage(1);
                })
                .on('click', ".orderHistoryTab", function (e) {
                    e.preventDefault();
                    app.myAccount.orderHistory.pager.getPage(1);
                })
                .on('click', ".accountSettingsTab", function (e) {
                    e.preventDefault();
                    app.myAccount.accountSettings.load();
                })
                //Now the mousemove function
                .on('mousemove', '#fancybox-content', function (e) {
                    //When the user hovers on the image, the script will first calculate
                    //the native dimensions if they don't exist. Only after the native dimensions
                    //are available, the script will show the zoomed version.

                    if ($("#fancybox-img").length && $("#fancybox-img").attr("src")) {

                        $(".large").css('background', 'url(' + $("#fancybox-img").attr("src") + ')');

                        if (!native_width && !native_height) {
                            //This will create a new image object with the same image as that in .small
                            //We cannot directly get the dimensions from .small because of the
                            //width specified to 200px in the html. To get the actual dimensions we have
                            //created this image object.
                            var image_object = new Image();
                            image_object.src = $("#fancybox-img").attr("src");
                            //$(".large").css('background','url('+image_object.src+')')

                            //This code is wrapped in the .load function which is important.
                            //width and height of the object would return 0 if accessed before
                            //the image gets loaded.
                            native_width = image_object.width;
                            native_height = image_object.height;
                        } else {
                            //x/y coordinates of the mouse
                            //This is the position of .magnify with respect to the document.
                            var magnify_offset = $(this).offset();
                            //We will deduct the positions of .magnify from the mouse positions with
                            //respect to the document to get the mouse positions with respect to the
                            //container(.magnify)
                            var mx = e.pageX - magnify_offset.left;
                            var my = e.pageY - magnify_offset.top;

                            //Finally the code to fade out the glass if the mouse is outside the container
                            if (mx < $(this).width() && my < $(this).height() && mx > 0 && my > 0) {
                                $(".large").fadeIn(100);
                            } else {
                                $(".large").fadeOut(100);
                            }
                            if ($(".large").is(":visible")) {
                                //The background position of .large will be changed according to the position
                                //of the mouse over the .small image. So we will get the ratio of the pixel
                                //under the mouse pointer with respect to the image and use that to position the
                                //large image inside the magnifying glass
                                var rx = Math.round(mx / $("#fancybox-img").width() * native_width - $(".large").width() / 2) * -1;
                                var ry = Math.round(my / $("#fancybox-img").height() * native_height - $(".large").height() / 2) * -1;
                                var bgp = rx + "px " + ry + "px";

                                //Time to move the magnifying glass with the mouse
                                var px = mx - $(".large").width() / 2;
                                var py = my - $(".large").height() / 2;
                                //Now the glass moves with the mouse
                                //The logic is to deduct half of the glass's width and height from the
                                //mouse coordinates to place it with its center at the mouse coordinates

                                //If you hover on the image now, you should see the magnifying glass in action
                                $(".large").css({ left: px, top: py, backgroundPosition: bgp });
                            }
                        }
                    }
                })
                .on('click', 'a.scroll-link', function (event) {
                    event.preventDefault();
                    app.scrollToElement($(event.currentTarget).attr('href'), { speed: 750 });
                })

                .on('click', '.logoutLink', function (event) {
                    event.preventDefault(); //this prevents the '#' from being added to the url
                    app.spinner.show();
                    $.ajax({
                        type: 'POST',
                        url: '/master/doLogout',
                        complete: function () {
                            location.href = '/';
                        }
                    });
                }).on('submit', '#requestForm', function () {
                //hide button for good measure
                $("#submitRequestBtn").hide();
                var form = $(this);

                //submit request
                $.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    data: form.serialize()
                });


                //hide form & show confirmation
                form.hide();
                $("#requestSaved").show();

                //google analytics
                if (_gaq) {
                    _gaq.push(['_trackPageview', '/request-quote-thank-you']);
                }

                //cancel any additional action
                return false;
            }).on('submit', '#newsletterSignupForm', function (e) {
                //hide the button
                $("#newsletterSignupBtn").hide();


                //submit request
                $.ajax({
                    type: 'POST',
                    url: $(this).attr("action"),
                    data: $(this).serialize(),
                    success: function (data) {
                        if (data === true) {
                            //google analytics
                            if (_gaq) {
                                _gaq.push(['_trackPageview', '/newsletter-signup-thank-you']);
                            }
                        }
                    }
                });

                //hide form & show thank you
                $("#newsletterSignup").hide();
                $("#newsletterSignupSuccess").show();


                //cancel future action
                e.preventDefault();
            }).on('submit', '.feedback-form', function (event) {
                event.preventDefault();
                var form = this;

                var val = $('.txtFeedback', form).first().val();
                if (!String.IsNullOrWhiteSpace(val)) {
                    $.ajax({
                        type: 'POST',
                        url: $(form).attr("action"),
                        data: $(form).serialize() + '&url=' + encodeURIComponent(window.location),
                        success: function (data) {
                            var successBlock = $(form).next('.feedback-success');
                            $('.feedbackPromoCode', successBlock).html(data);
                            $(form).hide();
                            successBlock.show();
                        }
                    });
                }
            }).on('submit', '#loginForm', function (event) {

                event.preventDefault();
                var form = this;
                var url = window.location.href;

                app.spinner.show();

                $.ajax({
                    type: 'POST',
                    url: '/MyAccount/LoginPartial/',
                    data: $(form).serialize(),
                    success: function (data) {

                        if (data != "") {
                            $('#LoginNavigationContainer').html(data);

                            $.fancybox.close();

                            //if we are on any of these pages, reload so accurate data is loaded
                            if (url.indexOf("my-account") >= 0 || url.indexOf("shopping-cart") >= 0 || url.indexOf("checkout") >= 0 || $("#pageType").val() == "Portal") {
                                location.reload(true);
                            }
                            else {
                                //old antiforgery tokens will no longer be valid so get a new one.
                                $.get('/MyAccount/RefreshAntiForgeryToken/', function (html) {
                                    var token = $(html);
                                    var tokenValue = token.val();

                                    //replace the value of all current af tokens on the page with the new one.
                                    $('input[name="__RequestVerificationToken"]').val(tokenValue);

                                    //re-initialize ajax settings so the new token will be used.
                                    app.initAjaxSettings();

                                    if (url.indexOf("design") >= 0) {
                                        //Update userFolderKey
                                        $.ajax({
                                            type: 'POST',
                                            url: '/Design/UpdateUserFolderKey/',
                                            success: function (data) {
                                                $(signsSite.designTool.design.canvas.elements.getAll()).each(function (i, elem) {
                                                    if (elem[0].attrs != null && elem[0].attrs.src != null) {
                                                        var src = elem[0].attrs.src.split("?")[0]; //get source without query strings
                                                        if (src != null) {
                                                            var newSrc = src.replace($('#signsUserFolderKey').html().trim().toUpperCase(), data.userFolder);
                                                            elem.attr({ src: newSrc + '?v=' + Math.random() });
                                                        }
                                                    }
                                                });
                                                $('#signsUserFolderKey').html(data.userFolder);
                                            }
                                        });
                                    }
                                });


                            }
                        }
                        //else if (data == "NotEnabled") {
                        //    $('.accountDisabled').show();
                        //}
                        else {

                            if ($("#unsuccessfulLoginDiv").length) {
                                $("#unsuccessfulLoginDiv").show();
                            }
                        }
                    },
                    complete: function () {
                        app.spinner.hide();
                    }
                });
            })
                .on('click', '.addedToCart-close', function (event) {
                    clearTimeout(addedOverlayTimeout);
                    $('#addedToCart-overlay').fadeOut(500);
                })
                .on('click', '.removePaymentButton', function (event) {
                    event.preventDefault();
                    var token = $('input[name="__RequestVerificationToken"]').val();
                    if (confirm("Delete saved payment method? This can't be undone.")) {
                        app.spinner.show({ text: "Deleting Payment Method..." });
                        var profileID = $(this).attr('data-profile-id');
                        $.ajax({
                            type: 'POST',
                            url: '/Checkout/RemoveCardConnectPaymentProfile/',
                            data: {
                                "__RequestVerificationToken": token,
                                "paymentProfileID": profileID
                            },
                            beforeSend: function (request) {
                                request.setRequestHeader("__RequestVerificationToken", token);
                            },
                            success: function (data) {
                                if (data.isDeleted === "1")
                                    $('#PaymentProfileDiv-' + profileID).fadeOut(300);
                                else if (data.responseText)//if responseText is returned as data
                                    alert('There was an error removing your payment method. This card may be your default card and the failure occured trying to make another card the default.');
                                else if (data.responseText === "Invalid field")//This message comes when deleting their default payment profile. Code is in place so hopefully this is never thrown
                                    alert('There was an error removing your payment method. This is your default card');
                                else
                                    alert('There was an error removing your payment method. It may have already been removed.');
                            },
                            complete: function () {
                                app.spinner.hide();
                            }
                        });
                    }
                })
                .on('submit', '.js_generalUseSearchForm', function (event) {
                    event.preventDefault();
                    var searchStr = $.trim($('input[type = "text"]', this).val()).replace(/[^\w\s]/gi, '');
                    location.href = '../home-templates/search/' + encodeURIComponent(searchStr);
                })

            app.myAccount.tooltips.init();

            $('#trustImages a[target="_blank"]').on('click', function (event) {
                event.preventDefault();
                window.open(event.currentTarget.href, '_blank', 'width=600,height=430,dependent=yes,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=no,directories=no,location=yes');
            });

            $('a#removeAccountLink').on('click', function () {
                signsSite.prompts.confirm('Are you sure you want to remove your account?', function () {
                    $('form#myAccount_accountSettings_removeAccountForm').submit();
                });
            });

            $('.expandCategory, .collapseCategory').on('click', function (event) {
                //alert("here");
                if ($(this).parent().nextAll('ul:first').is(":visible")) {
                    $(this).parent().nextAll('ul:first').hide('fast');
                    $(this).removeClass('collapseCategory');
                    $(this).addClass('expandCategory')
                }
                else {
                    $(this).parent().nextAll('ul:first').show('fast');
                    $(this).removeClass('expandCategory');
                    $(this).addClass('collapseCategory')
                }
            });


            $('.catLink').on('click', function (e) {
                var link = $(this).data('url');
                window.location.href = link;
            });

            $("#myAccountHeaderDetailsUl a:not(.logoutLink)").on('click', function (event) {
                event.preventDefault();

                if ($("#my-account-tabs").length > 0) {
                    var $tabs = $('#my-account-tabs').tabs();
                    var tabID = event.currentTarget.href.split("#")[1];
                    var index = $('#my-account-tabs a[href="#' + tabID + '"]').parent().index();
                    $tabs.tabs("option", "active", index);
                }

                window.location.href = event.currentTarget.href;
            });

            $("#headerLoginLink").on('click', function (event) {
                event.preventDefault(); //this prevents the '#' from being added to the url
                if ($("a#loginLink").length > 0) {
                    $("a#loginLink").click();
                } else {
                    location.href = '/my-account?tab=login';

                }
            });

            $("#LoginLinkInCart").on('click', function (event) {
                event.preventDefault(); //this prevents the '#' from being added to the url
                if ($("a#loginLink").length > 0) {
                    $("a#loginLink").click();
                } else {
                    location.href = '/my-account?tab=login';

                }
            });

            $(".productImage").on('hover', function (e) {
                $(this).find('img').css("padding", "0px 0px 0px 0px");
                $(this).find('img').css("border", "solid 6px #ff8100");
            });

            $(".productImage").on('mouseleave', function (e) {
                $(this).find('img').css("padding", "");
                $(this).find('img').css("border", "");
            });


            $(".backTop").on('click', function () {
                $('html, body').animate({ scrollTop: 0 }, 750);
                return false;
            });

            //NEW MENU Navigation
            $('.cd-dropdown-trigger').on('click', function (event) {
                event.preventDefault();
                //if (screen.width < 768) {
                // document.body.style.overflow = "hidden";
                //}
                // toggleNav($(this).parent());
            })

            //close meganavigation
            $('.cd-dropdown .cd-close').on('click', function (event) {
                event.preventDefault();
                // toggleNav($(this).parent());
                document.body.style.overflow = "visible";
            })

            /* if (screen.width > 768) {
             $('#ProductDropDwnWrapper').on('mouseover', function (event) {
             event.preventDefault();
             var navIsVisible = (!$(this).find('.cd-dropdown').hasClass('dropdown-is-active')) ? true : false;
             if (navIsVisible) {
             toggleNav($(this));
             }
             })
             $('#ProductDropDwnWrapper').on('mouseleave', function (event) {
             event.preventDefault();
             var navIsVisible = (!$(this).find('.cd-dropdown').hasClass('dropdown-is-active')) ? true : false;
             if (!navIsVisible) {
             toggleNav($(this));
             }
             })

             $('#IndustryDropDwnWrapper').on('mouseover', function (event) {
             event.preventDefault();
             var navIsVisible = (!$(this).find('.cd-dropdown').hasClass('dropdown-is-active')) ? true : false;
             if (navIsVisible) {
             toggleNav($(this));
             }
             })
             $('#IndustryDropDwnWrapper').on('mouseleave', function (event) {
             event.preventDefault();
             var navIsVisible = (!$(this).find('.cd-dropdown').hasClass('dropdown-is-active')) ? true : false;
             if (!navIsVisible) {
             toggleNav($(this));
             }
             })

             $('#SFHDropDwnWrapper').on('mouseover', function (event) {
             event.preventDefault();
             var navIsVisible = (!$(this).find('.cd-dropdown').hasClass('dropdown-is-active')) ? true : false;
             if (navIsVisible) {
             toggleNav($(this));
             }
             })
             $('#SFHDropDwnWrapper').on('mouseleave', function (event) {
             event.preventDefault();
             var navIsVisible = (!$(this).find('.cd-dropdown').hasClass('dropdown-is-active')) ? true : false;
             if (!navIsVisible) {
             toggleNav($(this));
             }
             })
             } */



            //on mobile - open submenu
            //$('.has-children').children('a').on('click', function (event) {
            //prevent default clicking on direct children of .has-children
            //event.preventDefault();
            //var selected = $(this);
            //selected.next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('move-out');

            //document.body.style.overflow = "hidden";

            //always scroll the div to the top for items that are on the bottom of the list
            /*$('.cd-dropdown-content').animate({ scrollTop: 0 }, 'fast'); */


            //})

            //on desktop - differentiate between a user trying to hover over a dropdown item vs trying to navigate into a submenu's contents
            /* var submenuDirection = (!$('.cd-dropdown-wrapper').hasClass('open-to-left')) ? 'right' : 'left';
             $('.cd-dropdown-content').menuAim({
             activate: function (row) {
             $('#webheader div#next-day-shipping-container').css('z-index', 'auto');
             $('#sliderNavLinks').css('z-index', 'auto');
             $(row).children().addClass('is-active').removeClass('fade-out');
             if ($('.cd-dropdown-content .fade-in').length == 0) $(row).children('ul').addClass('fade-in');
             },
             deactivate: function (row) {
             $('#webheader div#next-day-shipping-container').css('z-index', '1');
             $('#sliderNavLinks').css('z-index', '1');
             $(row).children().removeClass('is-active');
             if ($('li.has-children:hover').length == 0 || $('li.has-children:hover').is($(row))) {
             $('.cd-dropdown-content').find('.fade-in').removeClass('fade-in');
             $(row).children('ul').addClass('fade-out')
             }
             },
             exitMenu: function () {
             $('.cd-dropdown-content').find('.is-active').removeClass('is-active');
             // toggleNav($(this).parent());
             return true;
             },
             submenuDirection: submenuDirection,
             }) */

            //submenu items - go back link
            //$('.go-back').on('click', function () {
            //var selected = $(this),
            // visibleNav = $(this).parent('ul').parent('.has-children').parent('ul');
            // selected.parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('move-out');
            //})


            /*End Mega Navigation*/


        },
        //#endregion init

        //#region Misc

        getStatesDDL: function (countryDDL, callback) {
            var ddlVal = $(countryDDL).val();
            var submitData;

            if (/^[0-9]*$/.test(ddlVal)) {
                submitData = { countryID: ddlVal };
            }
            else if (/^([a-zA-Z]{2,3})$/.test(ddlVal)) {
                submitData = { countryIso: ddlVal };
            }
            else {
                submitData = { countryIso: 'US' };
            }

            $.ajax({
                type: 'POST',
                url: '/Default/GetStates/',
                data: submitData,
                success: function (data) {
                    var stateInput = $('#' + $(countryDDL).data('stateinputid'));

                    if (data.length) {
                        if (stateInput.is('select')) {
                            stateInput.empty();
                        }
                        else {
                            //convert to ddl
                            var newInput = $('<select>').insertAfter(stateInput);
                            stateInput.remove();

                            newInput.attr('id', stateInput.attr('id'))
                                .attr('name', stateInput.attr('name'))
                                .attr('class', stateInput.attr('class'));
                            //.attr('tabindex', stateInput.attr('tabindex'));

                            stateInput = newInput;
                        }

                        //populate options
                        stateInput.append('<option>'); //add blank option at top

                        for (var i = 0; i < data.length; i++) {
                            stateInput.append($('<option>')
                                .val(data[i].shortName)
                                .html(data[i].longName));
                        }
                    }
                    else {
                        //no states returned...present textbox
                        if (stateInput.is('input:text')) {
                            stateInput.val('');
                        }
                        else {
                            //convert to textbox
                            var newInput = $('<input type="text">').insertAfter(stateInput);
                            stateInput.remove();

                            newInput.attr('id', stateInput.attr('id'))
                                .attr('name', stateInput.attr('name'))
                                .attr('class', stateInput.attr('class'));
                            //.attr('tabindex', stateInput.attr('tabindex'));

                            stateInput = newInput;
                        }
                    }

                    $(stateInput).trigger('updatecomplete'); //fires the 'updatecomplete' event that can be bound to
                },
                complete: function () {
                    if (typeof callback === 'function') {
                        callback();
                    }
                }
            });
        },

        templateProductClick: function (productObj) {

            var templateCat = 'Generic';
            var listName = 'Generic Search Results';
            //if (signsSite.templateSearchManager.productUrl != null)
            //{
            //    templateCat = signsSite.templateSearchManager.productUrl;
            //    listName = templateCat + 'Search Results';
            //}
            //else
            if ($('#gaProductName').html() != undefined) {
                if ($('#gaProductName').html().length > 0) {
                    templateCat = $('#gaProductName').html().trim();
                    if (window.location.href.indexOf("search") > -1) {
                        listName = templateCat + ' Search Results';
                    }
                    else {
                        listName = templateCat + ' Page Results';
                    }
                }
            }
            var brand = '';

            if ($('#vendorName').html() != undefined) {
                brand = $('#vendorName').html().trim();
            }

            dataLayer.push({
                'event': 'productClick',
                'ecommerce': {
                    'click': {
                        'actionField': { 'list': listName },
                        'products': [{
                            'name': productObj.name,                      // Name or ID is required.
                            'id': productObj.id,
                            'brand': brand,
                            'category': templateCat,
                            'position': productObj.position
                        }]
                    }
                }//, --Removing callback and just having the link use the href. --GE
                //'eventCallback': function () {
                //    document.location = productObj.url
                //}
            })
        },

        scrollToElement: function (selector, options) {
            var speed = 'fast',
                offset = 0;

            if (options) {
                if (typeof options.speed === 'number' || typeof options.speed === 'string') {
                    speed = options.speed;
                }
                if (typeof options.offset === 'number') {
                    offset = options.offset;
                }
            }

            $('html, body').animate({ scrollTop: $(selector).first().offset().top + offset }, speed);
        },

        slideReplace: function (outboundElem, inboundElem, options) {
            var processData = {
                parentElem: outboundElem.parent()
            };
            var inboundElemStage,
                outboundElemDest,
                direction = 'left',
                speed = 500,
                callback;

            if (options) {
                if (typeof options.direction === 'string') {
                    direction = options.direction;
                }
                if (typeof options.speed === 'number') {
                    speed = options.speed;
                }
                if (typeof options.callback === 'function') {
                    callback = options.callback;
                }
            }

            // outboundElem.css({ position: 'absolute' });
            //processData.parentElem.css({ overflow: 'hidden' });

            if (direction === 'up' || direction === 'down') {
                var height = processData.parentElem.height();

                if (direction === 'up') {
                    inboundElemStage = height;
                    outboundElemDest = -height;
                }
                if (direction === 'down') {
                    inboundElemStage = -height;
                    outboundElemDest = height;
                }

                inboundElem.hide().appendTo(processData.parentElem).css({ top: inboundElemStage, position: 'absolute' });

                outboundElem.animate({ top: outboundElemDest }, speed, function () {
                    outboundElem.hide();
                    outboundElem.css({ top: null, position: null });
                });
                inboundElem.show().animate({ top: 0 }, speed, function () {
                    inboundElem.css({ top: null, position: null });
                    if (callback) {
                        callback(processData);
                    }
                });
            } else {
                var width = processData.parentElem.width();

                if (direction === 'right') {
                    inboundElemStage = -width;
                    outboundElemDest = width;
                } else {
                    inboundElemStage = width;
                    outboundElemDest = -width;
                }

                inboundElem.hide().appendTo(processData.parentElem).css({ left: inboundElemStage });

                outboundElem.animate({ left: outboundElemDest }, speed, function () {
                    outboundElem.hide();
                    outboundElem.css({ left: null, position: null });
                });
                inboundElem.show().animate({ left: 0 }, speed, function () {
                    inboundElem.css({ left: null, position: null });
                    if (callback) {
                        callback(processData);
                    }
                });
            }
        },

        myAccount: {
            animation: {
                queueGroups: {},
            },

            tooltips:
                {
                    init: function () {
                        $(document)
                            .tooltip({
                                items: '.optionInfo',
                                tooltipClass: "style1",
                                content: function () {

                                    return $(this).next('.infoToast').html();

                                },
                                position: {
                                    my: signsSite.mediaMatch.isScreenAndMaxWidth480Match ? 'bottom' : 'left+20 top-40',
                                    at: signsSite.mediaMatch.isScreenAndMaxWidth480Match ? 'top-20' : 'right',
                                    collision: 'flipfit',
                                    using: function (position, feedback) {
                                        $(this).css(position);

                                        if (!signsSite.mediaMatch.isScreenAndMaxWidth480Match) {
                                            //add the arrow
                                            $("<div>")
                                                .addClass("arrow")
                                                .addClass(feedback.vertical)
                                                .addClass(feedback.horizontal)
                                                .appendTo(this);
                                        }
                                    }
                                },
                                open: function (event, ui) {
                                    if (event.originalEvent === undefined) {
                                        return false;
                                    }

                                    var $id = $(ui.tooltip).attr('id');

                                    // close any lingering tooltips
                                    $('div.ui-tooltip').not('#' + $id).remove();
                                },
                                close: function (event, ui) {
                                    //this prevents the tooltip from closing if it is hovered so links can be added.
                                    //ui.tooltip.on('hover',
                                    //    function () {
                                    //        $(this).stop(true).fadeTo(400, 1);
                                    //    },
                                    //    function () {
                                    //        $(this).fadeOut('400', function () {
                                    //            $(this).remove();
                                    //        });
                                    //    });
                                }
                            });

                        $('#savedDesignsRepeater')
                            .tooltip({
                                items: '.pixelationWarning',
                                content: function () {
                                    return $(this).next('.infoToast').html();
                                },
                                position: {
                                    my: "right bottom",
                                    at: "right-2 top-20"
                                }
                            });

                        $('#orderHistoryRepeater').tooltip({
                            items: '.orderStatus-info',
                            tooltipClass: "style1",
                            content: function () {
                                return $(this).next('.infoToast').html();
                            },
                            position: {
                                my: signsSite.mediaMatch.isScreenAndMaxWidth480Match ? 'bottom' : 'left+20 top-40',
                                at: signsSite.mediaMatch.isScreenAndMaxWidth480Match ? 'top-20' : 'right',
                                collision: 'flipfit',
                                using: function (position, feedback) {
                                    $(this).css(position);

                                    if (!signsSite.mediaMatch.isScreenAndMaxWidth480Match) {
                                        //add the arrow
                                        $("<div>")
                                            .addClass("arrow")
                                            .addClass(feedback.vertical)
                                            .addClass(feedback.horizontal)
                                            .appendTo(this);
                                    }
                                }
                            }
                        });

                    }
                },
            orderHistory: {
                pager: {
                    pageSize: null, //set when _myAccountSavedDesignsRepeater loads
                    getPage: function (page) {
                        app.spinner.show({ container: "#orderHistoryRepeater", delay: 1 });
                        var data = {
                            page: page,
                            pageSize: app.myAccount.savedDesigns.pager.pageSize
                        };

                        jQuery('#orderHistoryRepeater').load('/MyAccount/GetOrderHistoryPage', data, function () {
                            app.spinner.hide();
                        });
                    }
                }
            },
            accountSettings: {
                load: function () {
                    app.spinner.show({ container: "#accountSettingsPartialDiv", delay: 1 });

                    jQuery('#accountSettingsPartialDiv').load('/MyAccount/GetAccountSettingsPage', function () {
                        app.spinner.hide();
                    });
                }
            },
            savedDesigns: {
                pager: {
                    pageSize: null, //set when _myAccountSavedDesignsRepeater loads
                    getPage: function (page) {
                        app.spinner.show({ container: "#savedDesignsRepeater", delay: 1 });
                        var data = {
                            page: page,
                            pageSize: app.myAccount.savedDesigns.pager.pageSize
                        };

                        jQuery('#savedDesignsRepeater').load('/MyAccount/GetMyDesignsPage', data, function () {
                            app.spinner.hide();
                        });
                    }
                },
                price: {
                    doHighlight: function (elems, queueName) {
                        if (typeof elems === 'object' || typeof elems === 'string') {
                            if (typeof queueName !== 'string') {
                                queueName = 'fx';
                            }

                            if (app.myAccount.animation.queueGroups[queueName]) {
                                app.myAccount.animation.queueGroups[queueName].stop(true, true, queueName); //clear any currently queued highlight animations to prevent multiple highlight operations firing consecutively
                                app.myAccount.animation.queueGroups[queueName] = app.myAccount.animation.queueGroups[queueName].add(elems); //add elements to the queue group
                            }
                            else {
                                app.myAccount.animation.queueGroups[queueName] = $(elems); //or create a new queue group
                            }

                            //add animations to a managed queue
                            $(elems).effect('highlight', { queue: queueName }, 3000, function () {
                                app.myAccount.animation.queueGroups[queueName] = app.myAccount.animation.queueGroups[queueName].not(elems); //remove items from the queue group once their animations have completed.
                            }).dequeue(queueName); //have to call dequeue to start the animations when using a custom queue name.
                        }
                    }
                },
                quantity: {
                    update: function (event) {
                        var elem = $(event.target),
                            isElemTxtBox = elem.is('.txtQuantity'),
                            row = elem.closest('.Row'),
                            designId = row.data('designid'),
                            txtBox = isElemTxtBox ? elem : $('#' + designId + '_qty', row),
                            revert = txtBox.data('revert'),
                            isError = false,
                            quantity
                        ;

                        if (elem.is('.discount')) {
                            event.preventDefault();
                            quantity = elem.data('quantity');
                            var row = elem.closest('.Row'),
                                designId = row.data('designid'),
                                ddl = $('#' + designId + '_selqty', row);
                            if (ddl.is(":visible")) {
                                ddl.val(quantity);
                            }
                        }
                        else {
                            quantity = parseInt(txtBox.val(), 10);
                        }


                        if (quantity < 1 || isNaN(quantity)) {
                            if (isElemTxtBox) {
                                txtBox.val(revert);
                            }
                        }
                        else if (quantity != revert) { //only run if changed
                            app.spinner.show({ container: row });

                            $.ajax({
                                type: 'POST',
                                url: '/myaccount/UpdateQuantity/' + designId,
                                data: { quantity: quantity },
                                success: function (data, textStatus, jqXHR) {
                                    if (data.error) {
                                        isError = true;
                                    }
                                    else {
                                        //update textbox if necessary
                                        if (!isElemTxtBox) {
                                            txtBox.val(quantity).data('revert', quantity);
                                        }

                                        //update revert value
                                        txtBox.data('revert', quantity);

                                        var elementsToHighlight = $([]);

                                        //update subtotal
                                        var subTotalElem = $('#' + designId + '_subTotal');
                                        subTotalElem.html(data.subtotal);
                                        var strikeThrough = $('#' + designId + '_strikeThrough');
                                        strikeThrough.html(data.strikeThroughPrice);
                                        elementsToHighlight = elementsToHighlight.add(subTotalElem);

                                        //update discounts
                                        if (data.discounts.length > 0) {
                                            $('.discount-tr', row).each(function (i, el) {
                                                if (data.discounts.length > i) {
                                                    var discount = data.discounts[i],
                                                        td = $(el).children('.discount'),
                                                        isChanged = td.data('quantity') !== discount.minQty,
                                                        isHidden = $(el).is(':hidden')
                                                    ;

                                                    if (isChanged) {
                                                        td.data('quantity', discount.minQty)
                                                            .attr('title', discount.title)
                                                            .html(discount.text);
                                                    }

                                                    if (isHidden) {
                                                        $(el).show();
                                                    }

                                                    if (isChanged || isHidden) {
                                                        elementsToHighlight = elementsToHighlight.add(td);
                                                    }
                                                }
                                                else {
                                                    $(el).hide();
                                                }
                                            });

                                            $('.discount-maxed-tr', row).hide();
                                        }
                                        else {
                                            $('.discount-tr', row).hide();
                                            $('.discount-maxed-tr', row).show();
                                        }

                                        //highlight changed elements
                                        app.myAccount.savedDesigns.price.doHighlight(elementsToHighlight, designId);
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    isError = true;
                                },
                                complete: function (jqXHR, textStatus) {
                                    app.spinner.hide();

                                    if (isError) {
                                        signsSite.prompts.alert('An error occured while attempting to update the quantity. Please try again. If the problem persists, please contact us at ' + app.customerServiceNumber + '.');
                                    }
                                }
                            });
                        }
                    }
                }
            }
        },

        checkLogin: function (isLoggedIn) {
            //check for login stuff in URL and show login fancybox if required.
            if (isLoggedIn) {
                window.location.href = '/my-account/';
            } else {
                var email = app.getQueryStringVariable('email');
                $('#signsUser_email').val(email);
                $("#loginLink").trigger('click');
            }
        },
        safeLoadImage: function (imageElement, maxTries, lineItemID, newGuid, imageServer, userFolderKey, isBack) {
            //this attempts to load the image from the lineItems folder 10 times and then gives up so that we don't end up in an endless loop
            this.maxTries = typeof maxTries === 'number' ? maxTries : 10;
            this.app = this;
            this.tryCount = 0;
            this.timeOut = 5000;

            var imageSrc = imageServer + 'images/' + userFolderKey + '/lineItemDesigns/' + lineItemID + '/' + lineItemID + '.png?' + newGuid;
            if (isBack) {
                imageSrc = imageServer + 'images/' + userFolderKey + '/lineItemDesigns/' + lineItemID + '/' + lineItemID + '_Back.png?' + newGuid;
            }

            this.tryLoadImage = function () {
                app.tryCount++;
                $('#counter').html(app.tryCount);

                var img = new Image();
                img.onload = app.success;
                img.onerror = app.error;
                img.src = imageSrc;
            }

            this.success = function () {
                imageElement.src = imageSrc;
            };

            this.error = function () {
                if (app.tryCount < app.maxTries) {
                    setTimeout(app.tryLoadImage, app.timeOut);
                }
            };

            app.tryLoadImage();
        },

        loadTestimonial: function () {
            $.get('/default/randomTestimonial', function (data) {
                if (data) {
                    $('#testimonialWidget_text').html(data.Text);
                    $('#testimonialWidget_author').html(data.Author);
                    $("#testimonialWidget_outerContainer").hide().fadeIn(1000);
                    setTimeout(app.loadTestimonial, 7000);
                }
            });
        },

        advancedRound: function (val, decimalPlaces) {
            var mulitiplier = Math.pow(10, decimalPlaces);
            return Math.round(val * mulitiplier) / mulitiplier;
            //var finResult = (Math.round(result * 4) / 4).toFixed(2); //rounding to nearest 1/4" as per this task: https://basecamp.com/1873037/projects/742309/todos/249246279  --GE
            //Leaving the above in case we change our minds again :)


        },

        getRoundedFeet: function (inchesValue) {
            return app.advancedRound(inchesValue / 12, 2);
        },

        setSiteUI: function (uimode, elem) {
            $.post('/master/SetSiteUI', { uimode: uimode }, function () {
                window.location = $(elem).attr('href');
            });
        },

        getQueryStringVariable: function (variable) {
            var query = window.location.search.substring(1),
                vars = query.split('&'),
                value = '';

            $.each(vars, function (i, pair) {
                pair = pair.split('=');

                if (decodeURIComponent(pair[0]) === variable) {
                    value = decodeURIComponent(pair[1]);

                    return;
                }
            });

            return value;
        },

        // Test if an event is supported by the browser
        isEventSupported: function (event) {
            var el = $('<div>');

            event = 'on' + event;

            var supported = el.hasOwnProperty(event);

            if (!supported) {
                el.attr(event, 'return;');

                supported = typeof el[event] === 'function';
            }

            el.remove();

            return supported;
        },

        /**
         * Recursive copy of object
         */
        cloneObj: function (obj) {
            //app.log.print('func: cloneObj');

            if (obj instanceof Array) {
                return obj.slice(0);
            }

            var clone = {};

            $.each(obj, function (i, value) {
                clone[i] = typeof value === 'object' ? signsSite.cloneObj(value) : value;
            });

            return clone;
        },

        hideLightbox: function () { $.fancybox.close(); },

        changeCheckbox: function (checkboxID) {
            //for good measure, use jquery CLICK event, in case jquery is wired up in other places, like the design tool advanced tools section
            $('#' + checkboxID).click().focus(); //give focus to the checkbox to facilitate proper tab indexing
        },

        showOrderSummary: function () {
            $("#orderDetailDiv").hide();
            $("#orderSummaryDiv").show();
        },

        addSavedDesignToCart: function (id) {
            app.spinner.show({ container: '#saved-designs' });
            $.ajax({
                url: '/design/addToCart',
                type: 'POST',
                data: { designID: id, quantity: 1, checkDefaultOptions: true },
                success: function (data) {

                    signsSite.updateCartCount();
                    location.href = "/shopping-cart/";
                }
            });
        },

        addGACheckoutStep: function (id, designPrice, signType, templateID, templateName, vendorName, qty) {
            dataLayer.push({
                'event': 'checkout',
                'ecommerce': {
                    'checkout': {
                        'actionField': { 'step': 1 },
                        'products': [{
                            'name': templateName,
                            'id': templateID,
                            'price': designPrice,
                            'brand': vendorName,
                            'category': signType,
                            'quantity': qty
                        }]
                    }
                }
            });

        },


        myAccountAddDesignToCart: function (id, designPrice, signType, templateID, templateName, vendorName) {
            app.spinner.show({ container: '#savedDesignsRepeater' });
            var qtyTxtBox = document.getElementById(id + "_qty");

            signsSite.addGACheckoutStep(id, designPrice, signType, templateID, templateName, vendorName, qtyTxtBox);


            //Google tag manager code
            dataLayer.push({
                'event': 'addToCart',
                'ecommerce': {
                    'currencyCode': 'US',
                    'add': {
                        'actionField': { 'step': 1 },
                        'products': [{
                            'name': templateName,
                            'id': templateID,
                            'price': designPrice,
                            'brand': vendorName,
                            'category': signType,
                            'quantity': qtyTxtBox.value

                        }]
                    }//,
                    //'checkout': {
                    //    'actionField': { 'step': 1 }
                    //}
                }
            });


            $.ajax({
                url: '/myAccount/addToCart',
                type: 'POST',
                data: { designID: id, quantity: qtyTxtBox.value, designPrice: designPrice },
                success: function (data) {
                    if (data.designInCart) {
                        app.spinner.hide();
                        signsSite.prompts.alert("This design is already in your cart.")
                    }
                    else {
                        signsSite.updateCartCount();
                        app.spinner.hide();
                        if (data.redirectUrl) {
                            location.href = data.redirectUrl;
                        }

                    }

                }
            });
        },

        addToCartNoRedirect: function (id, qty) {
            app.spinner.show({ container: 'body' });

            var qtyTxtBox = document.getElementById(id + "_qty");



            $.ajax({
                url: '/myAccount/AddDesignToCart',
                type: 'POST',
                data: { designID: id, quantity: qty, redirect: false },
                success: function (data) {
                    if (data.designInCart) {
                        app.spinner.hide();
                        signsSite.prompts.alert("This design is already in your cart.")
                    }
                    else {
                        signsSite.updateCartCount();
                        $('#addedToCart-overlay').fadeIn(500, function () {
                            //addedOverlayTimeout = setTimeout(function () { $('#addedToCart-overlay').fadeOut(500) }, 5000);
                        });
                        app.spinner.hide();
                    }

                }
            });
        },

        orderHistoryAddDesignToCart: function (id, signTypeName, price, quantity, templateID, templateName, vendor) {
            app.spinner.show({ container: '#orderHistoryContainer' });

            signsSite.addGACheckoutStep(id, price, signTypeName, templateID, templateName, vendor, quantity);

            //Google tag manager code
            dataLayer.push({
                'event': 'addToCart',
                'ecommerce': {
                    'currencyCode': 'US',
                    'add': {
                        'actionField': { 'step': 1 },
                        'products': [{
                            'name': templateName,
                            'id': templateID,
                            'price': price,
                            'brand': vendor,
                            'category': signTypeName,
                            'quantity': quantity,
                            'step': 1
                        }]
                    }//,
                    //'checkout': {
                    //    'actionField': { 'step': 1 }
                    //}
                }
            });
            $.ajax({
                url: '/myAccount/AddToCartFromOrderDetail',
                type: 'POST',
                data: { orderLineitemId: id },
                success: function (data) {
                    signsSite.updateCartCount();
                    location.href = data.redirectUrl;
                }
            });
        },


        removeSavedDesign: function (idStr) {
            signsSite.prompts.confirm('Are you sure you want to permanently remove this design?', function () {
                app.spinner.show({ container: '#saved-designs' });
                $.ajax(
                    {
                        type: "POST",
                        url: "/myAccount/removeUserDesign/" + idStr,
                        success: function (data) {
                            if (data.redirect) {
                                window.location = data.redirect;
                            }
                            else if (data.designShared) {
                                app.spinner.hide();
                                signsSite.prompts.confirm("This design has been shared. Do you want to continue?", function () {

                                    $.ajax(
                                        {
                                            type: "POST",
                                            url: "/myAccount/removeUserDesign/" + idStr,
                                            data: { page: 1, pageSize: 25, alsoRemoveCart: false, removeSharedDesign: true },
                                            success: function (data) {
                                                var removedRow = $('.Row[data-designid=' + idStr + ']');
                                                removedRow.parent('.Table').hide();
                                                $('.HorizontalSplitter[data-designid=' + idStr + ']').hide();

                                                //if all items are removed, reload the page
                                                if (!$('.Row').is(':visible')) {
                                                    location.reload();
                                                }
                                                app.updateDesignCount();
                                                signsSite.updateCartCount();
                                                app.spinner.hide();
                                            }
                                        });

                                }, function () { });

                            }
                            else if (data.failure) {
                                app.spinner.hide();
                                signsSite.prompts.confirm("This design is currently in your cart. Deleting your design will also remove it from your cart. Do you want to continue?", function () {

                                    $.ajax(
                                        {
                                            type: "POST",
                                            url: "/myAccount/removeUserDesign/" + idStr,
                                            data: { page: 1, pageSize: 25, alsoRemoveCart: true },
                                            success: function (data) {
                                                var removedRow = $('.Row[data-designid=' + idStr + ']');
                                                removedRow.parent('.Table').hide();
                                                $('.HorizontalSplitter[data-designid=' + idStr + ']').hide();

                                                //if all items are removed, reload the page
                                                if (!$('.Row').is(':visible')) {
                                                    location.reload();
                                                }
                                                app.updateDesignCount();
                                                signsSite.updateCartCount();
                                                app.spinner.hide();
                                            }
                                        });

                                }, function () { });
                            } else {
                                //$('#savedDesignsRepeater').html(data);
                                var removedRow = $('.Row[data-designid=' + idStr + ']');
                                removedRow.parent('.Table').hide();
                                $('.HorizontalSplitter[data-designid=' + idStr + ']').hide();
                                //removedRow.parent('.Table').parent().find('.HorizontalSplitter').hide();
                                //$('.DesignImage').quickFlip();
                                //$('.front').css({ position: '' })
                                //$('.back').css({ position: '' })
                                //$('.front').css({ height: '' })
                                //$('.back').css({ height: '' })

                                //if all items are removed, reload the page
                                if (!$('.Row').is(':visible')) {
                                    location.reload();
                                }
                                app.updateDesignCount();
                                app.spinner.hide();
                            }
                        }
                    });
            });
        },

        duplicateSavedDesign: function (idStr) {
            signsSite.prompts.confirm('Are you sure you want to duplicate this design?', function () {
                app.spinner.show({ container: '#saved-designs' });
                $.ajax(
                    {
                        type: "POST",
                        url: "/myAccount/duplicateUserDesign/" + idStr,
                        success: function (data) {
                            if (data.redirect) {
                                window.location = data.redirect;
                            } else {
                                $('#savedDesignsRepeater').html(data);
                                //$('.DesignImage').quickFlip();
                                //$('.front').css({ position: '' })
                                //$('.back').css({ position: '' })
                                //$('.front').css({ height: '' })
                                //$('.back').css({ height: '' })
                                app.updateDesignCount();
                                app.spinner.hide();
                            }
                        }
                    });
            });
        },


        updateDesignCount: function () {
            if ($(".savedDesignsCount").length) {
                $.ajax(
                    {
                        type: "POST",
                        url: "/master/getDesignCount",
                        success: function (data) {
                            $(".savedDesignsCount").html('(' + data.designCount + ')');
                        }
                    });
            }
        },

        updateCartCount: function () {
            var countContainer = $(".cartCount");

            if (countContainer.length) {
                $.ajax(
                    {
                        type: "POST",
                        url: "/master/getCartCount",
                        success: function (data) {
                            countContainer.data('count', data.cartCount).html(data.cartCount);
                        }
                    });
            }
        },



        showOrderDetail: function (id) {
            app.spinner.show({ container: '#orderSummaryDiv' });
            $.ajax(
                {
                    type: 'POST',
                    url: '/myAccount/orderDetails/' + id,
                    success: function (result) {
                        app.spinner.hide();
                        $("#orderSummaryDiv").hide();
                        $("#orderDetailDiv").show();
                        $("#orderDetailDiv").html(result);
                    },

                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        signsSite.prompts.alert(errorThrown);
                    }
                });
        },

        getGuid: function (callBack) {
            $.ajax({
                url: '/master/newGuid',
                type: 'POST',
                success: function (data) {
                    callBack(data.guid);
                }
            });
        },
        //#endregion Misc

        //#endregion Functions

        mediaMatch: {
            init: function () {
                if (typeof window.matchMedia === 'function') {
                    this.isScreenAndMaxWidth320Match = window.matchMedia('only screen and (max-device-width: 320px)').matches;
                    this.isScreenAndMaxWidth480Match = this.isScreenAndMaxWidth320Match ? true : window.matchMedia('only screen and (max-device-width: 480px)').matches;
                }
            },
            isScreenAndMaxWidth480Match: false,
            isScreenAndMaxWidth320Match: false
        },

        nextDayShippingBadge: {
            init: function () {
                app.nextDayShippingBadge.initTooltip();
                //app.nextDayShippingBadge.orderDeadlineTimer.init();
            },
            initTooltip: function () {
                $('#next-day-shipping-container .details-link').tooltip({
                    items: '#next-day-shipping-container .details-link',
                    tooltipClass: "verticalArrow next-day-shipping-tooltip",
                    content: function () {
                        return $(this).next('div.details-toast').html();
                    },
                    position: {
                        my: "right top",
                        at: "center+76 bottom+15",
                        using: function (position, feedback) {
                            //console.log(feedback);
                            $(this).css(position);
                            $("<div>")
                                .addClass("arrow")
                                .addClass(feedback.vertical)
                                .addClass(feedback.horizontal)
                                .appendTo(this);
                        }
                    },
                    open: function (event, ui) {
                        if (typeof (event.originalEvent) === 'undefined') {
                            return false;
                        }

                        var $id = $(ui.tooltip).attr('id');

                        // close any lingering tooltips
                        $('div.ui-tooltip').not('#' + $id).remove();
                    },
                    close: function (event, ui) {
                        //this prevents the tooltip from closing if it is hovered so links can be added.
                        //ui.tooltip.on('hover',
                        //        function () {
                        //            $(this).stop(true).fadeTo(400, 1);
                        //        },
                        //        function () {
                        //            $(this).fadeOut('400', function () {
                        //                $(this).remove();
                        //            });
                        //        });
                    }
                });
            },

            orderDeadlineTimer: {
                hours: 24,
                minutes: 0,
                seconds: 0,
                timeOut: null,
                clear: function () {
                    clearInterval(app.nextDayShippingBadge.orderDeadlineTimer.timeOut);
                    app.nextDayShippingBadge.orderDeadlineTimer.timeOut = null;
                },

                init: function (h, m, s) {
                    //app.nextDayShippingBadge.orderDeadlineTimer.updateSLA(true);

                    if (typeof h === 'number') {
                        app.nextDayShippingBadge.orderDeadlineTimer.hours = h;
                    }

                    if (typeof m === 'number') {
                        app.nextDayShippingBadge.orderDeadlineTimer.minutes = m;
                    }

                    if (typeof s === 'number') {
                        app.nextDayShippingBadge.orderDeadlineTimer.seconds = s;
                    }

                    app.nextDayShippingBadge.orderDeadlineTimer.set();
                },

                updateSLA: function (isRush) {
                    var earliestAvailableDate;
                    if (typeof isRush !== 'boolean') {
                        isRush = false;
                    }
                    app.nextDayShippingBadge.orderDeadlineTimer.clear();

                    $.ajax({
                        type: 'POST',
                        url: '/Master/GetSLA/',
                        data: { isRush: isRush },
                        success: function (data) {
                            app.nextDayShippingBadge.orderDeadlineTimer.hours = Math.floor(data.TimeTillNextProductionDeadline.TotalHours);
                            app.nextDayShippingBadge.orderDeadlineTimer.minutes = data.TimeTillNextProductionDeadline.Minutes;
                            app.nextDayShippingBadge.orderDeadlineTimer.seconds = data.TimeTillNextProductionDeadline.Seconds;
                            app.nextDayShippingBadge.orderDeadlineTimer.set();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(errorThrown);
                            app.nextDayShippingBadge.orderDeadlineTimer.hours = app.nextDayShippingBadge.orderDeadlineTimer.minutes = app.nextDayShippingBadge.orderDeadlineTimer.seconds = 0;
                        },
                        complete: function (jqXHR, textStatus) {
                            $('#next-day-shipping-container .timer-container .hours').html(app.nextDayShippingBadge.orderDeadlineTimer.hours);
                            $('#next-day-shipping-container .timer-container .minutes').html(app.nextDayShippingBadge.orderDeadlineTimer.minutes);
                            app.nextDayShippingBadge.orderDeadlineTimer.setUnitVisibility();
                        }
                    });
                },

                reset: function () {
                    clearInterval(app.nextDayShippingBadge.orderDeadlineTimer.timeOut);
                    app.nextDayShippingBadge.orderDeadlineTimer.set();
                },

                set: function () {
                    app.nextDayShippingBadge.orderDeadlineTimer.timeOut = setInterval(app.nextDayShippingBadge.orderDeadlineTimer.tick, 1000);
                },

                setUnitVisibility: function () {
                    if (app.nextDayShippingBadge.orderDeadlineTimer.hours > 0) {
                        $('#next-day-shipping-container .timer-container .hoursWrapper').show();
                        $('#next-day-shipping-container .timer-container .minutesWrapper').show();
                        $('#next-day-shipping-container .timer-container .secondsWrapper').hide();
                    } else if (app.nextDayShippingBadge.orderDeadlineTimer.minutes > 0) {
                        $('#next-day-shipping-container .timer-container .hoursWrapper').hide();
                        $('#next-day-shipping-container .timer-container .minutesWrapper').show();
                        $('#next-day-shipping-container .timer-container .secondsWrapper').show();
                    } else {
                        $('#next-day-shipping-container .timer-container .hoursWrapper').hide();
                        $('#next-day-shipping-container .timer-container .minutesWrapper').hide();
                        $('#next-day-shipping-container .timer-container .secondsWrapper').show();
                    }
                },

                tick: function () {
                    app.nextDayShippingBadge.orderDeadlineTimer.seconds--;

                    if (app.nextDayShippingBadge.orderDeadlineTimer.seconds <= 0
                        && app.nextDayShippingBadge.orderDeadlineTimer.minutes <= 0
                        && app.nextDayShippingBadge.orderDeadlineTimer.hours <= 0) {
                        //old SLA has expired get new one
                        app.nextDayShippingBadge.orderDeadlineTimer.updateSLA(true);
                    } else {
                        if (app.nextDayShippingBadge.orderDeadlineTimer.seconds < 0) {
                            app.nextDayShippingBadge.orderDeadlineTimer.minutes--;
                            app.nextDayShippingBadge.orderDeadlineTimer.seconds = 59;

                            if (app.nextDayShippingBadge.orderDeadlineTimer.minutes < 0) {
                                app.nextDayShippingBadge.orderDeadlineTimer.hours--;
                                app.nextDayShippingBadge.orderDeadlineTimer.minutes = 59;

                                $('#next-day-shipping-container .timer-container .hours').html(app.nextDayShippingBadge.orderDeadlineTimer.hours);

                                if (app.nextDayShippingBadge.orderDeadlineTimer.hours <= 0) {
                                    $('#next-day-shipping-container .timer-container .hoursWrapper').hide();
                                    $('#next-day-shipping-container .timer-container .secondsWrapper').show();
                                }
                            }

                            $('#next-day-shipping-container .timer-container .minutes').html(app.nextDayShippingBadge.orderDeadlineTimer.minutes);

                            if (app.nextDayShippingBadge.orderDeadlineTimer.hours <= 0 && app.nextDayShippingBadge.orderDeadlineTimer.minutes <= 0) {
                                $('#next-day-shipping-container .timer-container .minutesWrapper').hide();
                            }
                        }

                        $('#next-day-shipping-container .timer-container .seconds').html(app.nextDayShippingBadge.orderDeadlineTimer.seconds);
                    }
                }
            }
        },

        //#region productQuickStartFormManager
        productQuickStartFormManager: {
            isInitialized: false,
            selectedProduct: {
                width: null,
                height: null,
                quantity: null,
                productID: null,
                productName: null,
                unitPrice: null,
                slashPrice: null,
                percentOff: null,
                options: null,
                update: function (ddl, prodID, prodHeight, prodWidth, isSignUse) {
                    var form = $(ddl).closest('div.product-quick-start-form-container'),
                        prevProdID = $(form).data('productid'),
                        productID = $(ddl).val(),
                        selectedOption = $('option:selected', ddl),
                        productName = selectedOption.text(),
                        materialTypeID = selectedOption !== undefined ? selectedOption.data('materialtypeid') : $(form).data('materialtypeid'),
                        widthInput = $('input[name="Width"]', form).first(),
                        width = widthInput.val(),
                        heightInput = $('input[name="Height"]', form).first(),
                        height = heightInput.val(),
                        quantity = $('input[name="Quantity"]', form).first().val();

                    if (productID == undefined) {
                        productID = prodID;
                        height = prodHeight;
                        width = prodWidth;
                    }

                    $(form).removeClass('product-' + prevProdID).addClass('product-' + productID);
                    $(form).data('productid', productID);
                    $(form).data('producttitle', productName);
                    $(form).data('materialtypeid', materialTypeID);
                    $(form).attr('data-hassetsize', 'False');
                    //$(form).('hassetsize', 'False');

                    var hasSetSize = false;
                    var setSizeHeight = 0;
                    var setSizeWidth = 0;
                    if (productID !== null && productID > 0) {
                        //Get select list for sizes if the material has set sizes
                        $.ajax({
                            url: '/design/GetMaterialProperties',
                            type: 'POST',
                            data: { productID: productID, height: height, width: width, includeOptions: true, isSignUse: isSignUse },
                            success: function (data) {
                                if (data !== null) {
                                    if (data.hasSetSizes) {
                                        //$("#customSizeDiv").hide();
                                        //$("#endUnitsDiv").hide();
                                        //$("#standardSizeHeader").html("Sizes");
                                        hasSetSize = true;
                                        var ddl2 = $('#app-homeStandardSizes');

                                        $(form).attr('data-hassetsize', true);
                                        //var dims = $('.default', ddl2).first().val().split('x');
                                        // var w = parseFloat(dims[0]);
                                        var w = data.defaultWidth;
                                        var w1 = w;
                                        var w2;
                                        //var h = parseFloat(dims[1]);
                                        var h = data.defaultHeight;
                                        var h1 = h;

                                        ddl2.empty();

                                        var dispStr = '';
                                        var valStr = '';
                                        var defaultAssign = false;
                                        $.each(data.setSizes, function () {
                                            valStr = this.InchesWidth + 'x' + this.InchesHeight;
                                            var w2 = this.InchesWidth;
                                            var h2 = this.InchesHeight;

                                            dispStr = w2 + 'in x ' + h2 + 'in';

                                            if (this.FriendlyDescription !== '' && this.FriendlyDescription !== 'StdSize' && this.FriendlyDescription !== null)
                                                dispStr = this.FriendlyDescription;

                                            // the innerHTML.length was 0 on the first option always
                                            // only set default if height and width match
                                            if ((w == w2 && h == h2)) {// || ddl2[0].innerHTML.length == 0) {

                                                ddl2.append('<option value="' + valStr + '" class="default" selected="selected">' + dispStr + '</option>');
                                                setSizeWidth = w2;
                                                $(form).attr('data-setWidth', w2);
                                                setSizeHeight = h2;
                                                $(form).attr('data-setHeight', h2);
                                                defaultAssign = true;

                                            }
                                            else {
                                                ddl2.append('<option value="' + valStr + '">' + dispStr + '</option>');
                                            }

                                        })
                                        //$('.dropdown.ddl-dynamic-app-standardSizes').hide();
                                        var createdDDL = ddl2.createDropDown();

                                        // If no set sizes match the default height/width, select the first by default
                                        if (!defaultAssign) {
                                            createdDDL.children(":first").addClass('default').attr("selected", "selected");
                                            createdDDL.createDropDown();

                                            //Set defaulth height and width to be passed into GetOptionInfo
                                            var dimensions = createdDDL.children(":first").val().split('x');
                                            data.defaultWidth = Number(dimensions[0]);
                                            data.defaultHeight = Number(dimensions[1]);
                                        }

                                        createdDDL.val(createdDDL.find('.default').val());

                                        $(".sizeInput").hide();
                                        // $("#heightSizeInput").hide();
                                        ddl2.show;

                                        // $('#app-homeStandardSizes').change();
                                        //linkSizes.removeClass('linked');
                                    }
                                    else if (data.quickStartMessage !== null) {


                                        $(".quickStartSizeLabel").html(data.quickStartMessage);
                                        $(".quantityLabel").parent().hide();

                                        $(".sizeInput").hide();
                                        $(".priceLabel").hide();
                                        $(".unitPrice").parent().hide();
                                        //$(".sizeInput.qty").attr('style', 'display:none !important;');
                                        $('.dropdown.ddl-dynamic-app-homeStandardSizes').hide();

                                    }
                                    else {
                                        //ddl2.hide();
                                        $('.dropdown.ddl-dynamic-app-homeStandardSizes').hide();
                                        $("#widthSizeInput").show();
                                        $("#heightSizeInput").show();
                                        $(".quantityLabel").show();

                                        $(".quickStartSizeLabel").html("Size (inches):");
                                        $(".sizeInput").show();
                                        $(".priceLabel").show();
                                        $(".unitPrice").parent().show();

                                        if ($(window).width() < 800) {
                                            $(".unitPrice").parent().css("display", "");
                                        }
                                        //$("#customSizeDiv").show();
                                        //$("#endUnitsDiv").show();
                                        //$("#standardSizeHeader").html("Standard Sizes");
                                        //$('.dropdown.ddl-dynamic-app-standardSizes').show();
                                        $(form).attr('data-hassetsize', false);
                                    }

                                    if (data.hasSetQuantity) {

                                        //Update data attribute so that price will be just price and not price each
                                        $(form).data('hassetquantity', 'True');

                                        var ddl3 = $('#app-homeStandardQuantity');
                                        var dims = $('.default', ddl3).first();
                                        //var w = parseFloat(dims[0]);
                                        //var w1 = w;
                                        //var w2;
                                        //var h = parseFloat(dims[1]);
                                        //var h1 = h;
                                        var dispStr = '';
                                        var valStr = '';

                                        ddl3.empty();

                                        $.each(data.setQuantities, function () {
                                            valStr = this.Quantity;

                                            //var w2 = this.InchesWidth;
                                            //var h2 = this.InchesHeight;

                                            dispStr = this.Quantity;

                                            if (this.IsDefaultQuantityForMaterial != null && this.IsDefaultQuantityForMaterial == true) {
                                                ddl3.append('<option value="' + valStr + '" class="default" selected="selected">' + dispStr + '</option>');
                                                quantity = valStr;
                                            }
                                            else {
                                                ddl3.append('<option value="' + valStr + '">' + dispStr + '</option>');
                                            }
                                        });

                                        ddl3.createDropDown();
                                        $(".sizeInput.qty").hide();
                                        ddl3.show;
                                        //$('#app-homeStandardQuantity').change();
                                        $(".product-dropdown-container.Quantity").show();
                                    }
                                    else {
                                        //Update data attribute so that price will be price each
                                        $(form).data('hassetquantity', 'False');

                                        $('.dropdown.ddl-dynamic-app-homeStandardQuantity').hide();
                                        $('div.product-quick-start-form-container input[name="Quantity"]').val(1);
                                        quantity = 1;
                                        if (productID != 39 && productID != 61 && productID != 74 && productID != 88) {
                                            $(".sizeInput.qty").show();
                                        }
                                        $(".product-dropdown-container.Quantity").hide();
                                    }
                                    var configIds = [];

                                    $('option:selected', '.optionConfigurationDDL').each(function (i, o) {
                                        configIds.push($(o).val());
                                    });
                                    $("#designSuite_optionsList").empty();
                                    if (data.options.length > 0) {

                                        $(".show-options").removeClass("hidden");
                                        $(".show-options").off("click");
                                        $(".show-options").on('click', function (event) {
                                            $('.show-options i').toggleClass('fa-chevron-up');
                                            $('.show-options i').toggleClass('fa-chevron-down');
                                            $("#designSuite_optionsList").toggleClass('hidden');
                                            var placeHolderParent = $('.placeholder-sticky').parent();

                                            if ($(window).width() > 800 && (!placeHolderParent.hasClass('bullets-reviews'))) {
                                                $('.placeholder-sticky').height($('.product-quick-start-form-container').parent().outerHeight());
                                            }
                                        });
                                    }
                                    app.productQuickStartFormManager.getOptionInfo(productID, data.defaultHeight, data.defaultWidth, true, quantity, configIds, hasSetSize);

                                    // Change to the default and set revertvalues each time the product is changed
                                    height = app.productQuickStartFormManager.materialTypeDimensionsManager.materialTypeDimensions[materialTypeID].defaultHeight;
                                    width = app.productQuickStartFormManager.materialTypeDimensionsManager.materialTypeDimensions[materialTypeID].defaultWidth;
                                    widthInput.data('revertvalue', width);
                                    heightInput.data('revertvalue', height);

                                    app.productQuickStartFormManager.materialTypeDimensionsManager.revertSizes(widthInput, heightInput, false, data.defaultWidth, data.defaultHeight, defaultAssign);
                                }

                            }
                        });
                    }
                },
                selectedOptions: {
                    // Save options selected in get started so they can be accessed to load options correctly in the design tool.
                    saveSelectedOptions: function () {
                        // Clear this so we don't save extra values when switching products in get started
                        sessionStorage.clear();
                        $('option:selected', '.optionConfigurationDDL').each(function (i, o) {
                            sessionStorage.setItem(o.parentElement.id, o.value);
                            sessionStorage.setItem(o.value, $(o).data('codekey').toUpperCase());
                        });
                    }
                },
                dependencyMap: {
                    //ANY PROPERTY THAT IS NOT A FUNCTION WILL BE REMOVED WHEN THE CLEAR FUNCTION IS INVOKED!

                    //the dependency map will have non-native properties matching the dependent configuration ID
                    //the dependent configuration ID properties are objects containing properties matching the option ID
                    //the option ID properties are an array of required configuration IDs
                    //if the array of required configuration IDs contains a single item, the config dependency is a strict &&
                    //otherwise, the dependency is a || among the members of the array.

                    //example:

                    //{
                    //    2: { //config id 2
                    //        1:[9, 13] //depends on either config 9 or 13 in option 1
                    //    },
                    //    14:{ //config id 14
                    //        5:[5] //depends on config 5 in option 5
                    //    },
                    //    15:{ //config id 15
                    //        5:[7], //depends on config 7 in option 5
                    //        8:[20,21] //and also depends on config 20 or 21 in option 8
                    //    }
                    //}

                    addDependenciesForConfiguration: function (configurationDependencies) {
                        var map = app.productQuickStartFormManager.selectedProduct.dependencyMap;
                        $.each(configurationDependencies, function (i, dependency) {
                            if (map[dependency.DependentDesignOptionConfigurationID] == undefined) {
                                map[dependency.DependentDesignOptionConfigurationID] = {};
                            }

                            if (map[dependency.DependentDesignOptionConfigurationID][dependency.RequiredDesignOptionID] == undefined) {
                                map[dependency.DependentDesignOptionConfigurationID][dependency.RequiredDesignOptionID] = [];
                            }

                            if (map[dependency.DependentDesignOptionConfigurationID][dependency.RequiredDesignOptionID].indexOf(dependency.RequiredDesignOptionConfigurationID) === -1) {
                                map[dependency.DependentDesignOptionConfigurationID][dependency.RequiredDesignOptionID].push(dependency.RequiredDesignOptionConfigurationID);
                            }
                        });
                    },
                    checkDependencies: function (isCalledFromDesignTool, configSelect) {
                        //this should only be run on load and should only matter for legacy designs or when dependencies are changed on our end.
                        var optionDDLs = $('.optionConfigurationDDL');
                        $.each(optionDDLs, function (i, ddl) { //iterate over each option ddl
                            var isDependencyCheckComplete = $(ddl).is('isDependencyCheckComplete');

                            if (!isDependencyCheckComplete) {

                                var selectedConfigID = parseInt($(ddl).val()); //get its selected value
                                var selectedConfigDependencies = app.productQuickStartFormManager.selectedProduct.dependencyMap.getDependenciesByConfigID(selectedConfigID); //and get its dependencies

                                if (selectedConfigDependencies) { //if it has any dependencies
                                    for (var requiredOptionID in selectedConfigDependencies) { //iterate over the required options

                                        var requiredOptionSelect = $('#option-' + requiredOptionID + '-ddl'); //get the ddl of the required option
                                        var requiredOptionSelectedValue = parseInt(requiredOptionSelect.val()); //get the current value of the ddl

                                        if (!$.isNumeric(requiredOptionSelectedValue)) {
                                            return;
                                        }

                                        if (selectedConfigDependencies[requiredOptionID].indexOf(requiredOptionSelectedValue) === -1) { //if the currently selected value is not one of the valid dependency configs...
                                            var defaultConfigForOption = $('option', ddl).first();
                                            var defaultConfigName = defaultConfigForOption.data('name');
                                            var defaultConfigID = parseInt(defaultConfigForOption.val());
                                            var optionName = $(ddl).data('name');
                                            var configName = $('option[value="' + selectedConfigID + '"]', ddl).data('name');
                                            var requiredOptionName = requiredOptionSelect.data('name');
                                            var dependencySelectorMsg = '"' + optionName + ' - ' + configName + '" requires the selection of ';
                                            var conflicts = [];

                                            if (selectedConfigDependencies[requiredOptionID].length > 1) { //if there is more than one viable dependency config for the option...
                                                //present a dialog where they can choose one of the valid dependency configs for the option (radio button)
                                                dependencySelectorMsg += 'one of the following options for "' + requiredOptionName + '":<br><br>'

                                                $.each(selectedConfigDependencies[requiredOptionID], function (i, requiredConfigID) {
                                                    var requiredConfigName = $('option[value="' + requiredConfigID + '"]', ddl).data('name');

                                                    dependencySelectorMsg += '<input type="radio" name="configDependency" value="' + requiredConfigID + '">' + requiredConfigName + '<br>'
                                                });

                                                dependencySelectorMsg += '<br>Please select one of the options above and click "Ok" or click "Reset" to set"' + optionName + '" to "' + defaultConfigName + '".';

                                                var userSelectedConfigID;

                                                $('#dependencySelectorModal').html(dependencySelectorMsg).dialog({
                                                    title: 'Select Option Dependency',
                                                    closeOnEscape: false,
                                                    close: function (event, ui) {
                                                        //restart dependency check...
                                                        app.productQuickStartFormManager.selectedProduct.dependencyMap.checkDependencies(isCalledFromDesignTool);
                                                    },
                                                    open: function (event, ui) {
                                                        $('#input[name="configDependency"]').on('change', function () {
                                                            userSelectedConfigID = parseInt($('input[name="configDependency"]:checked').val());

                                                            //if their choice conflicts with any other options, let them choose the winner. The loser will be set to default (none/standard).
                                                            //otherwise, set the required option ddl to the required config id and move on.

                                                            //requiredOptionSelectedValue
                                                            for (var potentiallyConflictingConfigID in app.productQuickStartFormManager.selectedProduct.dependencyMap) { //iterate through each config that has dependencies...
                                                                if (potentiallyConflictingConfigID != userSelectedConfigID && typeof app.productQuickStartFormManager.selectedProduct.dependencyMap[potentiallyConflictingConfigID] !== 'function') { //filter functions and the config selected by the user

                                                                    for (var potentiallyConflictingOptionID in app.productQuickStartFormManager.selectedProduct.dependencyMap[potentiallyConflictingConfigID]) { //...and then iterate through its option dependencies...
                                                                        if (app.productQuickStartFormManager.selectedProduct.dependencyMap[potentiallyConflictingConfigID][potentiallyConflictingOptionID].indexOf(requiredOptionSelectedValue) > -1) { //...and see if the currently selected value is required

                                                                            //then determine if the config that has the dependency is selected
                                                                            var conflictingSelectedConfig = $('option[value="' + potentiallyConflictingConfigID + '"]:selected', '.optionConfigurationDDL')

                                                                            if (conflictingSelectedConfig.length) {
                                                                                //at this point we know that the currently selected config is dependent upon the one we are changing
                                                                                //so we want to give the customer the choice to keep the current option config or set it to default

                                                                                var conflictingOptionDDL = conflictingSelectedConfig.parent('select');
                                                                                var defaultConfigForPotentiallyConflictingOptionDdl = $('option', conflictingOptionDDL).first();

                                                                                conflicts.push({
                                                                                    optionID: $(conflictingOptionDDL).data('optionid'),
                                                                                    optionName: $(conflictingOptionDDL).data('name'),
                                                                                    configID: potentiallyConflictingConfigID,
                                                                                    configName: conflictingSelectedConfig.data('name'),
                                                                                    defaultConfigID: parseInt(defaultConfigForPotentiallyConflictingOptionDdl.val()),
                                                                                    defaultConfigName: defaultConfigForPotentiallyConflictingOptionDdl.data('name')
                                                                                });
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            if (conflicts.length) {
                                                                //let the customer know
                                                                var conflictMessage = 'This selection will require the following changes to your current option choices:<br><br><ul>';

                                                                $.each(conflicts, function (i, conflict) {
                                                                    conflictMessage += '<li>"' + conflict.optionName + '" will be changed from "' + conflict.configName + '" to "' + conflict.defaultConfigName + '".</li>';
                                                                });

                                                                conflictMessage += '</ul>';

                                                                signsSite.prompts.alert(conflictMessage);
                                                            }
                                                        });
                                                    },
                                                    buttons: [
                                                        {
                                                            text: 'Ok',
                                                            click: function () {
                                                                //set the current ddl to the selected value
                                                                $(ddl).val(userSelectedConfigID).createDivBasedDropDown('updateDisplay');

                                                                //iterate through conflicts and set them to default
                                                                if (conflicts.length) {
                                                                    $.each(conflicts, function (i, conflict) {
                                                                        $('#option-' + conflict.optionID + '-ddl').val(conflict.defaultConfigID).createDivBasedDropDown('updateDisplay');
                                                                    });
                                                                }

                                                                $(this).dialog('close');
                                                            }
                                                        },
                                                        {
                                                            text: 'Reset',
                                                            click: function () {
                                                                //set current option to default config and move on
                                                                $(ddl).val(defaultConfigID).createDivBasedDropDown('updateDisplay');
                                                                $(this).dialog('close');
                                                            }
                                                        }
                                                    ]
                                                });

                                                return false; //break out of the loop of ddls
                                            }
                                            else {
                                                //if the viable dependency config conflicts with any other options, let them choose the winner. The loser will be set to default (none/standard).
                                                //iterate over the ddls that are NOT the current option DDL to see if their currently selected config is dependent upon the one we are changing

                                                var requiredConfigID = selectedConfigDependencies[requiredOptionID][0];
                                                var requiredConfigName = $('option[value="' + requiredConfigID + '"]', requiredOptionSelect).data('name');

                                                dependencySelectorMsg += '"' + requiredOptionName + ' - ' + requiredConfigName + '".<br>';

                                                for (var potentiallyConflictingConfigID in app.productQuickStartFormManager.selectedProduct.dependencyMap) { //iterate through each config that has dependencies...
                                                    if (potentiallyConflictingConfigID != selectedConfigID && typeof app.productQuickStartFormManager.selectedProduct.dependencyMap[potentiallyConflictingConfigID] !== 'function') { //filter functions and the config selected by the user

                                                        for (var potentiallyConflictingOptionID in app.productQuickStartFormManager.selectedProduct.dependencyMap[potentiallyConflictingConfigID]) { //...and then iterate through its option dependencies...
                                                            if (app.productQuickStartFormManager.selectedProduct.dependencyMap[potentiallyConflictingConfigID][potentiallyConflictingOptionID].indexOf(requiredOptionSelectedValue) > -1) { //...and see if the currently selected value is required

                                                                //then determine if the config that has the dependency is selected
                                                                var conflictingSelectedConfig = $('option[value="' + potentiallyConflictingConfigID + '"]:selected', '.optionConfigurationDDL')

                                                                if (conflictingSelectedConfig.length) {
                                                                    //at this point we know that the currently selected config is dependent upon the one we are changing
                                                                    //so we want to give the customer the choice to keep the current option config or set it to default

                                                                    var conflictingOptionDDL = conflictingSelectedConfig.parent('select');
                                                                    var defaultConfigForPotentiallyConflictingOptionDdl = $('option', conflictingOptionDDL).first();

                                                                    conflicts.push({
                                                                        optionID: $(conflictingOptionDDL).data('optionid'),
                                                                        optionName: $(conflictingOptionDDL).data('name'),
                                                                        configID: potentiallyConflictingConfigID,
                                                                        configName: conflictingSelectedConfig.data('name'),
                                                                        defaultConfigID: parseInt(defaultConfigForPotentiallyConflictingOptionDdl.val()),
                                                                        defaultConfigName: defaultConfigForPotentiallyConflictingOptionDdl.data('name')
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                if (conflicts.length) {

                                                    //let the customer choose to override conflicts or sset this to default
                                                    dependencySelectorMsg += '<br>This will also require the following changes to your other current option choices:<br><br><ul>';

                                                    $.each(conflicts, function (i, conflict) {
                                                        dependencySelectorMsg += '<li>"' + conflict.optionName + '" will be changed from "' + conflict.configName + '" to "' + conflict.defaultConfigName + '".</li>';
                                                    });

                                                    dependencySelectorMsg += '</ul>';
                                                }

                                                dependencySelectorMsg += '<br>Clicking "Ok" will apply the changes above. Clicking "Reset" will set ' + optionName + ' to "' + defaultConfigName + '"';

                                                $('#dependencySelectorModal').html(dependencySelectorMsg).dialog({
                                                    title: 'Option Requirement Changes',
                                                    closeOnEscape: false,
                                                    close: function (event, ui) {
                                                        if (isCalledFromDesignTool) {
                                                            signsSite.designTool.design.details.price.update(35);
                                                        }
                                                        else {
                                                            //$('option:selected', 'select.optionConfigurationDDL').first().change();
                                                            console.log('trying to close');
                                                        }
                                                        //restart dependency check
                                                        app.productQuickStartFormManager.selectedProduct.dependencyMap.checkDependencies(isCalledFromDesignTool);
                                                    },
                                                    buttons: [
                                                        {
                                                            text: 'Ok',
                                                            click: function () {
                                                                //set the current ddl to the selected value
                                                                $(requiredOptionSelect).val(requiredConfigID).createDivBasedDropDown('updateDisplay');
                                                                $('#' + requiredOptionSelect[0].id + '2').val(requiredConfigID).createDivBasedDropDown('updateDisplay');

                                                                //iterate through conflicts and set them to default
                                                                $.each(conflicts, function (i, conflict) {
                                                                    $('#option-' + conflict.optionID + '-ddl').val(conflict.defaultConfigID).createDivBasedDropDown('updateDisplay');
                                                                    $('#option-' + conflict.optionID + '-ddl2').val(conflict.defaultConfigID).createDivBasedDropDown('updateDisplay');
                                                                });

                                                                $(this).dialog('close');
                                                                buildQuantityDdl(true);
                                                            }
                                                        },
                                                        {
                                                            text: 'Reset',
                                                            click: function () {
                                                                //set current option to default config and move on
                                                                $(ddl).val(defaultConfigID).createDivBasedDropDown('updateDisplay');
                                                                $('#' + ddl.id + '2').val(defaultConfigID).createDivBasedDropDown('updateDisplay');
                                                                $(this).dialog('close');
                                                            }
                                                        }
                                                    ]
                                                });

                                                return false; //break out of the loop of ddls
                                            }
                                        }
                                    }
                                }

                                $(ddl).data('isDependencyCheckComplete', true);
                            }

                            if (i === optionDDLs.length - 1) {
                                //todo: make this run after all have been updated
                                //app.design.details.price.update();
                                //app.productQuickStartFormManager.getDiscountedUnitPrice(productID, quantity, height, width, formData, function (data) {
                                //    app.productQuickStartFormManager.updateAllFormsForProduct(productID, data.unitPrice, quantity, height, width, false, data.slashedPrice, data.totalPercent, data.showSlashed);
                                //});
                            }
                        });

                        if (!isCalledFromDesignTool) {
                            app.productQuickStartFormManager.selectedProduct.selectedOptions.saveSelectedOptions();
                        }
                    },
                    checkSingleConfigurationDependencies: function (configSelect, isCalledFromDesignTool) {
                        //configSelect should be a DDL
                        var map = app.productQuickStartFormManager.selectedProduct.dependencyMap;
                        var dependentConfigID = parseInt($(configSelect).val());
                        var dependencies = app.productQuickStartFormManager.selectedProduct.dependencyMap.getDependenciesByConfigID(dependentConfigID);

                        if (dependencies !== undefined) {  //if there are any dependencies
                            map.compileDependencyPrompt(configSelect, dependencies, isCalledFromDesignTool);

                        }
                        else {

                            if (isCalledFromDesignTool) {
                                if (signsSite.designTool.design.product.material.isCutVinyl) {
                                    if ($(configSelect.selectedOptions[0]).data('codekey').toUpperCase() == 'BB947098-C302-4F8F-8CC4-EE160A7AECB4') { //reverse weed

                                        signsSite.designTool.config.cutVinyl.reverseWeed = true;
                                        signsSite.designTool.design.canvas.reverseWeed(true);

                                    }
                                    else if ($(configSelect.selectedOptions[0]).data('codekey').toUpperCase() == 'BFAE9A1B-4323-4F0F-AD58-D2402FDEB15E') {
                                        signsSite.designTool.config.cutVinyl.reverseWeed = false;
                                        signsSite.designTool.design.canvas.reverseWeed(false);
                                    }
                                }
                                map.checkAddCustomDrillHolesNote(configSelect);
                                map.uploadCustomOverlayPrompt($(':selected', configSelect).data('codekey').toUpperCase());
                                signsSite.designTool.design.details.price.update(36, configSelect);
                            }
                            else {
                                app.productQuickStartFormManager.selectedProduct.dependencyMap.checkDependencies(isCalledFromDesignTool, configSelect);
                            }

                        }

                        //$('option:selected', 'select.optionConfigurationDDL').first().change();
                    },
                    clear: function () {
                        for (var prop in this) {
                            if (typeof this[prop] !== 'function') {
                                delete this[prop];
                            }
                        }
                    },

                    checkAddCustomDrillHolesNote: function (configSelect) {
                        // Only show custom drilled holes note popup if custom drilled holes was just selected
                        var selectedConfigCodeKey = $(':selected', configSelect).data('codekey').toUpperCase();
                        if (selectedConfigCodeKey == 'D285F7C2-41AD-4136-B893-F3BDC38618B5') {
                            var optionDDLs = $('.optionConfigurationDDL');

                            $.each(optionDDLs, function (i, ddl) { //iterate over each option ddl

                                var optionCodeKey = $(ddl).data('codekey').toUpperCase();
                                var configCodeKey = $(':selected', ddl).data('codekey').toUpperCase();

                                if (optionCodeKey == 'F3CE6B7A-6CF7-4920-B706-8B3536A0F09B' /*optionID == 2*/ /*drilled holes*/ && configCodeKey == 'D285F7C2-41AD-4136-B893-F3BDC38618B5' /*configID == 57*/ /*Custom*/) {
                                    $('#customDrillHolesNoteModal').dialog({
                                        title: 'Custom Drill Hole Note',
                                        closeOnEscape: false,
                                        close: function (event, ui) {

                                        },
                                        open: function (event, ui) {

                                        },
                                        buttons: [
                                            {
                                                text: 'Save',
                                                click: function () {

                                                    if ($("#txtDrillHoleNote").val() === '' || $("#txtDrillHoleNote").val() === undefined) {
                                                        var input = $("#txtDrillHoleNote");
                                                        var contentValue = input.val();
                                                        if (contentValue) {
                                                            input.removeClass("invalid").addClass("valid");
                                                            signsSite.designTool.design.details.price.update(37);
                                                            $('#customNote_changesSavedDiv').align({ container: '#app-paper' }).fadeIn(800).delay(500).fadeOut(2000);
                                                        }
                                                        else {
                                                            input.removeClass("valid").addClass("invalid");
                                                        }
                                                    }
                                                    else {
                                                        $(this).dialog('close');
                                                        $('#customNote_changesSavedDiv').align({ container: '#app-paper' }).fadeIn(800).delay(500).fadeOut(2000);
                                                    }
                                                }
                                            },
                                            {
                                                text: 'Cancel',
                                                click: function () {

                                                    $("#txtDrillHoleNote").val('');
                                                    //$(configSelect).val(revertValue);
                                                    //$(configSelect).createDivBasedDropDown('updateDisplay');
                                                    $(this).dialog('close');
                                                }
                                            }
                                        ]
                                    });

                                    return false; //break out of the loop of ddls
                                }
                            })
                        }
                    },

                    uploadCustomOverlayPrompt: function (selectedConfigCodeKey, optionDDLsExist) {
                        if (typeof optionDDLsExist !== 'boolean') { optionDDLsExist = true; }
                        var showCustomModal = false;
                        var showCustomWithBorderModal = false;
                        var modalWidth = 600;
                        if ($('#deviceType').html().trim() === "Smartphone") {
                            modalWidth = 400;
                        }

                        if (optionDDLsExist === true) {
                            // Only show custom signShape popup if custom signShape was just selected
                            if (selectedConfigCodeKey == 'C8307B36-AF45-43B6-BAAE-C83D880EF6C7' || selectedConfigCodeKey == '3DC6E204-3E52-4BEB-9CF7-1F3EE23036A4' || selectedConfigCodeKey == 'D04C1D10-9896-41C9-B80E-981414865F6A') {
                                var optionDDLs = $('.optionConfigurationDDL');

                                $.each(optionDDLs, function (i, ddl) { //iterate over each option ddl
                                    var optionCodeKey = $(ddl).data('codekey').toUpperCase();
                                    var configCodeKey = $(':selected', ddl).data('codekey').toUpperCase();

                                    if (optionCodeKey == 'F5586631-7CC1-4EFA-A1B6-A3F3B2225918' /*sign shape*/ && (configCodeKey == 'C8307B36-AF45-43B6-BAAE-C83D880EF6C7')  /*Custom*/) {
                                        showCustomModal = true;
                                        return false; //break out of the loop of ddls
                                    }

                                    if (optionCodeKey == '17A166B6-B000-40EE-AADB-9525E54E1CD1' /*shape*/ && (configCodeKey == 'D04C1D10-9896-41C9-B80E-981414865F6A')  /*Custom*/) {
                                        showCustomModal = true;
                                        return false; //break out of the loop of ddls
                                    }

                                    if (optionCodeKey == 'F5586631-7CC1-4EFA-A1B6-A3F3B2225918' /*sign shape*/ && (configCodeKey == '3DC6E204-3E52-4BEB-9CF7-1F3EE23036A4')  /*Custom shape with border*/) {
                                        showCustomWithBorderModal = true;
                                        return false; //break out of the loop of ddls
                                    }
                                })
                            }
                        }
                        else if (optionDDLsExist === false) {
                            if (selectedConfigCodeKey === 'C8307B36-AF45-43B6-BAAE-C83D880EF6C7' || selectedConfigCodeKey === 'D04C1D10-9896-41C9-B80E-981414865F6A') {
                                showCustomModal = true;
                            }
                            else if (selectedConfigCodeKey === '3DC6E204-3E52-4BEB-9CF7-1F3EE23036A4') {
                                showCustomWithBorderModal = true;
                            }
                        }

                        // Show appropriate modal here
                        var openModalValue = $('#openCustomShapeModal');
                        if (showCustomModal) {
                            signsSite.designTool.config.customShapeFileUploaded = false;
                            openModalValue.val('withoutBorder');
                            $('#customShapeDialog').dialog({
                                title: 'Custom Shape',
                                width: modalWidth,
                                closeOnEscape: false,
                                close: function (event, ui) {
                                    $('.jsCloseCustomUploadDialog').click();
                                },
                                open: function (event, ui) {
                                    $('#customShapeDialog .uploadBtnText').html('').html('Upload My Custom Shape File');
                                },
                            });
                        }
                        else if (showCustomWithBorderModal) {
                            signsSite.designTool.config.customShapeFileUploaded = false;
                            openModalValue.val('withBorder');
                            $('#customShapeWithBorderDialog').dialog({
                                title: 'Custom Shape With Border',
                                width: modalWidth,
                                closeOnEscape: false,
                                close: function (event, ui) {
                                    $('.jsCloseCustomUploadDialog').click();
                                },
                                open: function (event, ui) {
                                    $('#customShapeWithBorderDialog .uploadBtnText').html('').html('Upload My Custom Shape File');
                                },
                            });
                        }
                    },

                    compileDependencyPrompt: function (configSelect, dependencies, isCalledFromDesignTool) {
                        var msg = null;
                        var requiredOptionChanges = null;
                        var revertValue = $(configSelect).data('revertValue');
                        var optionid = $(configSelect).data('optionid');

                        for (var requiredOption in dependencies) {
                            //get the currently selected value of the dependency dropdown
                            var requiredOptionSelect = $('#option-' + requiredOption + '-ddl');

                            if (requiredOptionSelect.length) { //if the required option is not there, it is not enabled for the product so the dependency is not valid.

                                var requiredOptionSelectedValue = parseInt(requiredOptionSelect.val());

                                if (dependencies[requiredOption].indexOf(requiredOptionSelectedValue) === -1) //if the currently selected value is not one of the valid dependencies...
                                {
                                    if (requiredOptionChanges === null) { //if this is the first unmatched dependency, init msg and requiredOptionChanges;
                                        //msg = 'Your selection will require the following additional changes:<br /><br /><strong>Add:</strong><br /><ol>';

                                        requiredOptionChanges = {
                                            dependentOption: { id: $(configSelect).data('optionid'), name: $(configSelect).data('name') },
                                            dependentConfig: { id: parseInt($(configSelect).val()), name: $(':selected', configSelect).data('name') },
                                            unmatchedDependencies: [],
                                            deselections: []
                                        };
                                    }

                                    //add requiredOptionChanges
                                    var requiredOptionObject = {
                                        id: requiredOption,
                                        name: requiredOptionSelect.data('name'),
                                        requiredConfigs: []
                                    }

                                    $.each(dependencies[requiredOption], function (i, val) {
                                        requiredOptionObject.requiredConfigs.push({
                                            id: val,
                                            name: $('option[value="' + val + '"]', requiredOptionSelect).data('name')
                                        });
                                    });

                                    requiredOptionChanges.unmatchedDependencies.push(requiredOptionObject);
                                }
                            }
                        }
                        // requiredOptionChanges will not be null if the currently selected value is not a valid dependency
                        if (requiredOptionChanges != null) {
                            //find out if the previously selected option configuration (revertValue) is a dependency of any other selected option
                            //and add it to requiredOptionChanges.deselections
                            for (var dependentConfigID in app.productQuickStartFormManager.selectedProduct.dependencyMap) { //iterate through each config that has dependencies...
                                if (typeof app.productQuickStartFormManager.selectedProduct.dependencyMap[dependentConfigID] !== 'function') {

                                    for (var requiredOptionID in app.productQuickStartFormManager.selectedProduct.dependencyMap[dependentConfigID]) { //...and then iterate through its option dependencies...
                                        if (app.productQuickStartFormManager.selectedProduct.dependencyMap[dependentConfigID][requiredOptionID].indexOf(revertValue) > -1) { //...and see if the currently selected value is in there

                                            //at this point we can see that the previously selected value was required for dependentConfigID
                                            //now we have to see if dependentConfigID is selected in its option DDL

                                            //get the element matching the dependent config if it is selected
                                            var dependentConfigElement = $('option[value="' + dependentConfigID + '"]:selected', '#designSuite_optionsList');

                                            if (dependentConfigElement.length) { //if it is selected, add it to the deselections
                                                var dependentConfigOptionDDL = dependentConfigElement.parent();

                                                requiredOptionChanges.deselections.push({
                                                    optionID: dependentConfigOptionDDL.data('optionid'),
                                                    optionName: dependentConfigOptionDDL.data('name'),
                                                    configID: dependentConfigID,
                                                    configName: dependentConfigElement.data('name')
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        }


                        if (requiredOptionChanges != null) {
                            msg = 'Selecting "' + requiredOptionChanges.dependentOption.name + ':  ' + requiredOptionChanges.dependentConfig.name + '" will require the following changes to your configuration:';

                            if (requiredOptionChanges.unmatchedDependencies.length) {
                                msg += '<br /><br /><strong>Additions:</strong><br /><ol>';

                                $.each(requiredOptionChanges.unmatchedDependencies, function (i, val) {
                                    val.name = val.id == 34 ? "Drilled Holes Diameter" : val.name;
                                    msg += '<li>' + val.name;

                                    if (val.requiredConfigs.length > 1) {
                                        msg += ' (one of the following):<br /><ul>'

                                        $.each(val.requiredConfigs, function (i, val) {
                                            var defaultSelectionMessage = i == 0 ? ' (selected)' : '';
                                            msg += '<li>' + val.name + defaultSelectionMessage + '</li>';
                                        });

                                        msg += '</ul>'
                                    }
                                    else {
                                        msg += ':  ' + val.requiredConfigs[0].name;
                                    }

                                    msg += '</li>';
                                });

                                msg += '</ol>';
                            }

                            if (requiredOptionChanges.deselections.length) {
                                msg += '<br /><br /><strong>Removals:</strong><br /><ol>';

                                $.each(requiredOptionChanges.deselections, function (i, val) {
                                    val.optionName = val.optionID == 34 ? "Drilled Holes Diameter" : val.optionName;

                                    msg += '<li>' + val.optionName + ':  ' + val.configName + '</li>';
                                });

                                msg += '</ol>';
                            }

                            msg += '<br /><br />Apply these changes?';

                            signsSite.prompts.confirm(msg,
                                function () {
                                    //deselect conflicting options
                                    $.each(requiredOptionChanges.deselections, function (i, val) {
                                        var revertOptionSelect = $('#option-' + val.optionID + '-ddl');
                                        var revertConfigID = parseInt($('option', revertOptionSelect).first().val());

                                        //select the default value
                                        revertOptionSelect.val(revertConfigID).data('revertValue', revertConfigID);
                                        revertOptionSelect.createDivBasedDropDown('updateDisplay');
                                        //need to update 2nd group of options as well
                                        var revertOptionSelect2 = $('#option-' + val.optionID + '-ddl2');


                                        //select the default value
                                        revertOptionSelect2.val(revertConfigID).data('revertValue', revertConfigID);
                                        revertOptionSelect2.createDivBasedDropDown('updateDisplay');
                                    });

                                    //select required options
                                    $.each(requiredOptionChanges.unmatchedDependencies, function (i, val) {
                                        var requiredOptionSelect = $('#option-' + val.id + '-ddl');

                                        var firstRequiredConfig = val.requiredConfigs[0];
                                        requiredOptionSelect.val(firstRequiredConfig.id).data('revertValue', firstRequiredConfig.id);
                                        requiredOptionSelect.createDivBasedDropDown('updateDisplay');

                                        var requiredOptionSelect2 = $('#option-' + val.id + '-ddl2');
                                        requiredOptionSelect2.val(firstRequiredConfig.id).data('revertValue', firstRequiredConfig.id);
                                        requiredOptionSelect2.createDivBasedDropDown('updateDisplay');

                                        if (firstRequiredConfig.name == 'Custom') {
                                            $('#customDrillHolesNoteModal').dialog({
                                                title: 'Custom Drill Hole Note',
                                                closeOnEscape: false,
                                                close: function (event, ui) {

                                                },
                                                open: function (event, ui) {

                                                },
                                                buttons: [
                                                    {
                                                        text: 'Save',
                                                        click: function () {

                                                            if ($("#txtDrillHoleNote").val() === '' || $("#txtDrillHoleNote").val() === undefined) {
                                                                var input = $("#txtDrillHoleNote");
                                                                var contentValue = input.val();
                                                                if (contentValue) {
                                                                    input.removeClass("invalid").addClass("valid");
                                                                    if (isCalledFromDesignTool) {
                                                                        signsSite.designTool.design.details.price.update(38);
                                                                    }
                                                                }
                                                                else {
                                                                    input.removeClass("valid").addClass("invalid");
                                                                }
                                                            }
                                                            else {
                                                                $(this).dialog('close');
                                                            }
                                                        }
                                                    },
                                                    {
                                                        text: 'Cancel',
                                                        click: function () {
                                                            $("#txtDrillHoleNote").val('');
                                                            $(this).dialog('close');
                                                        }
                                                    }
                                                ]
                                            });
                                        }
                                    });

                                    $('option:selected', 'select.optionConfigurationDDL').first().change();
                                    if (isCalledFromDesignTool) {
                                        // If the select option is custom or custom with border we need to display the custom shape modal popup
                                        // Bug 1909, the modal wouldn't appear if option had dependencies. Need this here because they only want it to appear after user chooses to yes to dependency prompt -Kevin 1/31/19
                                        app.productQuickStartFormManager.selectedProduct.dependencyMap.uploadCustomOverlayPrompt($(':selected', configSelect).data('codekey').toUpperCase());

                                        signsSite.designTool.design.details.price.update(29);
                                    }
                                    if (!isCalledFromDesignTool) {
                                        app.productQuickStartFormManager.selectedProduct.dependencyMap.checkDependencies(isCalledFromDesignTool);
                                    }
                                },
                                function () {
                                    // Don't use $(configSelect) anymore, we call getOptionInfo in the onchange method for optionConfigurationDDL, which resets the JQuery assigned data for $(configSelect)
                                    var configSelect = $('#option-' + optionid + '-ddl');

                                    configSelect.val(revertValue).data('revertValue', revertValue);
                                    configSelect.createDivBasedDropDown('updateDisplay');

                                    //Update 2nd group of options in design tool
                                    var configSelect2ID;
                                    if (configSelect.attr('id').slice(-1) == "2") {
                                        configSelect2ID = configSelect.attr('id').slice(0, -1)
                                    }
                                    else {
                                        configSelect2ID = configSelect.attr('id') + "2";
                                    }

                                    var configSelect2 = $('#' + configSelect2ID);
                                    configSelect2.val(revertValue);
                                    configSelect2.createDivBasedDropDown('updateDisplay');

                                    $('option:selected', 'select.optionConfigurationDDL').first().change();
                                    if (!isCalledFromDesignTool) {
                                        app.productQuickStartFormManager.selectedProduct.dependencyMap.checkDependencies(isCalledFromDesignTool);
                                    }
                                }
                            );
                        }
                        else {
                            if (isCalledFromDesignTool) {
                                // If the select option is custom or custom with border we need to display the custom shape modal popup
                                // Bug 1909, the modal wouldn't appear if option had dependencies. Need this here when dependencies exists, but there are no requiredOptionChanges -Kevin 2/4/19
                                app.productQuickStartFormManager.selectedProduct.dependencyMap.uploadCustomOverlayPrompt($(':selected', configSelect).data('codekey').toUpperCase());

                                signsSite.designTool.design.details.price.update(30);
                            }
                            if (!isCalledFromDesignTool) {
                                app.productQuickStartFormManager.selectedProduct.dependencyMap.checkDependencies(isCalledFromDesignTool);
                            }
                        }
                    },
                    getDependenciesByConfigID: function (configID) {
                        return app.productQuickStartFormManager.selectedProduct.dependencyMap[configID];
                    }
                }
            },
            search: {
                pager: {
                    currentPage: 1,
                    pageCount: 1,
                    parent: null,
                    pageIndicator: null,
                    buttons: {
                        all: null,
                        forward: $('<div class="circled-chevron gallery-nav right hidden"></div>'),
                        back: $('<div class="circled-chevron gallery-nav left hidden"></div>'),
                        init: function (parent) {
                            app.productQuickStartFormManager.search.pager.buttons.all = app.productQuickStartFormManager.search.pager.buttons.forward.add(app.productQuickStartFormManager.search.pager.buttons.back);
                            app.productQuickStartFormManager.search.pager.parent = parent;
                            parent.append(app.productQuickStartFormManager.search.pager.buttons.all);
                            app.productQuickStartFormManager.search.pager.pageIndicator = $('div.page-indicator', parent);

                            //init events
                            app.productQuickStartFormManager.search.pager.buttons.forward.on('click', function (event) {
                                var nextPage = app.productQuickStartFormManager.search.pager.currentPage + 1;
                                var inbound = $('ul.product-quick-start-search-results-page.page-' + nextPage, app.productQuickStartFormManager.search.pager.parent).first();

                                if (!inbound.length) {
                                    var form = $('.product-quick-start-search-form.new-search', app.productQuickStartFormManager.search.pager.parent).first();
                                    $(form).data('lastSearch', null);
                                    app.productQuickStartFormManager.search.submit(form, nextPage);
                                } else {
                                    var outbound = $('ul.product-quick-start-search-results-page.page-' + app.productQuickStartFormManager.search.pager.currentPage, app.productQuickStartFormManager.search.pager.parent).first();
                                    app.slideReplace(outbound, inbound, {
                                        direction: 'left',
                                        callback: function () {
                                            app.productQuickStartFormManager.search.pager.update(nextPage);
                                        }
                                    });
                                }
                            });

                            app.productQuickStartFormManager.search.pager.buttons.back.on('click', function (event) {
                                var prevPage = app.productQuickStartFormManager.search.pager.currentPage - 1;
                                var inbound = $('ul.product-quick-start-search-results-page.page-' + prevPage, app.productQuickStartFormManager.search.pager.parent).first();
                                var outbound = $('ul.product-quick-start-search-results-page.page-' + app.productQuickStartFormManager.search.pager.currentPage, app.productQuickStartFormManager.search.pager.parent).first();

                                app.slideReplace(outbound, inbound, {
                                    direction: 'right',
                                    callback: function () {
                                        app.productQuickStartFormManager.search.pager.update(prevPage);
                                    }
                                });
                            });
                        }
                    },
                    hide: function () {
                        app.productQuickStartFormManager.search.pager.buttons.all.hide();
                    },
                    update: function (page, pages) {
                        if (typeof page === 'number') {
                            app.productQuickStartFormManager.search.pager.currentPage = page;
                        }
                        if (typeof pages === 'number') {
                            app.productQuickStartFormManager.search.pager.pageCount = pages;
                        }

                        $('.current-page', app.productQuickStartFormManager.search.pager.pageIndicator).text(app.productQuickStartFormManager.search.pager.currentPage);
                        $('.page-count', app.productQuickStartFormManager.search.pager.pageIndicator).text(app.productQuickStartFormManager.search.pager.pageCount);

                        if (app.productQuickStartFormManager.search.pager.currentPage < app.productQuickStartFormManager.search.pager.pageCount) {
                            app.productQuickStartFormManager.search.pager.buttons.forward.show();
                        } else {
                            app.productQuickStartFormManager.search.pager.buttons.forward.hide();
                        }

                        if (app.productQuickStartFormManager.search.pager.currentPage > 1) {
                            app.productQuickStartFormManager.search.pager.buttons.back.show();
                        } else {
                            app.productQuickStartFormManager.search.pager.buttons.back.hide();
                        }
                    }
                },
                submit: function (form, page) {
                    var prod = app.productQuickStartFormManager.selectedProduct,
                        searchTextInput = $('input[name="searchText"]', form),
                        searchText = searchTextInput.val(),
                        middle = $(form).closest('.middle-wrapper'),
                        searchWrapper = $('.template-search-wrapper', middle).first(),
                        results = $('.product-quick-start-search-results', searchWrapper).first(),
                        submitData = {
                            searchText: encodeURIComponent(searchText),
                            productID: prod.productID,
                            productName: prod.productName,
                            width: prod.width,
                            height: prod.height,
                            quantity: prod.quantity,
                            page: page,
                            pageSize: 12
                        },
                        resultContent;

                    if ($(form).data('lastSearch') !== searchText) {
                        app.spinner.show({ container: middle, delay: 500 });

                        $('.product-quick-start-search-form.new-search').data('lastSearch', searchText);

                        //set same value on all other forms
                        $('.product-quick-start-search-form input[name="searchText"]').not(searchTextInput).val(searchText);

                        $.post('/Template/ProductQuickStartSearch/', submitData, function (data) {
                            app.spinner.hide();

                            if (data.links.length > 0) {
                                resultContent = $('<ul class="product-quick-start-search-results-page page-' + page + '">');
                                resultContent.html($('#product-quick-start-search-result-template').tmpl(data));
                            } else {
                                resultContent = $('<p class="no-results">No results found for "' + searchText + '"</p>');
                            }

                            if (page == 1) {
                                if ($(form).is('.next-steps-search')) {
                                    results.html(resultContent);

                                    var outbound = $(form).closest('.next-steps-wrapper');
                                    var inbound = searchWrapper;

                                    app.slideReplace(outbound, inbound, {
                                        direction: 'left',
                                        callback: function (processData) {
                                            processData.parentElem.animate({ height: inbound.height() }, function () {
                                                app.productQuickStartFormManager.search.pager.update(page, data.pageCount);
                                                $(app.productQuickStartFormManager.modals.step1.modal).dialog('option', 'position', 'center');
                                            });
                                        }
                                    });
                                } else {
                                    var outbound = results;
                                    var inbound = $('<div class="product-quick-start-search-results">').html(resultContent);

                                    app.slideReplace(outbound, inbound, {
                                        direction: 'left',
                                        callback: function (processData) {
                                            app.productQuickStartFormManager.search.pager.update(page, data.pageCount);
                                            outbound.remove();
                                        }
                                    });
                                }
                            } else {
                                var outbound = $('ul.product-quick-start-search-results-page.page-' + (page - 1), results).first();
                                var inbound = resultContent;

                                app.slideReplace(outbound, inbound, {
                                    direction: 'left',
                                    callback: function (processData) {
                                        app.productQuickStartFormManager.search.pager.update(page, data.pageCount);
                                    }
                                });
                            }
                        });
                    }
                }
            },
            modals: {
                init: function () {
                    app.productQuickStartFormManager.modals.step1.init();
                },

                step1: {
                    elem: null,
                    modal: null,

                    close: function () {
                        app.productQuickStartFormManager.modals.step1.modal.dialog('close');
                    },

                    init: function () {
                        app.productQuickStartFormManager.modals.step1.elem = $('.product-quick-start-form-step-1-modal').first();
                        $('.product-quick-start-form-step-1-modal').not(app.productQuickStartFormManager.modals.step1.elem).remove();

                        app.productQuickStartFormManager.modals.step1.modal = app.productQuickStartFormManager.modals.step1.elem.dialog({
                            autoOpen: false,
                            dialogClass: 'product-quick-start-form-step-1-dialog',
                            modal: true,
                            width: 954,
                            resizable: false,
                            create: function (event, ui) {
                                app.productQuickStartFormManager.search.pager.buttons.init($(event.target).parent());
                            }
                        });
                    },
                    show: function (button) {
                        var form = $(button).closest('div.product-quick-start-form-container'),
                            width = $('input[name="Width"]', form).first().val(),
                            height = $('input[name="Height"]', form).first().val(),
                            quantity = $('input[name="Quantity"]', form).first().val(),
                            unitPrice = $('span.unitPrice', form).first().text(),
                            slashPrice = $('span.slashedPrice', form).first().text();

                        app.productQuickStartFormManager.selectedProduct.width = width;
                        app.productQuickStartFormManager.selectedProduct.height = height;
                        app.productQuickStartFormManager.selectedProduct.quantity = quantity;
                        app.productQuickStartFormManager.selectedProduct.productID = $(form).data('productid');
                        app.productQuickStartFormManager.selectedProduct.productName = $(form).data('producttitle');
                        app.productQuickStartFormManager.selectedProduct.unitPrice = unitPrice;
                        app.productQuickStartFormManager.selectedProduct.slashPrice = slashPrice;
                        $('.width', app.productQuickStartFormManager.modals.step1.modal).text(width);
                        $('.height', app.productQuickStartFormManager.modals.step1.modal).text(height);
                        $('.quantity', app.productQuickStartFormManager.modals.step1.modal).text(quantity);
                        $('.unitPrice', app.productQuickStartFormManager.modals.step1.modal).text(unitPrice);
                        $('.slashedPrice', app.productQuickStartFormManager.modals.step1.modal).text(slashPrice);



                        if ($('#deviceType').html().trim() === "Smartphone") {
                            window.location =
                                '/MobileGetStartedLanding/?productID=' + app.productQuickStartFormManager.selectedProduct.productID +
                                '&width='  + app.productQuickStartFormManager.selectedProduct.width +
                                '&height=' + app.productQuickStartFormManager.selectedProduct.height +
                                '&qty='    + app.productQuickStartFormManager.selectedProduct.quantity +

                                window.options;

                        }
                        else {
                            if (app.productQuickStartFormManager.selectedProduct.productName == 'Vinyl Lettering' || app.productQuickStartFormManager.selectedProduct.productName == 'A-Frame Signs'
                                || app.productQuickStartFormManager.selectedProduct.productName == 'Retractable Banners') {
                                var prod = app.productQuickStartFormManager.selectedProduct;
                                window.location = '/design/?productID=' + prod.productID + '&width=' + prod.width + '&height=' + prod.height + '&quantity=' + prod.quantity;

                            }
                            else {
                                if ($('span.percentOff', form).first().is(':visible')) {
                                    $('.slashedPrice', app.productQuickStartFormManager.modals.step1.modal).show();
                                    $('.percentOff', app.productQuickStartFormManager.modals.step1.modal).show();
                                }
                                else {
                                    $('.slashedPrice', app.productQuickStartFormManager.modals.step1.modal).hide();
                                    $('.percentOff', app.productQuickStartFormManager.modals.step1.modal).hide();
                                }
                                app.productQuickStartFormManager.modals.step1.modal.dialog('open');
                            }
                        }

                    }
                },
            },
            typingTimer: {
                obj: null,
                timeout: 2500
            },
            getOptionInfo: function (productID, height, width, includeOptions, quantity, configIds, hasSetSize) {
                $.ajax({
                    type: 'POST',
                    url: '/design/GetOptionInfo/',
                    data: {
                        productID: productID,
                        height: height,
                        width: width,
                        includeOptions: includeOptions,
                        quantity: quantity,
                        selectedConfigurationIDs: configIds
                    },
                    success: function (data) {
                        // For suboptions, if we select the main option we want to show this DDL to easily select the suboption next
                        var showDDL = false;
                        var showDDL2 = false;
                        if ($('.js_subOptions').is(':visible')) {
                            if ($($('.js_subOptions:visible')[0].closest('div.dropdown')).attr('class').split('-')[4] == 'ddl2') {
                                showDDL2 = true;
                            }
                            else {
                                showDDL = true;
                            }
                        }

                        $('#designSuite_optionsList').empty();
                        $('#rowOptions').empty();
                        signsSite.productQuickStartFormManager.selectedProduct.dependencyMap.clear();
                        if (data.previouslySelectedConfigsWhoseSizeRequirementsAreNotMet) {
                            //alert user and have them review options.
                            var innerMsg = '';
                            var msg = $('<div>');
                            msg.append('<p>The following option(s) that you had previously selected are not compatible with the current size of your sign:</p><br>');

                            var ul = $('<ul>');
                            $.each(data.previouslySelectedConfigsWhoseSizeRequirementsAreNotMet, function (i, item) {
                                ul.append('<li>' + item + '</li>');
                            });
                            if (productID === "97") {
                                innerMsg = "<br><p>We've automatically changed the number of poles on each side to fit the height of your banner. If you have your own poles please select 'None' in the Poles option dropdown.</p>";
                            } else {
                                innerMsg = '<br><p>Please review your selected options before continuing.</p>';
                            }
                            msg.append(ul).append(innerMsg);

                            signsSite.prompts.alert(msg);
                        }

                        // Sort to make sure Drilled Holes Diameter comes in the list before Drilled Holes Location
                        data.options.sort(function (a, b) {
                            if (a.DesignOptionID == 34 && b.DesignOptionID == 2)
                                return b.DesignOptionID - a.DesignOptionID;
                            if (b.DesignOptionID == 34 && a.DesignOptionID == 2)
                                return b.DesignOptionID - a.DesignOptionID;
                            if (b.DesignOptionID == 34 && a.DesignOptionID == 2)
                                return b.DesignOptionID - a.DesignOptionID;
                        });

                        $.each(data.options, function (i, option) {
                            var rowOptions = $('<div class="col-5-lg-1 col-sm-6"><div class="form-group">');
                            var li = $('<div class="tools-Sign-Options">');
                            var toolsOptions = $('<div class="form-group">')

                            if (option.DesignOptionID == 34) {
                                option.Name = "Drilled Holes";
                            }

                            var titleSpan = $('<span class="optionName">');
                            titleSpan.html(option.Name);

                            var titleSpan2 = $('<span class="optionName">');
                            titleSpan2.html(option.Name);

                            var titleSpan3 = $('<span class="optionName">');
                            titleSpan3.html(option.Name);

                            if (option.DesignOptionID != 2) {
                                $(li).append(titleSpan);
                                $(rowOptions).append(titleSpan2);
                                $(toolsOptions).append(titleSpan3);
                            }

                            var ddl = $('<select class="optionConfigurationDDL">');
                            ddl.data('name', option.Name);
                            ddl.data('optionid', option.DesignOptionID);
                            ddl.data('codekey', option.CodeKey);
                            ddl.attr('id', 'option-' + option.DesignOptionID + '-ddl');
                            ddl.addClass('codekey-' + option.CodeKey.toUpperCase());

                            var ddl2 = $('<select class="optionConfigurationDDL">');
                            ddl2.data('name', option.Name);
                            ddl2.data('optionid', option.DesignOptionID);
                            ddl2.data('codekey', option.CodeKey);
                            ddl2.attr('id', 'option-' + option.DesignOptionID + '-ddl2');
                            ddl2.addClass('codekey-' + option.CodeKey.toUpperCase());
                            var selectedConfigID = null;


                            $.each(option.DesignOptionConfigurations, function (i, config) {
                                app.productQuickStartFormManager.selectedProduct.dependencyMap.addDependenciesForConfiguration(config.ConfigurationDependencies);
                                var selectOption = $('<option>');
                                selectOption.data('name', config.Name);
                                selectOption.data('fullname', option.Name + ': ' + config.Name);
                                selectOption.data('codekey', config.CodeKey);
                                selectOption.data('hasSetMinSize', config.OptionHasSetSizes);
                                selectOption.data('hasSetMinQuantity', config.IsSignTypeHasSetQuantity);
                                selectOption.data('isQuickStart', data.isQuickStart);
                                selectOption.data('price', config.Price);
                                selectOption.data('pricepersquareinch', config.PricePerSquareInch);
                                selectOption.data('pricingmultipliertype', config.PricingMultiplierType);
                                selectOption.data('currentProductBindingID', config.CurrentProductBindingID);
                                selectOption.data('preDiscountBasePrice', config.PreDiscountBasePrice);
                                var selectOption2 = $('<option>');
                                selectOption2.data('name', config.Name);
                                selectOption2.data('codekey', config.CodeKey);
                                selectOption2.data('hasSetMinSize', config.OptionHasSetSizes);
                                selectOption2.data('hasSetMinQuantity', config.IsSignTypeHasSetQuantity);
                                selectOption2.data('isQuickStart', data.isQuickStart);
                                selectOption2.data('price', config.Price);
                                selectOption2.data('pricepersquareinch', config.PricePerSquareInch);
                                selectOption2.data('pricingmultipliertype', config.PricingMultiplierType);
                                selectOption2.data('currentProductBindingID', config.CurrentProductBindingID);
                                selectOption2.data('preDiscountBasePrice', config.PreDiscountBasePrice);
                                var optionhtml = $('<div>');
                                var nameSpan = '<span class="optionText optionInfo" >' + config.Name + '</span>';

                                //optionhtml.append(nameSpan);

                                var toast = $('<div class="productOptionInfo" >');
                                var imageHtml = '';

                                if (config.Image !== null && typeof config.Image.Url === 'string') {
                                    imageHtml = '<div class="optionImg"><img style="position:relative;top:0;float:left; max-width:60px;" src="' + config.Image.Url + '"  alt="' + config.Image.DisplayName + '" title="' + config.Image.DisplayName + '" /></div>'
                                }

                                var toastHtml = imageHtml;
                                toastHtml += '<div class="optionTextOuter"> ';
                                toastHtml += nameSpan;
                                if (!String.IsNullOrWhiteSpace(config.Description)) {
                                    toastHtml += '<div class="optionText configDescrip"><p>' + config.Description + '</p>'
                                }

                                if (config.FormattedUnitPriceDeltaFromSelectedConfig && option.DesignOptionID != 34) {
                                    var priceSpan = '<span class="priceSpan text-success">' + '(' + config.FormattedUnitPriceDeltaFromSelectedConfig + ')' + '</div>';
                                    // optionhtml.append(priceSpan );
                                }
                                //}

                                if (priceSpan !== undefined) {
                                    if (productID == 39 || productID == 61 || productID == 74 || productID == 88) {
                                        if (config.CodeKey !== 'bfae9a1b-4323-4f0f-ad58-d2402fdeb15e' && config.CodeKey !== 'bb947098-c302-4f8f-8cc4-ee160a7aecb4') {
                                            toastHtml += priceSpan;
                                        }
                                    }
                                    else {
                                        toastHtml += priceSpan;
                                    }
                                }
                                toastHtml += '<br class="clear_0" />' +
                                    '</div></div>';

                                toast.html(toastHtml);

                                optionhtml.append(toast);

                                var newPrice = 0.0;

                                if (config.IsSelected) {
                                    selectedConfigID = parseInt(config.DesignOptionConfigurationID);
                                    selectOption.prop('selected', true);
                                    selectOption2.prop('selected', true);
                                    ddl.data('revertValue', config.DesignOptionConfigurationID);
                                    ddl2.data('revertValue', config.DesignOptionConfigurationID);

                                    //Add just the name for the tools summary
                                    $(toolsOptions).append('<p class=\"form-control-static\">' + config.Name + '</p>');
                                }

                                selectOption.val(config.DesignOptionConfigurationID).html(optionhtml.html()).data('quotedprice', config.CalculatedTotalPrice);
                                selectOption2.val(config.DesignOptionConfigurationID).html(optionhtml.html()).data('quotedprice', config.CalculatedTotalPrice);

                                ddl.append(selectOption);
                                ddl2.append(selectOption2);
                            });

                            $(li).append(ddl);

                            if (option.DesignOptionID == 2) {
                                $('#designSuite_optionsList').find('#option-34-ddl').after(ddl);
                                $('#option-34-ddl2').after(ddl2);
                            }
                            else {
                                $('#designSuite_optionsList').append(li);
                                $(rowOptions).append(ddl2);
                                $(rowOptions).append('</div></div>');
                                $('#rowOptions').append(rowOptions);

                                $(toolsOptions).append('</div>');
                                $('#tools-summary-options').append(toolsOptions);
                            }

                            if (option.DesignOptionID == 2) {
                                ddl.createSubDropDown(null, 34, '', showDDL);
                                ddl2.createSubDropDown(null, 34, '2', showDDL2);
                            }
                            else {
                                ddl.createDivBasedDropDown();
                                ddl2.createDivBasedDropDown();
                            }
                        });

                        // Need to update session storage in case there was a previouslySelectedConfigsWhoseSizeRequirementsAreNotMet
                        app.productQuickStartFormManager.selectedProduct.selectedOptions.saveSelectedOptions();

                        // Update the price once we have the options updated, needed here in case size requirement changed any options
                        var formData = [];
                        $('option:selected', '.optionConfigurationDDL').each(function (i, o) {
                            formData[i] = {};
                            formData[i].DesignOptionConfigurationID = $(o).val();
                            formData[i].Price = $(o).data('price');
                            formData[i].PricePerSquareInch = $(o).data('pricepersquareinch');
                            formData[i].PricingMultiplierType = $(o).data('pricingmultipliertype');
                            formData[i].CurrentProductBindingID = $(o).data('currentProductBindingID');
                            formData[i].PreDiscountBasePrice = $(o).data('preDiscountBasePrice');
                        });

                        if (data.hasSetQuantity == true) {
                            app.productQuickStartFormManager.getDiscountedPrice(productID, quantity, height, width, formData, function (data) {
                                app.productQuickStartFormManager.updateAllFormsForProduct(productID, data.unitPrice, quantity, height, width, true, data.slashedPrice, data.totalPercent, data.showSlashed);
                            });
                        }
                        else {
                            app.productQuickStartFormManager.getDiscountedUnitPrice(productID, quantity, height, width, formData, function (data) {
                                app.productQuickStartFormManager.updateAllFormsForProduct(productID, data.unitPrice, quantity, height, width, false, data.slashedPrice, data.totalPercent, data.showSlashed);
                            });
                        }

                        $('.select.optionConfigurationDDL').off();
                        $('select.optionConfigurationDDL').on('change', function (event) {
                            //app.design.details.options.dependencyMap.checkDependencies();
                            //set other dropdown to same value
                            var id = $(this).attr('id');
                            var val = $(this).val();
                            if (id.lastIndexOf('2') >= 12) {
                                id = id.substring(0, id.length - 1);

                                //$(id).val($(this).val()).change();
                                //$(id).val(val);
                                // $(id).createDropDown('updateDisplay');
                                $('#' + id).val(val);
                                $('#' + id).createDivBasedDropDown('updateDisplay');
                            }
                            else {
                                id = id + 2;
                                //$(id).val(val).change();
                                //$(id).val(val);
                                $('#' + id).val(val);
                                $('#' + id).createDivBasedDropDown('updateDisplay');
                            }

                            app.productQuickStartFormManager.selectedProduct.dependencyMap.checkSingleConfigurationDependencies(this);
                            buildQuantityDdl(false);
                            //$('.tools-Sign-Options .dropdown div.dd ul').off();
                            //$('.tools-Sign-Options .dropdown div.dd ul').on('click', function (event) {
                            var formData = [];
                            $('option:selected', 'select.optionConfigurationDDL').each(function (i, o) {
                                formData[i] = {};
                                formData[i].DesignOptionConfigurationID = $(o).val();
                                formData[i].Price = $(o).data('price');
                                formData[i].PricePerSquareInch = $(o).data('pricepersquareinch');
                                formData[i].PricingMultiplierType = $(o).data('pricingmultipliertype');
                                formData[i].CurrentProductBindingID = $(o).data('currentProductBindingID');
                                formData[i].PreDiscountBasePrice = $(o).data('preDiscountBasePrice');
                            });
                            var configIds = [];
                            $('option:selected', '.optionConfigurationDDL').each(function (i, o) {
                                configIds.push($(o).val());
                            });
                            var form = $('div.product-quick-start-form-container'),
                                widthInput = $('input[name="Width"]', form).first(),
                                heightInput = $('input[name="Height"]', form).first();
                            width = widthInput.val();
                            height = heightInput.val();
                            quantity = $('input[name="Quantity"]', form).first().val();

                            app.productQuickStartFormManager.getOptionInfo(productID, height, width, true, quantity, configIds, hasSetSize);
                        });
                        $(".tools-Sign-Options .dropdown").on('click', function (event) {
                            $('.edge').removeClass('edge');
                            $('.flip').removeClass('flip');
                            if ($('ul', this).length) {
                                var elm = $('ul:first', this);
                                var off = elm.offset();
                                var l = off.left;
                                var t = off.top;
                                var w = elm.width();
                                var h = elm.height();
                                var winH = $(window).height();
                                var winW = $(window).width();
                                var isWidthVisible = (l + w <= winW);
                                var isHeightVisible = (t + h <= $(window).scrollTop() + winH);

                                if (!isWidthVisible) {
                                    $(elm).addClass('edge');
                                } else {
                                    $(elm).removeClass('edge');
                                }

                                if (!isHeightVisible) {
                                    $(elm).addClass('flip');
                                } else {
                                    $(elm).removeClass('flip');
                                }
                            }
                        });
                        var $window = $(window);

                        var $stickyEl = $('.product-quick-start-form-container').parent();
                        if ($stickyEl.offset()) {
                            var elTop = $stickyEl.offset().top,
                                elBottom = $stickyEl.offset().top + $stickyEl.outerHeight(true);
                            var scrollVar = elTop;
                            if (!$('.product-quick-start-form-container').parent().hasClass("midWrapper")) {
                                scrollVar = elBottom;
                            }
                            if ($(window).width() > 800) {
                                $('.placeholder-sticky').height($('.product-quick-start-form-container').parent().outerHeight());
                            }
                            $window.on('scroll', function () {
                                if ($window.scrollTop() > scrollVar) {
                                    $stickyEl.addClass('sticky');
                                    $stickyEl.find('#designSuite_optionsList').addClass('stuck');
                                } else {
                                    $stickyEl.removeClass('sticky');
                                    $stickyEl.find('#designSuite_optionsList').removeClass('stuck');
                                    if ($(window).width() < 800) {
                                        $('.placeholder-sticky').height($('.product-quick-start-form-container').parent().outerHeight());
                                    }
                                }
                            });
                        }
                        window.options = getOptionsValue();

                        var queries = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                        var formmattedQueries = queries.map(function (query) {
                            return {
                                name: query.split('=')[0],
                                value: query.split('=')[1]
                            };
                        });

                        $('.tools-Sign-Options').each(function () {

                            // format the option name same as the queries in the URL
                            var optionName = $(this).find('.optionName').text();
                            optionName = optionName.replace(/\s/g, "_").toLowerCase();
                            var select = $(this).find('.optionConfigurationDDL');

                            // pass the "this" to a variable so that we can use it inside another loop


                            // loop throughout the formattedQueries (URL)
                            formmattedQueries.forEach(function (query) {

                                // if the option name from the URL is the same as the option in the dropdown
                                if (optionName === query.name) {
                                    // update the value of the select from the query in the URL
                                    select.val(query.value);

                                    var selectedValue = select.val();

                                    // get the Title of the selected option
                                    var selectedTitle = '';
                                    select.find('option').each(function () {
                                        if (Number($(this).attr('value')) === Number(selectedValue)) {
                                            selectedTitle = $(this).find('.optionInfo').text();
                                        }
                                    });

                                    // trigger change by clicking the selected option
                                    select.siblings('.dropdown').find('.dd > ul  li').each(function () {
                                        var listTitle = $(this).find('.optionInfo').text();
                                        var $this = this;
                                        if (selectedTitle === listTitle) {
                                            setTimeout(function () {
                                                $($this).trigger('click');
                                            }, 1000);

                                        }
                                    });
                                }
                            });
                        });
                    }
                });
            },
            getDiscountedUnitPrice: function (productID, quantity, height, width, selectedDesignOptionConfigurations, callback) {
                $.post('/Product/GetDiscountedUnitPrice/' + productID + '/', { quantity: quantity, height: height, width: width, selectedDesignOptionConfigurations: selectedDesignOptionConfigurations }, function (data) {
                    if (callback) {
                        callback(data);
                    }
                });
            },

            getDiscountedPrice: function (productID, quantity, height, width, selectedDesignOptionConfigurations, callback) {
                $('.size-input-qty').val($('#app-homeStandardQuantity').val());
                quantity = $('#app-homeStandardQuantity').val();
                $.post('/Product/GetDiscountedPrice/' + productID + '/', { quantity: quantity, height: height, width: width, selectedDesignOptionConfigurations: selectedDesignOptionConfigurations }, function (data) {
                    if (callback) {
                        callback(data);
                    }
                });
            },

            init: function (h, m, s) {
                //var ddl = $('#designSuite_optionsList');
                //ddl.val($('.product-quick-start-form-container').attr('data-productid'));
                app.productQuickStartFormManager.modals.init();
                app.productQuickStartFormManager.orderDeadlineTimer.init(h, m, s);
                app.productQuickStartFormManager.events.init();
                app.productQuickStartFormManager.isInitialized = true;
                //app.productQuickStartFormManager.selectedProduct.update(ddl);
            },

            events: {
                init: function () {
                    $(document)
                        .on('click', '.product-quick-start-search-form-wrapper .back-link', function (event) {
                            event.preventDefault();

                            var outbound = $(event.currentTarget).closest('.template-search-wrapper');
                            var inbound = outbound.siblings('.next-steps-wrapper').first();

                            app.slideReplace(outbound, inbound, {
                                direction: 'right',
                                callback: function (processData) {
                                    app.productQuickStartFormManager.search.pager.hide();
                                    processData.parentElem.animate({ height: inbound.height() }, function () {
                                        $(app.productQuickStartFormManager.modals.step1.modal).dialog('option', 'position', 'center');
                                    });
                                }
                            });
                        })
                        .on('click', 'div.get-started-button', function (event) {
                            var form = $(event.currentTarget).closest('div.product-quick-start-form-container'),
                                widthInput = $('input[name="Width"]', form).first(),
                                heightInput = $('input[name="Height"]', form).first(),
                                materialTypeID = $(form).data('materialtypeid'),
                                productName = $(form).data('producttitle'),
                                dimensionsValid = app.productQuickStartFormManager.materialTypeDimensionsManager.validateDimensions(widthInput, heightInput, materialTypeID, productName);
                            if (dimensionsValid) {
                                $('.product-quick-start-form-step-1-modal .search-input').trigger('blur');
                                app.productQuickStartFormManager.modals.step1.show(event.currentTarget);
                            }
                        })
                        .on('click', 'div.product-quick-start-form-step-1-wrapper div.next-step.blank-template', function (event) {
                            var prod = app.productQuickStartFormManager.selectedProduct;

                            dataLayer.push({
                                'event': 'productClick',
                                'ecommerce': {
                                    'click': {
                                        'products': [{
                                            'name': 'Blank',                      // Name or ID is required.
                                            'id': '00000000-0000-0000-0000-000000000000',
                                            'brand': 'Signs.com',
                                            'category': prod.productName
                                        }]
                                    }
                                }
                            })
                            window.location = '/design/?productID=' + prod.productID + '&width=' + prod.width + '&height=' + prod.height + '&quantity=' + prod.quantity;
                        })
                        .on('click', 'div.product-quick-start-form-step-1-wrapper div.next-step.upload', function (event) {
                            var prod = app.productQuickStartFormManager.selectedProduct;
                            dataLayer.push({
                                'event': 'productClick',
                                'ecommerce': {
                                    'click': {
                                        'products': [{
                                            'name': 'Upload',                      // Name or ID is required.
                                            'id': '00000000-0000-0000-0000-000000000000',
                                            'brand': 'Signs.com',
                                            'category': prod.productName
                                        }]
                                    }
                                }
                            })

                            window.location = '/design/?productID=' + prod.productID + '&width=' + prod.width + '&height=' + prod.height + '&quantity=' + prod.quantity + '&initUpload=true';
                        })
                        .on('click', '.designUpload', function (event) {
                            var templateCat = $('#gaProductName').html().trim();
                            var form = $('div[data-producttitle="' + templateCat + '"]'),
                                width = $('input[name="Width"]', form).first().val(),
                                height = $('input[name="Height"]', form).first().val(),
                                quantity = $('input[name="Quantity"]', form).first().val(),
                                unitPrice = $('span.unitPrice', form).first().text(),
                                slashPrice = $('span.slashedPrice', form).first().text(),
                                percentOff = $('span.percentOff', form).first().text(),
                                productId = $(form).data('productid');
                            dataLayer.push({
                                'event': 'productClick',
                                'ecommerce': {
                                    'click': {
                                        'products': [{
                                            'name': 'designUpload',                      // Name or ID is required.
                                            'id': '00000000-0000-0000-0000-000000000000',
                                            'brand': 'Signs.com',
                                            'category': $('#gaProductName').html()
                                        }]
                                    }
                                }
                            })

                            window.location = '/design/?productID=' + productId + '&width=' + width + '&height=' + height + '&quantity=' + quantity + '&initUpload=true';
                        })
                        .on('click', 'div.product-quick-start-form-step-1-wrapper div.close-button-circled-x', function (event) {
                            app.productQuickStartFormManager.modals.step1.close();
                        })
                        .on('change', '#app-homeStandardSizes', function (event) {
                            var dimensions = $(this).val().split('x');
                            var w = Number(dimensions[0]);
                            var h = Number(dimensions[1]);
                            var form = $(event.currentTarget).closest('div.product-quick-start-form-container');
                            var hasSetSizes = $(form).data('hassetsize');
                            var productID = $(form).data('productid');
                            var test = $(this).val();
                            var quantity = $('input[name="Quantity"]', form).first().val();
                            var optionPrices = [0.0];
                            var configIds = [];
                            $('option:selected', '.optionConfigurationDDL').each(function (i, o) {
                                configIds.push($(o).val());
                            });
                            $('div.product-quick-start-form-container input[name="Width"]').val(w).data('inchesvalue', w).data('revertvalue', w);
                            $('div.product-quick-start-form-container input[name="Height"]').val(h).data('inchesvalue', h).data('revertvalue', h);
                            // $('#app-homeStandardSizes').change();
                            app.productQuickStartFormManager.getOptionInfo(productID, h, w, true, quantity, configIds, true)
                            //app.design.details.dimensions.checkAndUpdate();
                        })
                        .on('change', '#app-homeStandardQuantity', function () {
                            var quantity = $(this).val();

                            $('div.product-quick-start-form-container input[name="Quantity"]').val(quantity);
                            $('div.product-quick-start-form-container input[name="Quantity"]').keyup();
                            // $('#app-homeStandardSizes').change();

                            //app.design.details.dimensions.checkAndUpdate();
                        })
                        .on('keyup', 'div.product-quick-start-form-container input[name="Width"],div.product-quick-start-form-container input[name="Height"],div.product-quick-start-form-container input[name="Quantity"]', function (event) {
                            clearTimeout(app.productQuickStartFormManager.typingTimer.obj);
                            $(".unitPrice").css("display", "none");
                            $(".unitPriceMiniSpinner").show();
                            app.productQuickStartFormManager.typingTimer.obj = setTimeout(function () {
                                var isQuantityInput = $(event.currentTarget).is('input[name="Quantity"]');

                                //prevent invalid qty
                                if (isQuantityInput) {
                                    var val = parseInt($(event.currentTarget).val());
                                    val = isNaN(val) ? 1 : Math.abs(val);
                                    $(event.currentTarget).val(val);
                                }

                                var form = $(event.currentTarget).closest('div.product-quick-start-form-container');
                                var hasSetSizes = $(form).data('hassetsize');
                                var productID = $(form).data('productid');
                                if (hasSetSizes) {
                                    var dimensions = $('#app-homeStandardSizes').val().split('x');
                                    var w = Number(dimensions[0]);
                                    var h = Number(dimensions[1]);

                                    $('div.product-quick-start-form-container input[name="Width"]').val(w).data('inchesvalue', w).data('revertvalue', w);
                                    $('div.product-quick-start-form-container input[name="Height"]').val(h).data('inchesvalue', h).data('revertvalue', h);
                                }

                                var widthInput = $('input[name="Width"]', form).first(),
                                    width = widthInput.val(),
                                    heightInput = $('input[name="Height"]', form).first(),
                                    height = heightInput.val();


                                var ddl = $('select[name="Product"]'),
                                    selectedOption = $('option:selected', ddl),
                                    materialTypeID = selectedOption !== undefined ? selectedOption.data('materialtypeid') : $(form).data('materialtypeid'),
                                    quantity = $('input[name="Quantity"]', form).first().val(),
                                    productName = $(form).data('producttitle'),
                                    dimensionsValid = app.productQuickStartFormManager.materialTypeDimensionsManager.validateDimensions(widthInput, heightInput, materialTypeID, productName);

                                var configIds = [];
                                $('option:selected', '.optionConfigurationDDL').each(function (i, o) {
                                    configIds.push($(o).val());
                                });

                                if (dimensionsValid) {
                                    app.productQuickStartFormManager.getOptionInfo(productID, height, width, true, quantity, configIds, false);
                                }
                                else {
                                    $(".unitPriceMiniSpinner").hide();
                                    $(".unitPrice").css("color", "#3fa9f5");
                                    app.productQuickStartFormManager.materialTypeDimensionsManager.revertSizes(widthInput, heightInput, true);
                                    app.productQuickStartFormManager.getOptionInfo(productID, $(heightInput).data('revertvalue'), $(widthInput).data('revertvalue'), true, quantity, configIds, false);
                                    width = $(widthInput).data('revertvalue');
                                    height = $(heightInput).data('revertvalue');
                                }

                                // Set the revertvalue to current selection so if we revert, it's the previous size instead of the default for material -Kevin 12/11/18
                                $(widthInput).data('revertvalue', width);
                                $(heightInput).data('revertvalue', height);

                                app.productQuickStartFormManager.typingTimer.obj = null; //null this out so we can check if the timer is running elsewhere.
                            }, app.productQuickStartFormManager.typingTimer.timeout);
                        })
                        .on('submit', '.product-quick-start-search-form', function (event) {
                            event.preventDefault();
                            app.productQuickStartFormManager.search.submit(event.currentTarget, 1);
                        })
                        .on('change', 'div.product-quick-start-form-container .product-select', function (event) {
                            var ddl = $(event.currentTarget);
                            app.productQuickStartFormManager.selectedProduct.update(ddl);
                        });
                },
            },

            materialTypeDimensionsManager: new MaterialTypeDimensionsManager(),

            orderDeadlineTimer: {
                hours: 24,
                minutes: 0,
                seconds: 0,
                timeOut: null,
                clear: function () {
                    clearInterval(app.productQuickStartFormManager.orderDeadlineTimer.timeOut);
                    app.productQuickStartFormManager.orderDeadlineTimer.timeOut = null;
                },

                init: function (h, m, s) {
                    if (typeof h === 'number' || typeof m === 'number' || typeof s === 'number') {
                        if (typeof h === 'number') {
                            app.productQuickStartFormManager.orderDeadlineTimer.hours = h;
                        }

                        if (typeof m === 'number') {
                            app.productQuickStartFormManager.orderDeadlineTimer.minutes = m;
                        }

                        if (typeof s === 'number') {
                            app.productQuickStartFormManager.orderDeadlineTimer.seconds = s;
                        }

                        app.productQuickStartFormManager.orderDeadlineTimer.toolTips.init();
                        app.productQuickStartFormManager.orderDeadlineTimer.set();
                    }
                },

                toolTips: {
                    init: function () {
                        $('.earliestAvailableDateInfo').tooltip({
                            items: '.earliestAvailableDateInfo',
                            tooltipClass: "verticalArrow",
                            content: function () {
                                return $(this).parent().next('span.earliestAvailableDateInfoToast').html();
                            },
                            position: {
                                my: "right bottom",
                                at: "center+25 top-20",
                                using: function (position, feedback) {
                                    $(this).css(position);
                                    $("<div>")
                                        .addClass("arrow")
                                        .addClass(feedback.vertical)
                                        .addClass(feedback.horizontal)
                                        .appendTo(this);
                                }
                            },
                            open: function (event, ui) {
                                if (typeof (event.originalEvent) === 'undefined') {
                                    return false;
                                }

                                var $id = $(ui.tooltip).attr('id');

                                // close any lingering tooltips
                                $('div.ui-tooltip').not('#' + $id).remove();
                            },
                            close: function (event, ui) {
                                //this prevents the tooltip from closing if it is hovered so links can be added.
                                //ui.tooltip.on('hover',
                                //    function() {
                                //            $(this).stop(true).fadeTo(400, 1);
                                //        },
                                //    function() {
                                //        $(this).fadeOut('400', function() {
                                //                $(this).remove();
                                //            });
                                //        });
                            }
                        });
                        $('.checkDesignPriceInfo').tooltip({
                            items: '.checkDesignPriceInfo',
                            tooltipClass: "verticalArrow",
                            content: function () {
                                return $(this).parent().next('span.checkPriceInfoToast').html();
                            },
                            position: {
                                my: "right bottom",
                                at: "center+25 top-20",
                                using: function (position, feedback) {
                                    //console.log(feedback);
                                    $(this).css(position);
                                    $("<div>")
                                        .addClass("arrow")
                                        .addClass(feedback.vertical)
                                        .addClass(feedback.horizontal)
                                        .appendTo(this);
                                }
                            },
                            open: function (event, ui) {
                                if (typeof (event.originalEvent) === 'undefined') {
                                    return false;
                                }

                                var $id = $(ui.tooltip).attr('id');

                                // close any lingering tooltips
                                $('div.ui-tooltip').not('#' + $id).remove();
                            },
                            close: function (event, ui) {
                                //this prevents the tooltip from closing if it is hovered so links can be added.
                                //ui.tooltip.on('hover',
                                //    function() {
                                //            $(this).stop(true).fadeTo(400, 1);
                                //        },
                                //    function() {
                                //        $(this).fadeOut('400', function() {
                                //                $(this).remove();
                                //            });
                                //        });
                            }
                        });
                    }
                },

                updateSLA: function (isRush) {
                    var earliestAvailableDate;
                    if (typeof isRush !== 'boolean') {
                        isRush = false;
                    }
                    app.productQuickStartFormManager.orderDeadlineTimer.clear();

                    $.ajax({
                        type: 'POST',
                        url: '/Master/GetSLA/',
                        data: { isRush: isRush },
                        success: function (data) {
                            app.productQuickStartFormManager.orderDeadlineTimer.hours = Math.floor(data.TimeTillNextProductionDeadline.TotalHours);
                            app.productQuickStartFormManager.orderDeadlineTimer.minutes = data.TimeTillNextProductionDeadline.Minutes;
                            app.productQuickStartFormManager.orderDeadlineTimer.seconds = data.TimeTillNextProductionDeadline.Seconds;
                            earliestAvailableDate = data.OvernightShippingEstimatedDueDate.Format_ddddMMMMd;
                            app.productQuickStartFormManager.orderDeadlineTimer.set();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(errorThrown);
                            app.productQuickStartFormManager.orderDeadlineTimer.hours = app.productQuickStartFormManager.orderDeadlineTimer.minutes = app.productQuickStartFormManager.orderDeadlineTimer.seconds = 0;
                            earliestAvailableDate = 'Refresh to get date';
                        },
                        complete: function (jqXHR, textStatus) {
                            $('.orderDeadline .hours').html(app.productQuickStartFormManager.orderDeadlineTimer.hours);
                            $('.orderDeadline .minutes').html(app.productQuickStartFormManager.orderDeadlineTimer.minutes);
                            $('.earliestAvailableDate').html(earliestAvailableDate);
                            app.productQuickStartFormManager.orderDeadlineTimer.setUnitVisibility();
                        }
                    });
                },

                reset: function () {
                    clearInterval(app.productQuickStartFormManager.orderDeadlineTimer.timeOut);
                    app.productQuickStartFormManager.orderDeadlineTimer.set();
                },

                set: function () {
                    app.productQuickStartFormManager.orderDeadlineTimer.timeOut = setInterval(app.productQuickStartFormManager.orderDeadlineTimer.tick, 1000);
                },

                setUnitVisibility: function () {
                    if (app.productQuickStartFormManager.orderDeadlineTimer.hours > 0) {
                        $('.orderDeadline .hoursWrapper').show();
                        $('.orderDeadline .minutesWrapper').show();
                        $('.orderDeadline .secondsWrapper').hide();
                    } else if (app.productQuickStartFormManager.orderDeadlineTimer.minutes > 0) {
                        $('.orderDeadline .hoursWrapper').hide();
                        $('.orderDeadline .minutesWrapper').show();
                        $('.orderDeadline .secondsWrapper').show();
                    } else {
                        $('.orderDeadline .hoursWrapper').hide();
                        $('.orderDeadline .minutesWrapper').hide();
                        $('.orderDeadline .secondsWrapper').show();
                    }
                },

                tick: function () {
                    app.productQuickStartFormManager.orderDeadlineTimer.seconds--;

                    if (app.productQuickStartFormManager.orderDeadlineTimer.seconds <= 0
                        && app.productQuickStartFormManager.orderDeadlineTimer.minutes <= 0
                        && app.productQuickStartFormManager.orderDeadlineTimer.hours <= 0) {
                        //old SLA has expired get new one
                        app.productQuickStartFormManager.orderDeadlineTimer.updateSLA(true);
                    } else {
                        if (app.productQuickStartFormManager.orderDeadlineTimer.seconds < 0) {
                            app.productQuickStartFormManager.orderDeadlineTimer.minutes--;
                            app.productQuickStartFormManager.orderDeadlineTimer.seconds = 59;

                            if (app.productQuickStartFormManager.orderDeadlineTimer.minutes < 0) {
                                app.productQuickStartFormManager.orderDeadlineTimer.hours--;
                                app.productQuickStartFormManager.orderDeadlineTimer.minutes = 59;

                                $('.orderDeadline .hours').html(app.productQuickStartFormManager.orderDeadlineTimer.hours);

                                if (app.productQuickStartFormManager.orderDeadlineTimer.hours <= 0) {
                                    $('.orderDeadline .hoursWrapper').hide();
                                    $('.orderDeadline .secondsWrapper').show();
                                }
                            }

                            $('.orderDeadline .minutes').html(app.productQuickStartFormManager.orderDeadlineTimer.minutes);

                            if (app.productQuickStartFormManager.orderDeadlineTimer.hours <= 0 && app.productQuickStartFormManager.orderDeadlineTimer.minutes <= 0) {
                                $('.orderDeadline .minutesWrapper').hide();
                            }
                        }

                        $('.orderDeadline .seconds').html(app.productQuickStartFormManager.orderDeadlineTimer.seconds);
                    }
                }
            },

            updateAllFormsForProduct: function (productID, unitPrice, quantity, height, width, isSetQuantity, slashedPrice, totalPercent, showSlashed) {
                $('div.product-quick-start-form-container.product-' + productID).each(function (index) {
                    var form = $(this);

                    //$('input[name="Width"]', form).val(width);
                    //$('input[name="Height"]', form).val(height);
                    //$('input[name="Quantity"]', form).val(quantity);
                    if (isSetQuantity) {
                        $('.priceLabel').html('Price:')
                    }
                    else if (productID == 41) {
                        $('.priceLabel').html('Starting at:')
                    }
                    else {
                        $('.priceLabel').html('Price each:')
                    }



                    if (showSlashed == true && (slashedPrice != '' && slashedPrice != null && totalPercent != null && parseInt(totalPercent) > 0)) {
                        $('span.slashedPrice', form).html(slashedPrice);
                        $('span.percentOff', form).html("(" + totalPercent + " off)");
                        $('span.slashedPrice', form).show();
                        $('span.percentOff', form).show();
                        $('span.unitPrice').addClass('unitPriceRed');
                    }
                    else {
                        $('span.slashedPrice', form).hide();
                        $('span.percentOff', form).hide();
                    }

                    if (productID != 39 && productID != 61 && productID != 74 && productID != 88) {
                        $(".unitPriceMiniSpinner").hide();
                        $(".unitPrice").css("display", "inline");
                        if (showSlashed == true && (slashedPrice != '' && slashedPrice != null && totalPercent != null && parseInt(totalPercent) > 0)) {
                            $(".unitPrice").css("color", "red");
                            $('span.unitPrice.quickStart.unitPriceRed').show(); //explicitly show since it is being hidden above
                        }
                        else {
                            $(".unitPrice").css("color", "#3fa9f5");
                        }
                        $('span.unitPrice', form).html(unitPrice).effect('highlight', 3000);
                        //need to update the modal prices at the same time so they don't get out of sync
                        $('.unitPrice', app.productQuickStartFormManager.modals.step1.modal).text(unitPrice);
                        $('.slashedPrice', app.productQuickStartFormManager.modals.step1.modal).text(slashedPrice);

                    }


                });
            }
        },
        //#endregion productQuickStartFormManager

        prompts: {
            alert: function (content, options) {
                var width = options && typeof options.width === 'number' ? options.width : 300;
                var height = options && typeof options.height === 'number' ? options.height : 'auto';
                var title = options && typeof options.title === 'string' ? options.title : 'Alert';

                app.prompts.elem.html(content).dialog({
                    title: title,
                    width: width,
                    buttons: [
                        {
                            text: 'Ok',
                            click: function () {
                                $(this).dialog('close');
                            }
                        }
                    ]
                });

                app.prompts.elem.dialog('open');
            },

            addedToCart: function (content, options) {
                var width = 200
                var height = options && typeof options.height === 'number' ? options.height : 'auto';
                var title = options && typeof options.title === 'string' ? options.title : 'Added to Cart';
                app.prompts.elem.html(content).dialog({
                    title: title,
                    width: width,
                    buttons: [
                        {
                            text: 'Ok',
                            click: function () {
                                $(this).dialog('close');
                            }
                        }
                    ]
                });

                app.prompts.elem.dialog('open');
            },

            confirmChanges: function (content, confirmCallback) {
                app.prompts.elem.html(content).dialog({
                    title: 'Confirm Changes',
                    open: function (event, ui) {
                        $(".ui-dialog-titlebar-close", ui.dialog).hide();
                    },
                    buttons: [
                        {
                            text: 'Confirm',
                            click: function () {
                                $(this).dialog('close');

                                if (confirmCallback) {
                                    confirmCallback();
                                }
                            }
                        },
                        {
                            text: 'Cancel',
                            click: function () {
                                $(this).dialog('close');
                            }
                        }
                    ]
                });

                app.prompts.elem.dialog('open');
            },

            confirm: function (content, yesCallback, noCallback) {
                app.prompts.elem.html(content).dialog({
                    title: 'Confirm',
                    open: function (event, ui) {
                        $(".ui-dialog-titlebar-close", ui.dialog).hide();

                    },
                    buttons: [
                        {
                            text: 'Yes',
                            click: function () {
                                $(this).dialog('close');

                                if (yesCallback) {
                                    yesCallback();
                                }
                            }
                        },
                        {
                            text: 'No',
                            click: function () {
                                $(this).dialog('close');

                                if (noCallback) {
                                    noCallback();
                                }
                            }
                        }
                    ]
                });

                app.prompts.elem.dialog('open');
            },
            confirmGuestCheckout: function (msg, loginCallback, guestCheckoutCallback) {
                app.prompts.elem.html(msg).dialog({
                    title: 'Log In or Checkout As Guest?',
                    open: function (event, ui) {
                        $(".ui-dialog-titlebar-close", ui.dialog).hide();
                    },
                    buttons: [
                        {
                            text: 'Log In',
                            click: function () {
                                $(this).dialog('close');

                                if (loginCallback) {
                                    loginCallback();
                                }
                            }
                        },
                        {
                            text: 'Guest Checkout',
                            click: function () {
                                $(this).dialog('close');

                                if (guestCheckoutCallback) {
                                    guestCheckoutCallback();
                                    //$("#checkout-confirm-email").attr('class', 'checkoutInputDiv');

                                }
                            }
                        }
                    ]
                });

                app.prompts.elem.dialog('open');
            },
            confirmWallDecal: function (content, yesCallback, noCallback) {
                app.prompts.elem.html(content).dialog({
                    title: 'Confirm',
                    buttons: [
                        {
                            text: 'Cancel',
                            click: function () {
                                $(this).dialog('close');

                                if (noCallback) {
                                    noCallback();
                                }
                            }
                        },
                        {
                            text: 'Confirm',
                            click: function () {
                                $(this).dialog('close');

                                if (yesCallback) {
                                    yesCallback();
                                }
                            }
                        }
                    ],
                    minWidth: 320
                });

                app.prompts.elem.dialog('open');
            },
            confirmCheckoutError: function (content, yesCallback, noCallback) {
                app.prompts.elem.html(content).dialog({
                    title: 'Error',
                    buttons: [

                        {
                            text: 'OK',
                            click: function () {
                                $(this).dialog('close');

                                if (yesCallback) {
                                    yesCallback();
                                }
                            }
                        }
                    ]
                });

                app.prompts.elem.dialog('open');
            },
            confirmPaidArt: function (content, yesCallback, noCallback) {
                app.prompts.elem.html(content).dialog({
                    title: 'Note',
                    open: function () {

                        $(this).parent().find('button:nth-child(2)').focus();
                        // $('#okClipBtn').focus();

                    },
                    buttons: [
                        {
                            text: 'Cancel',
                            click: function () {
                                $(this).dialog('close');

                                if (noCallback) {
                                    noCallback();
                                }
                            }
                        },
                        {
                            id: 'okClipBtn',
                            text: 'OK',
                            click: function () {
                                $(this).dialog('close');

                                if (yesCallback) {
                                    yesCallback();
                                }
                            }
                        }
                    ]
                });

                app.prompts.elem.dialog('open');
            },

            confirmPhysicalProof: function (content, yesCallback, noCallback) {
                var width = "50%";
                if ($('#deviceType').html().trim() === "Smartphone") {
                    width = "100%";
                }

                app.prompts.elem.html(content).dialog({
                    title: 'Want a physical proof?',
                    open: function (event, ui) {
                        $(".ui-dialog-titlebar-close", ui.dialog).hide();
                    },
                    buttons: [
                        {
                            text: 'Yes',
                            click: function () {
                                $(this).dialog('close');

                                if (yesCallback) {
                                    yesCallback();
                                }
                            }
                        },
                        {
                            text: 'No Thanks',
                            click: function () {
                                $(this).dialog('close');

                                if (noCallback) {
                                    noCallback();
                                }
                            }
                        }
                    ],

                    width: width
                });

                app.prompts.elem.dialog('open');
            },

            confirmCancelOrder: function (msg, yesCallback, noCallback) {
                app.prompts.elem.html(msg).dialog({
                    title: 'Are you sure you want to cancel?',
                    open: function (event, ui) {
                        $(".ui-dialog-titlebar-close", ui.dialog).hide();
                    },
                    buttons: [
                        {
                            text: 'Yes, cancel and refund my order',
                            click: function () {
                                $(this).dialog('close');

                                if (yesCallback) {
                                    yesCallback();
                                }
                            }
                        },
                        {
                            text: 'No, do not cancel my order',
                            click: function () {
                                $(this).dialog('close');

                                if (noCallback) {
                                    noCallback();

                                }
                            }
                        }
                    ],
                    width: 500
                });

                app.prompts.elem.dialog('open');
            },

            elem: null,

            init: function () {
                app.prompts.elem = $('#signs-alert-confirm-modal');

                app.prompts.elem.dialog({
                    modal: true,
                    autoOpen: false
                });
            },
        },

        addressModals: {


            init: function () {

                var addCustomerAddressModal = $('#addCustomerAddress');
                var defaultCustomerShipModal = $('#defaultCustomerShippingAddress');
                var defaultCustomerBillModal = $('#defaultCustomerBillingAddress');

                $('#addCustomerAddress').dialog({
                    autoOpen: false,
                    modal: true,
                    width: '500px',
                    buttons: {
                        Cancel: function () {
                            $(this).dialog('close');
                        }
                    }
                });

                $('#defaultCustomerShippingAddress').dialog({
                    autoOpen: false,
                    modal: true,
                    width: '800px',
                    buttons: {
                        Cancel: function () {
                            $(this).dialog('close');
                        }
                    }
                });
                $('#defaultCustomerBillingAddress').dialog({
                    autoOpen: false,
                    modal: true,
                    width: '800px',
                    buttons: {
                        Cancel: function () {
                            $(this).dialog('close');
                        }
                    }
                });
            },



        },

        searchAndDestroy: {
            timerCounter: 0,
            timer: null,
            start: function (selector, options) {
                //check options and set defaults
                if (!options) options = {};
                if (typeof options.maxRunDurationMilliseconds !== 'number') options.maxRunDurationMilliseconds = 10000;
                if (typeof options.searchIntervalMilliseconds !== 'number') options.searchIntervalMilliseconds = 100;
                if (typeof options.forceRunToMaxDuration !== 'boolean') options.forceRunToMaxDuration = false;

                var elements = $(selector);

                if (signsSite.searchAndDestroy.timerCounter < options.maxRunDurationMilliseconds && signsSite.searchAndDestroy.timer === null && (!elements.length || options.forceRunToMaxDuration)) {
                    signsSite.searchAndDestroy.timer = setTimeout(function () {
                        signsSite.searchAndDestroy.timer = null;
                        signsSite.searchAndDestroy.timerCounter += options.searchIntervalMilliseconds;
                        signsSite.searchAndDestroy.start(selector, options);
                    }, options.searchIntervalMilliseconds);
                }
                else {
                    signsSite.searchAndDestroy.timer = null;
                    signsSite.searchAndDestroy.timerCounter = 0;
                }

                if (elements.length) {
                    elements.remove();
                }
            }
        },

        //#region Spinner
        spinner: {
            timer: null,

            show: function (options) {
                var spinner = $('#in-progress-overlay');
                var spinnerTextContainer = $('#in-progress-text');
                var delay = 500;
                var timeOut = 30000;
                var text = '';
                var textStyle = "text-align:center;color:White;font-size:20px;width:100%;";

                var textOptions = {
                    container: spinner,
                    duration: 0,
                    offsetTop: -50,
                    alignX: false
                };

                if (options) {
                    if (options.delay && typeof options.delay === 'number') {
                        delay = options.delay > 0 ? options.delay : 1; //delays of less than 1 default to 500ms in jquery so we need to prevent this when setting to 0 or less.
                    }
                    if (options.timeOut && typeof options.timeOut === 'number') {
                        timeOut = options.timeOut;
                    }
                    if (options.text) {
                        text = options.text;
                    }
                    if (options.textStyle) {
                        textStyle = options.textStyle;
                    }
                    if (options.textOffsetTop) {
                        textOptions.offsetTop = options.textOffsetTop;
                    }
                    if (options.textOffsetLeft) {
                        textOptions.offsetLeft = options.textOffsetLeft;
                    }
                }

                spinnerTextContainer.attr('style', textStyle).html(text);


                spinner.delay(delay).queue(function () {
                    // If an alert is visible, we don't want to show the spinner because it makes it so the user can't click on the modal
                    if ($('#signs-alert-confirm-modal').is(':visible')) {
                        if (signsSite.designTool !== null) {
                            $(signsSite.designTool.design.canvas.elements.selection.select.hard(null));
                        }
                        return;
                    }

                    if (options && options.container) {
                        options.duration = 0;
                        spinner.outerHeight($(options.container).outerHeight());
                        spinner.outerWidth($(options.container).outerWidth());
                        spinner.align(options);
                    }

                    $(spinner).show().dequeue();
                    spinnerTextContainer.align(textOptions);

                    if (timeOut > -1) {
                        app.spinner.timer = setTimeout(function () {
                            $(spinner).hide();
                        }, timeOut);
                    }

                });
            },

            hide: function () {
                $('#in-progress-overlay').dequeue().hide();
                clearTimeout(app.spinner.timer);
            }
        },

        // Spinner only used when "Processing Your Order". It would disappear if it was displayed 50 seconds after a shipping options overlay was displayed.
        checkoutSpinner: {
            timer: null,

            show: function (options) {
                var spinner = $('#checkout-in-progress-overlay');
                var spinnerTextContainer = $('#checkout-in-progress-text');
                var delay = 500;
                var timeOut = 30000;
                var text = '';
                var textStyle = "text-align:center;color:White;font-size:20px;width:100%;";

                var textOptions = {
                    container: spinner,
                    duration: 0,
                    offsetTop: -50,
                    alignX: false
                };

                if (options) {
                    if (options.delay && typeof options.delay === 'number') {
                        delay = options.delay > 0 ? options.delay : 1; //delays of less than 1 default to 500ms in jquery so we need to prevent this when setting to 0 or less.
                    }
                    if (options.timeOut && typeof options.timeOut === 'number') {
                        timeOut = options.timeOut;
                    }
                    if (options.text) {
                        text = options.text;
                    }
                    if (options.textStyle) {
                        textStyle = options.textStyle;
                    }
                    if (options.textOffsetTop) {
                        textOptions.offsetTop = options.textOffsetTop;
                    }
                    if (options.textOffsetLeft) {
                        textOptions.offsetLeft = options.textOffsetLeft;
                    }
                }

                spinnerTextContainer.attr('style', textStyle).html(text);


                spinner.delay(delay).queue(function () {
                    // If an alert is visible, we don't want to show the spinner because it makes it so the user can't click on the modal
                    if ($('#signs-alert-confirm-modal').is(':visible')) {
                        $(signsSite.designTool.design.canvas.elements.selection.select.hard(null));
                        return;
                    }

                    if (options && options.container) {
                        options.duration = 0;
                        spinner.outerHeight($(options.container).outerHeight());
                        spinner.outerWidth($(options.container).outerWidth());
                        spinner.align(options);
                    }

                    $(spinner).show().dequeue();
                    spinnerTextContainer.align(textOptions);

                    if (timeOut > -1) {
                        app.spinner.timer = setTimeout(function () {
                            $(spinner).hide();
                        }, timeOut);
                    }

                });
            },

            hide: function () {
                $('#checkout-in-progress-overlay').dequeue().hide();
                clearTimeout(app.spinner.timer);
            }
        },
        //#endregion Spinner

        //#region Slider
        slider: {
            timer: null,
            count: 0,

            init: function () {
                if ($('#sliderContainer').length) {

                    app.slider.count = $('.sliderBannerOuter').length;
                    $(document).on('mouseover mousemove mouseout', '#sliderContainer', function (e) {
                        if (e.type === 'mousemove' || e.type === 'mouseover') {
                            app.slider.stop();
                        }

                        if (e.type === 'mouseout') {
                            app.slider.resume();
                        }
                    });

                    $('#heroActionButton').on('click', function () {
                        var customFunction = $('.sliderBannerOuter:visible').first().data('buttonfunction');
                        var evalFunction = customFunction || $('#heroActionButton').data('defaultfunction');
                        eval(evalFunction);
                    });

                    $('#sliderNavLinks').empty();

                    if (app.slider.count > 1) {
                        $('.sliderBannerOuter').each(function (i, e) {
                            var src = i === 0 ? '/Content/assets/styles/home/images/slider_selected.png' : '/Content/assets/styles/home/images/slider_unselected.png';
                            $('#sliderNavLinks').append('<img class="sliderSelectorLink" src="' + src + '" style="cursor: pointer;" alt="Slide ' + (i + 1) + ' Selector" />');
                        });

                        $(document).on('click', '.sliderSelectorLink', function () {
                            app.slider.start($(this).index());
                        });

                        $('#sliderNavMask').show();
                    }

                    app.slider.start();
                }
            },

            start: function (index) {
                app.slider.show(index);
                app.slider.resume();
            },

            show: function (index) {
                app.slider.hideAllSlides();

                if (!index || index + 1 > $('.sliderBannerOuter').length) {
                    index = 0;
                }

                $('.sliderSelectorLink').eq(index).attr('src', '/Content/assets/styles/home/images/slider_selected.png');

                if (index + 1 === $('.sliderBannerOuter').length) {
                    $("#heroActionButton").css("background-image", "url(/content/assets/styles/home/images/slider_blueBtn.png)");
                } else {
                    $("#heroActionButton").css("background-image", "url(/content/assets/styles/home/images/slider_orangeBtn.png)");
                }

                var activeSlider = $('.sliderBannerOuter').eq(index);

                activeSlider.addClass('active').fadeIn(function () {
                    var btn = $('#heroActionButton');
                    var newText = activeSlider.data('buttontext');
                    var btnText = newText || btn.data('defaulttext');

                    btn.val(btnText);
                    $("#sliderNavContainer").show();
                });
            },

            hideAllSlides: function () {
                $('.sliderBannerOuter').hide().removeClass('active');
                $('#sliderNavContainer').hide();
                $('#sliderNavLinks > img').attr('src', '/Content/assets/styles/home/images/slider_unselected.png');
            },

            resume: function () {
                clearTimeout(app.slider.timer);

                if (app.slider.count > 1) {
                    app.slider.timer = setTimeout(function () {
                        var currentSlideIndex = $('.active').index('.sliderBannerOuter');
                        app.slider.show(currentSlideIndex + 1);
                        app.slider.resume();
                    }, 5500);
                }
            },

            stop: function () {
                clearTimeout(app.slider.timer);
            }
        },
        //#endregion Slider

        templateSearchManager: {
            productUrl: null,

            init: function () {
                $(document)
                    .on('submit', '#headerSearchForm, .template-list-widget-search-form', function (event) {
                        event.preventDefault();
                        var searchStr = $.trim($('input[type = "text"]', this).val()).replace(/[^\w\s]/gi, '');
                        location.href = String.IsNullOrWhiteSpace(app.templateSearchManager.productUrl) ? '/templates/search/' + encodeURIComponent(searchStr) : '/' + app.templateSearchManager.productUrl + '-templates/search/' + encodeURIComponent(searchStr);
                    });
            }
        },

        user: {
            isAuthenticated: null,
            isRegistered: null
        },

    };

    // Initialise the application on DOM ready
    $(function () { app.init(); });

    return app;

}(jQuery));
//#endregion signsSite object

//#region Prototype Utilities
/*jslint browser: true, devel: true, continue: true, sub: false, vars: true, white: true, plusplus: true, newcap: false, eqeq: false, nomen: true, unparam: true */
(function () {
    'use strict';
    ///Indicates whether a specified string is null, or empty.
    ///Returns Boolean
    ///Usage String.IsNullOrEmpty(stringToEvaluate)
    String.IsNullOrEmpty = function (value) {
        if (typeof value === 'string' && value.length > 0) {
            return false;
        }
        return true;
    };

    ///Indicates whether a specified string is null, empty, or consists only of white-space characters.
    ///Returns Boolean
    ///Usage String.IsNullOrWhiteSpace(stringToEvaluate)
    String.IsNullOrWhiteSpace = function (value) {
        if (typeof value === 'string' && value.replace(/^\s+|\s+$/g, "").length > 0) {
            return false;
        }
        return true;
    };

    String.prototype.isValidColorHex = function () {
        return /(^#[0-9a-fA-F]{6}$)|(^#[0-9a-fA-F]{3}$)/i.test(this);
    };

    String.prototype.isValidUrl = function () {
        //this is the url regex used by the jquery validate plugin
        return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(this);
    }

    ///Trims leading and trailing whitespace from the string.
    ///Returns string
    ///Usage - string.trim()
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "");
    };

    ///Trims leading whitespace from the string.
    ///Returns string
    ///Usage - string.lTrim()
    String.prototype.lTrim = function () {
        return this.replace(/^\s+/, "");
    };

    ///Trims trailing whitespace from the string.
    ///Returns string
    ///Usage - string.rTrim()
    String.prototype.rTrim = function () {
        return this.replace(/\s+$/, "");
    };

    ///Returns the index of the specified item within the array or -1 if not found.
    ///Returns Int
    ///Usage - var index = array.indexOf(item);
    Array.prototype.indexOf = function (item) {
        var i;
        for (i = 0; i < this.length; i++) {
            if (this[i] === item) {
                return i;
            }
        }
        return -1;
    };

    ///Replaces all instances of specified substring within the string.
    ///Returns String
    ///Usage - string.replaceAll("subString", "newString")
    ///or to ignore case - string.replaceAll("subString", "newString", true)
    String.prototype.replaceAll = function (substr, newstr, caseInsensitive) {
        var i,
            regexMeta = ['\\', '[', '^', '$', '.', '|', '?', '*', '+', '(', ')'], //Regex meta characters must be escaped.
            regexMod = caseInsensitive ? 'gi' : 'g';
        for (i = 0; i < regexMeta.length; i++) {
            substr = substr.replace(regexMeta[i], "\\" + regexMeta[i]);
        }
        return this.replace(new RegExp(substr, regexMod), newstr);
    };

    ///Returns true if the string is "true" or "True" otherwise returns false.
    ///Returns Bool
    ///Usage - string.toBool()
    String.prototype.toBool = function () {
        return (/^true$/i).test(this);
    };

    String.prototype.getFileExtension = function (forceLowerCase) {
        if (typeof forceLowerCase !== 'boolean') {
            forceLowerCase = false;
        }
        var ext = this.substr((~-this.lastIndexOf(".") >>> 0) + 2);
        if (forceLowerCase) {
            ext = ext.toLowerCase();
        }
        return ext;
    };

    Number.prototype.formatMoney = function (decimalPlaces, decimalDelim, thousandsDelim) {
        decimalPlaces = isNaN(decimalPlaces = Math.abs(decimalPlaces)) ? 2 : decimalPlaces;
        decimalDelim = decimalDelim || '.';
        thousandsDelim = thousandsDelim || ',';
        var n = this,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(+n || 0).toFixed(decimalPlaces), 10)),
            j = i.length > 3 ? i.length % 3 : 0;
        return s + (j ? i.substr(0, j) + thousandsDelim : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousandsDelim) + (decimalPlaces ? decimalDelim + Math.abs(n - i).toFixed(decimalPlaces).slice(2) : "");
    };
}());
//#endregion Prototype Utilities

//#region jQuery Plugins
/*jslint browser: true, devel: true, continue: true, sub: false, vars: true, white: true, plusplus: true, newcap: false, eqeq: false, nomen: true, unparam: true */
/*global jQuery */
(function ($) {
    'use strict';

    $.fn.timer = function (options) {
        var app = this,
            h = 24,
            m = 0,
            s = 0,
            hSelect = '.hours',
            mSelect = '.minutes',
            sSelect = '.seconds',
            hWrapperSelect = '.hoursWrapper',
            mWrapperSelect = '.minutesWrapper',
            sWrapperSelect = '.secondsWrapper',
            expiredCallback = function (parent, timer) {
                clearInterval(timer);
            };

        this.setUnitVisibility = function (parent) {
            if (h > 0) {
                $(hWrapperSelect, parent).show();
                $(mWrapperSelect, parent).show();
                $(sWrapperSelect, parent).hide();
            } else if (m > 0) {
                $(hWrapperSelect, parent).hide();
                $(mWrapperSelect, parent).show();
                $(sWrapperSelect, parent).show();
            } else {
                $(hWrapperSelect, parent).hide();
                $(mWrapperSelect, parent).hide();
                $(sWrapperSelect, parent).show();
            }
        };

        this.tick = function (parent, timer) {
            s--;

            if (s <= 0
                && m <= 0
                && h <= 0) {
                //old SLA has expired

                if (expiredCallback) {
                    expiredCallback(parent, timer);
                }
            } else {
                if (s < 0) {
                    m--;
                    s = 59;

                    if (m < 0) {
                        h--;
                        m = 59;

                        $(hSelect, parent).html(h);

                        if (h <= 0) {
                            $(hWrapperSelect, parent).hide();
                            $(sWrapperSelect, parent).show();
                        }
                    }

                    $(mSelect, parent).html(m);

                    if (h <= 0 && m <= 0) {
                        $(mWrapperSelect, parent).hide();
                    }
                }
            }

            $(sSelect, parent).html(s);
        };

        if (options) {
            if (typeof options.hours === 'number') {
                h = options.hours;
            }
            if (typeof options.minutes === 'number') {
                m = options.minutes;
            }
            if (typeof options.seconds === 'number') {
                s = options.seconds;
            }
            if (typeof options.hoursSelector === 'string') {
                hSelect = options.hoursSelector;
            }
            if (typeof options.minutesSelector === 'string') {
                mSelect = options.minutesSelector;
            }
            if (typeof options.secondsSelector === 'string') {
                sSelect = options.secondsSelector;
            }
            if (typeof options.hoursWrapperSelector === 'string') {
                hWrapperSelect = options.hoursWrapperSelector;
            }
            if (typeof options.minutesWrapperSelector === 'string') {
                mWrapperSelect = options.minutesWrapperSelector;
            }
            if (typeof options.secondsWrapperSelector === 'string') {
                sWrapperSelect = options.secondsWrapperSelector;
            }
            if (typeof options.expiredCallback === 'function') {
                expiredCallback = options.expiredCallback;
            }
        }

        return this.each(function () {
            var parent = this;
            app.setUnitVisibility(parent);
            var timer = setInterval(function () { app.tick(parent, timer); }, 1000);
        });
    };

    $.fn.align = function (options) {
        var alignY = true;
        var alignX = true;
        var animateOptions = {};
        var offsetTop = 0;
        var offsetLeft = 0;

        if (options) {
            if (options.offsetTop && typeof options.offsetTop === "number") {
                offsetTop = options.offsetTop;
            }
            if (options.offsetLeft && typeof options.offsetLeft === "number") {
                offsetLeft = options.offsetLeft;
            }
            if (typeof (options.alignY) === "boolean") {
                alignY = options.alignY;
            }
            if (typeof (options.alignX) === "boolean") {
                alignX = options.alignX;
            }
            if (options.duration || options.duration === 0) {
                animateOptions.duration = options.duration;
            }
            if (options.easing) {
                animateOptions.easing = options.easing;
            }
            if (options.complete) {
                animateOptions.complete = options.complete;
            }
            if (options.step) {
                animateOptions.step = options.step;
            }
            if (typeof (options.queue) === "boolean") {
                animateOptions.queue = options.queue;
            }
            if (options.specialEasing) {
                animateOptions.specialEasing = options.specialEasing;
            }
        }

        return this.each(function () {
            var offsetParent = $(this).offsetParent();
            var container;
            if (options && options.container) {
                container = $(options.container);
            } else {
                container = $(this).parent();
            }

            var h = $(this).outerHeight();
            var w = $(this).outerWidth();
            if (h < 1 || w < 1) {
                return false;
            } //exit if height or width is 0

            var ch; //container height
            var cw; //container width
            var top;
            var left;

            if (container[0] === window || container[0] === document || offsetParent[0] === container[0]) {
                ch = $(container).height();
                cw = $(container).width();

                top = ((ch - h) / 2) + offsetTop;
                left = ((cw - w) / 2) + offsetLeft;
            } else {
                var offset = $(container).offset();
                ch = $(container).innerHeight();
                cw = $(container).innerWidth();

                top = ((ch - h) / 2) + offset.top + offsetTop;
                left = ((cw - w) / 2) + offset.left + offsetLeft;
            }

            var properties = {};
            if (alignY) {
                properties.top = top;
            }
            if (alignX) {
                properties.left = left;
            }

            $(this).css("position", "absolute");
            $(this).animate(properties, animateOptions);
        });
    };

    $.fn.createDropDown = function (method) {
        if (method) {
            if (method === 'updateDisplay') {
                return this.each(function () {
                    var source = $(this);
                    var options = $('option', source); // get all <option> elements
                    var selected = options.filter(':selected'); // get selected <option>
                    var dropdown = source.next('.dropdown');
                    var value = source.val();
                    var text = selected.text();


                    $('dt', dropdown).html(text).data('value', value);
                });
            }
        }

        $(document).on('click', function (e) {
            var $clicked = $(e.target);
            var clickedDropdown = $clicked.closest('.dropdown');

            $('.dropdown').not(clickedDropdown).find('dd ul').hide();
        });

        return this.each(function () {
            var source = $(this);
            var options = $('option', source); // get all <option> elements
            var selected = options.filter(':selected'); // get selected <option>

            var optionList = $('<ul>');
            // iterate through all the <option> elements and create UL
            options.each(function () {
                var li = $('<li>').html($(this).text()).data('value', $(this).val());
                optionList.append(li);
            });

            // create <dl> and <dt> with selected value inside it
            var ddlClass = null;
            var sourceID = source.attr('id');
            var sourceName = source.attr('name');

            if (sourceID || sourceName) {
                ddlClass = 'ddl-dynamic-';
                ddlClass += sourceID || sourceName;
            }

            var dropdown = $('<dl class="dropdown">');
            if (ddlClass) {
                dropdown.addClass(ddlClass);
            }

            dropdown.append($('<dt>').text(selected.text()).data('value', selected.val()));
            dropdown.append($('<dd>').append(optionList));

            source.next('.dropdown').remove(); //remove any currently existing ddl if there is one.

            source.after(dropdown);
            source.hide();

            $('dt', dropdown).on('click', function () {
                $('div.dd ul .productOptionInfo .configDescrip', dropdown).show();
                $('dd ul', dropdown).toggle();
                $('div.dd ul .productOptionInfo', dropdown).toggle();
                $('div.dd ul .productOptionInfo', dropdown).css("display", "flex");


            });

            $('dd ul li', dropdown).on('click', function () {
                $('div.dd ul .productOptionInfo .configDescrip', dropdown).hide();
                var text = $(this).html();
                var value = $(this).data('value');
                $('dt', dropdown).html(text).data('value', value);

                source.val(value).change();
                $('dd ul', dropdown).hide();
            });
        });
    };

    $.fn.createDivBasedDropDown = function (method) {
        if (method) {
            if (method === 'updateDisplay') {
                return this.each(function () {
                    var source = $(this);
                    var options = $('option', source); // get all <option> elements
                    var selected = options.filter(':selected'); // get selected <option>
                    var dropdown = source.next('.dropdown');
                    var value = source.val();
                    var html = selected.html();

                    $('div.dt', dropdown).html(html).data('value', value);
                });
            }
        }
        $(document).on('click', function (e) {
            var $clicked = $(e.target);
            var clickedDropdown = $clicked.closest('.dropdown');
            $('.dropdown').not(clickedDropdown).find('div.dd ul').hide();
        });

        return this.each(function () {
            var source = $(this);
            var options = $('option', source); // get all <option> elements
            var selected = options.filter(':selected'); // get selected <option>
            globalSource = source;

            var optionList = $('<ul>');

            if (source.prop('id')) {
                if (source.prop('id').includes('option-34-ddl')) {
                    optionList.append('<div class="js_optionTitle mainOptionTitle">Select Your Hole Diameter</div>');
                }
            }

            // iterate through all the <option> elements and create UL
            options.each(function (index, value) {

                var li = $('<li>').html($(this).html()).data({ value: $(this).val(), hasSetMinSize: $(this).data().hasSetMinSize, hasSetMinQuantity: $(this).data().hasSetMinQuantity, isQuickStart: $(this).data().isQuickStart });
                optionList.append(li);
            });

            // create <dl> and <dt> with selected value inside it
            var ddlClass = null;
            var sourceID = source.prop('id');
            var sourceName = source.prop('name');

            if (sourceID || sourceName) {
                ddlClass = 'ddl-dynamic-';
                ddlClass += sourceID || sourceName;
            }

            var dropdown = $('<div class="dropdown">');
            if (ddlClass) {
                dropdown.addClass(ddlClass);
            }

            dropdown.append($('<div class="dt">').html(selected.html()).data('value', selected.val()));
            dropdown.append($('<div class="dd">').append(optionList));

            source.next('.dropdown').remove(); //remove any currently existing ddl if there is one.

            source.after(dropdown);
            source.hide();

            $('div.dt', dropdown).on('click', function () {
                $('div.dd ul .productOptionInfo .configDescrip', dropdown).show();
                $('div.dd ul', dropdown).toggle();

                $('div.dd ul .productOptionInfo', dropdown).toggle();
                $('div.dd ul .productOptionInfo', dropdown).css("display", "flex");

            });

            $('div.dd ul li', dropdown).on('click', function () {
                dropdown.find('li').not('.js_subOptions').find('.productOptionInfo .configDescrip').hide();
                var text = $(this).html();
                var value = $(this).data('value');

                // If we select an option that has sub-options,
                if (dropdown.prop('class').includes('dynamic-option-34-ddl')) {
                    // If none was selected, select none for drilled holes location and don't createSelectedOptionListItem
                    if (value == 191) {
                        dropdown.prev().val(4).change();
                    }
                    else {
                        // Create a new selectedMainOption list item that we prepend onto the start of ul and show suboptions.
                        var innerText = $(this).find('span.optionInfo').html();
                        dropdown.createSelectedOptionListItem(dropdown, innerText, value);
                    }
                }
                else {

                    $('div.dt', dropdown).html(text).data('value', value);
                    $('div.dd ul', dropdown).hide();
                }
                // quantity check for tear off cards
                //if ($(this).data().hasSetMinSize || $(this).data().hasSetMinQuantity) {

                var minSetSize;
                var minSetQuantity;
                var optionHasSetSize;
                var tempValue;
                var minSetSizeWidth;
                var minSetSizeInchesHeight;
                var defaultWidth;
                var defaultHeight;

                var setQuantity;
                var setSize;


                if (!$(this).data().isQuickStart) {
                    var designID = this.baseURI.substring(this.baseURI.lastIndexOf('=') + 1);

                    var valGuid = isGuid(designID);

                    if (valGuid == false) {
                        designID = '00000000-0000-0000-0000-000000000000';
                    }
                    var productID = $('#app-product4').val();
                    if (productID == null || productID == undefined) {
                        productID = $('#ProductID').val();
                    }

                    var submitData = [
                        { name: 'designID', value: designID },
                        { name: 'productID', value: productID },
                        { name: 'designType', value: null }
                    ];

                    submitData.push({ name: 'selectedConfigurationGUIDs[0]', value: value });
                    //// Else options were selected in Get Started
                    //else if (optionSelectedInGetStarted.length > 0) {
                    //    //Add selectedConfigurationIDs from Get Started so we set total price correctly
                    //    optionSelectedInGetStarted.forEach(function (configID, index) {
                    //        submitData.push({ name: 'selectedConfigurationIDs[' + index + ']', value: configID });
                    //    });
                    //}

                    $.ajax({
                        url: '/design/getSignOptionQuantitySet',
                        type: 'POST',
                        async: false,
                        data: submitData,
                        success: function (data) {
                            if (data.hasSetQuantity == true) {
                                minSetQuantity = data.setQuantities[0].Quantity;
                                minSetSizeWidth = data.setSizes[0].InchesWidth;
                                minSetSizeInchesHeight = data.setSizes[0].InchesHeight;
                            }

                        },
                        error: function (request, status, error) {
                            signsSite.prompts.alert('error');
                        }
                    });

                    if ($(this).data().hasSetMinQuantity) {
                        if ($('#designSuitQuantityListContainer2 .quantityAmountDDL').val() < minSetQuantity) {

                            var revertVal = source.val();
                            var changeQuantity = confirm("The minimum order quantity for the option you selected is " + minSetQuantity + "; your order quantity will be updated to " + minSetQuantity + ".  Apply these changes?");
                            if (!changeQuantity) {
                                value = revertVal;
                            }
                        }
                    }

                    if ($(this).data().hasSetMinSize) {

                        var size = minSetSizeWidth + "x" + minSetSizeInchesHeight

                        if ($('#app-defStandardSizes,#app-defStandardSizes2,#app-defStandardSizes3').val() != size) {
                            var revertVal = source.val();
                            var changeQuantity = confirm("The option you selected will change the size options. Apply these changes?");
                            if (!changeQuantity) {
                                value = revertVal;
                            }
                        }
                    }
                }

                source.val(value).change();
            });
        });
    };

    $.fn.createSelectedOptionListItem = function (dropdown, innerText, value, showSubOptionDropdown, selectedSubOption) {
        // Hide all main options, show sub options
        dropdown.find('li').not('.js_subOptions').hide();
        dropdown.find('.js_optionTitle').hide();
        dropdown.find('.js_subOptions').show();

        var li = "<div class='js_selectedMainOption'><div class='selectedMainOption'><p >" + innerText + " Diameter</p><span class='mainOptionEdit'> edit </span></div></div>";

        dropdown.find('ul').prepend(li);

        // Click to display dropdown if it was visible before and no location is selected
        // Goal here is to keep it open after you selected your diameter
        if (showSubOptionDropdown && selectedSubOption == 4) {
            $('div.dt', dropdown).click();
        }

        $('.js_selectedMainOption', dropdown).on('click', function (e) {
            // Hide new li and suboptions, show main options
            dropdown.find('.js_selectedMainOption').remove();
            dropdown.find('.js_subOptions').hide();
            $('div.dd ul .productOptionInfo .configDescrip', dropdown).show();
            dropdown.find('li').not('.js_subOptions').show();
            dropdown.find('.js_optionTitle').show();
            e.stopPropagation();
        });

    };

    $.fn.createSubDropDown = function (method, designOptionID, ddlNumber, showSubOptionDropdown) {
        var ddl = 'ddl' + ddlNumber;
        if (method) {
            if (method === 'updateDisplay') {
                return this.each(function () {
                    var source = $(this);
                    var options = $('option', source); // get all <option> elements
                    var selected = options.filter(':selected'); // get selected <option>
                    var dropdown = source.next('.dropdown');
                    var value = source.val();
                    var html = selected.html();

                    $('div.dt', dropdown).html(html).data('value', value);
                });
            }
        }

        $(document).on('click', function (e) {
            var $clicked = $(e.target);
            var clickedDropdown = $clicked.closest('.dropdown');
            $('.dropdown').not(clickedDropdown).find('div.dd ul').hide();
        });

        return this.each(function () {
            var source = $(this);
            var options = $('option', source); // get all <option> elements
            var selected = options.filter(':selected'); // get selected <option>

            var optionList = null;
            var parentDivDropDown = $('.ddl-dynamic-option-' + designOptionID + '-' + ddl);
            var parentInnerText = $('#option-' + designOptionID + '-' + ddl).find(':selected').find('.optionInfo').html();
            var parentValue = $('#option-' + designOptionID + '-' + ddl).find('option').filter(':selected').val();
            var parentUL = parentDivDropDown.find('ul')

            // Get started doesn't create ddl2
            if (parentDivDropDown.length > 0) {

                // iterate through all the <option> elements and create UL
                options.each(function () {
                    //var li = $('<li>').html($(`#option-${designOptionID}-${ddl}`).html()).data('value', $(`#option-${designOptionID}-${ddl}`).val());
                    var li = $('<li class="js_subOptions">').html($(this).html()).data('value', $(this).val());
                    parentUL.append(li);
                });

                // Hide suboptions. Will display these once the main option is selected
                parentDivDropDown.find('.js_subOptions').hide();

                // If they have selected a diameter and location is none, show "Select hole location" instead of the selected location in the dropdown dt
                if (parentValue != 191 && selected.find('.optionInfo').html() == "None") {
                    parentDivDropDown.find('.dt').find('.optionInfo')[0].innerHTML = "Select Hole Location";
                }
                else {
                    parentDivDropDown.find('.dt').find('.optionInfo')[0].innerHTML = selected.find('.optionInfo').html();
                }

                source.hide();

                // If showSubOptionDropdown passed in true or no diameter was selected
                if (showSubOptionDropdown || parentValue != 191) {
                    parentDivDropDown.createSelectedOptionListItem(parentDivDropDown, parentInnerText, parentValue, showSubOptionDropdown, selected.val());
                }

                $('div.dd ul li.js_subOptions', parentDivDropDown).on('click', function () {

                    $('div.dd ul .productOptionInfo .configDescrip', parentDivDropDown).hide();
                    var text = $(this).html();
                    var value = $(this).data('value');
                    $('div.dt', parentDivDropDown).html(text).data('value', value);

                    if (value == 4) {
                        parentDivDropDown.prev().prev().val(191).change();
                    }

                    source.val(value).change();

                    $('div.dd ul', parentDivDropDown).hide();
                });
            }
        });
    };

    $.fn.maxlength = function (options) {
        var settings = jQuery.extend(
            {
                events: [], // Array of events to be triggerd
                maxCharacters: 10, // Characters limit
                status: true, // True to show status indicator below the element
                statusClass: "status", // The class on the status div
                statusText: "characters left", // The status text
                notificationClass: "notification", // Will be added to the emement when maxlength is reached
                showAlert: false, // True to show a regular alert message
                alertText: "You have typed too many characters.", // Text in the alert message
                slider: true // Use counter slider
            }, options);

        // Add the default event
        $.merge(settings.events, ['keyup']);

        return this.each(function () {
            var item = $(this);
            var charactersLength = $(this).val().length;

            // Shows an alert msg
            function showAlert() {
                if (settings.showAlert) {
                    signsSite.prompts.alert(settings.alertText);
                }
            }

            // Update the status text
            function updateStatus() {
                var charactersLeft = settings.maxCharacters - charactersLength;

                if (charactersLeft < 0) {
                    charactersLeft = 0;
                }

                item.next("div").html(charactersLeft + " " + settings.statusText);
            }

            function checkChars() {
                var valid = true;

                // Too many chars?
                if (charactersLength >= settings.maxCharacters) {
                    // Too may chars, set the valid boolean to false
                    valid = false;
                    // Add the notifycation class when we have too many chars
                    item.addClass(settings.notificationClass);
                    // Cut down the string
                    item.val(item.val().substr(0, settings.maxCharacters));
                    // Show the alert dialog box, if its set to true
                    showAlert();
                } else {
                    // Remove the notification class
                    if (item.hasClass(settings.notificationClass)) {
                        item.removeClass(settings.notificationClass);
                    }
                }

                if (settings.status) {
                    updateStatus();
                }
            }

            // Check if the element is valid.
            function validateElement() {
                var ret = false;

                if (item.is('textarea')) {
                    ret = true;
                } else if (item.filter("input[type=text]")) {
                    ret = true;
                } else if (item.filter("input[type=password]")) {
                    ret = true;
                }

                return ret;
            }

            // Validate
            if (!validateElement()) {
                return false;
            }

            // Loop through the events and bind them to the element
            $.each(settings.events, function (i, n) {
                item.on(n, function (e) {
                    charactersLength = item.val().length;
                    checkChars();
                });
            });

            // Insert the status div
            if (settings.status) {
                item.after($("<div/>").addClass(settings.statusClass).html('-'));
                updateStatus();
            }

            // Remove the status div
            if (!settings.status) {
                var removeThisDiv = item.next("div." + settings.statusClass);

                if (removeThisDiv) {
                    removeThisDiv.remove();
                }

            }

            // Slide counter
            if (settings.slider) {
                item.next().hide();

                item.on('focus', function () {
                    item.next().slideDown('fast');
                });

                item.on('blur', function () {
                    item.next().slideUp('fast');
                });
            }

        });
    };

    //this really needs fleshing out... its a quickie
    $.fn.scroller = function (options) {
        var container = {
                height: null,
                width: null
            },
            animateOptions = {},
            verticalIncrement = 20,
            horizontalIncrement = 20,
            callback = null;

        if (options) {
            if (typeof options.height === "number") {
                container.height = options.height;
            }
            if (typeof options.width === "number") {
                container.width = options.width;
            }
            if (typeof options.verticalIncrement === "number") {
                verticalIncrement = options.verticalIncrement;
            }
            if (typeof options.horizontalIncrement === "number") {
                horizontalIncrement = options.horizontalIncrement;
            }
            if (typeof options.animateOptions === "object") {
                animateOptions = options.animateOptions;

                if (animateOptions.complete) {
                    callback = animateOptions.complete;
                }
            }
        }

        $(document)
            .on('click', '.scroller-button', function (event) {
                event.preventDefault();
                var container;
                var animateProps;


                if (!$(this).is('.disabled')) {
                    if ($(this).is('.up')) {
                        container = $(this).nextAll('.scroller-container').first();
                        animateProps = { scrollTop: '-=' + verticalIncrement };


                        animateOptions.complete = function () {

                            $(container).nextAll('.scroller-button.down').first().removeClass('disabled');

                            if (container[0].scrollTop > 0) {
                                $(container).prevAll('.scroller-button.up').first().removeClass('disabled');
                            } else {
                                $(container).prevAll('.scroller-button.up').first().addClass('disabled');
                            }

                            if (callback) callback();
                        };

                    } else if ($(this).is('.down')) {
                        container = $(this).prevAll('.scroller-container').first();
                        //var scroll = container[0].scrollTop + verticalIncrement > container[0].clientHeight ? container[0].clientHeight : '+=' + verticalIncrement;


                        var scroll = '+=' + verticalIncrement;


                        animateProps = { scrollTop: scroll };

                        animateOptions.complete = function () {


                            //if (scroll >= container[0].clientHeight) { $(container).nextAll('.scroller-button.down').first().addClass('disabled'); }

                            if (container[0].scrollTop > container[0].clientHeight) {
                                $(container).nextAll('.scroller-button.down').first().addClass('disabled');
                            } else {
                                $(container).nextAll('.scroller-button.down').first().removeClass('disabled');
                            }

                            if (container[0].scrollTop > 0) {
                                $(container).prevAll('.scroller-button.up').first().removeClass('disabled');
                            } else {
                                $(container).prevAll('.scroller-button.up').first().addClass('disabled');
                            }

                            if (callback) callback();
                        };
                    } else if ($(this).is('.left')) {
                    } else if ($(this).is('.right')) {
                    }

                    $(container).animate(animateProps, animateOptions);
                }
            });

        return this.each(function () {
            var containerCSS = {};

            //set up vertical scrolling
            if (typeof container.height === "number") {
                containerCSS.height = container.height;

                var upButton = $('<div class="scroller-button up disabled"></div>');
                var downButton = $('<div class="scroller-button down"></div>');

                $(this).before(upButton).after(downButton);
            }

            //set up horizontal scrolling
            if (typeof container.width === "number") {
                containerCSS.width = container.width;
            }

            $(this)
                .addClass('scroller-container')
                .css(containerCSS);
        });
    };

    $.fn.outerHTML = function () {
        var $t = $(this);
        if ($t[0].outerHTML !== undefined) {
            return $t[0].outerHTML;
        } else {
            var content = $t.wrap('<div/>').parent().html();
            $t.unwrap();
            return content;
        }
    };

    $.fn.populateAggregateRatingData = function () {
        return this.each(function (i, val) {
            var container = $(val);
            if (!container.attr('itemscope')) {
                container.attr('itemscope', '');
            }

            if (!container.attr('itemtype')) {
                container.attr('itemtype', 'http://schema.org/Product');
            }

            if (!$('meta[itemprop="description"]', container).length) {
                container.prepend('<meta itemprop="description" content="' + signsSite.shopperApprovedModel.productDescription + '">');
            }

            if (!$('meta[itemprop="name"]', container).length) {
                container.prepend('<meta itemprop="name" content="' + signsSite.shopperApprovedModel.productName + '">');
            }

            var aggregateRatingContainer = $('.aggregateRating-container', container).first();

            if (aggregateRatingContainer) {
                if (!aggregateRatingContainer.attr('itemprop')) {
                    aggregateRatingContainer.attr('itemprop', 'aggregateRating');
                }

                if (!aggregateRatingContainer.attr('itemscope')) {
                    aggregateRatingContainer.attr('itemscope', '');
                }

                if (!aggregateRatingContainer.attr('itemtype')) {
                    aggregateRatingContainer.attr('itemtype', 'http://schema.org/AggregateRating');
                }

                if (!$('meta[itemprop="reviewCount"]', aggregateRatingContainer).length) {
                    aggregateRatingContainer.prepend('<meta itemprop="reviewCount" content="' + signsSite.shopperApprovedModel.aggregateRating.reviewCount + '">');
                }

                if (!$('meta[itemprop="bestRating"]', aggregateRatingContainer).length) {
                    aggregateRatingContainer.prepend('<meta itemprop="bestRating" content="5.0">');
                }

                if (!$('meta[itemprop="worstRating"]', aggregateRatingContainer).length) {
                    aggregateRatingContainer.prepend('<meta itemprop="worstRating" content="1.0">');
                }

                if (!$('meta[itemprop="ratingValue"]', aggregateRatingContainer).length) {
                    aggregateRatingContainer.prepend('<meta itemprop="ratingValue" content="' + signsSite.shopperApprovedModel.aggregateRating.ratingValue + '">');
                }
            }

        });
    };



    $.fn.createVideoGallery = function () {
        return this.each(function (i, val) {
            var ul = $(val);
            if (ul.is('ul,ol')) {
                var parent = ul.parent();
                var gallery = $('<ul class="signs-video-gallery">').appendTo(parent);

                $('li', ul).each(function (i, val) {
                    var li = $(val);
                    var attrArray = li.text().split('::');
                    var videoID = attrArray[0];
                    var title = attrArray[1];
                    var newLi = $('<li>');
                    var a = $('<a class="product-video fancybox.iframe" href="https://www.youtube.com/embed/' + videoID + '?autoplay=1"><img src="https://i.ytimg.com/vi/' + videoID + '/mqdefault.jpg" alt="" width="196" height="110"></a>');

                    if (title) {
                        a.append('<br /><span>' + title + '</span>');
                    }

                    a.append('<div class="play-video-overlay-196x110">');

                    newLi.append(a);

                    gallery.append(newLi);
                });

                $('a.product-video').fancybox({
                    'openEffect': 'elastic',
                    'closeEffect': 'elastic',
                    'openSpeed': 200,
                    'closeSpeed': 200,
                    helpers: {
                        overlay: {
                            locked: false
                        }
                    }
                });

                ul.remove();
            }
        });
    };

    $.fn.createImageGallery = function (options) {
        var width = 915,
            height = 390,
            resizeHost = 'https://images.signs.com/',
            app = this;

        if (options) {
            if (typeof options.height === "number") {
                height = options.height;
            }
            if (typeof options.width === "number") {
                width = options.width;
            }
            if (typeof options.resizeHost === "string") {
                resizeHost = options.resizeHost;
            }
        }

        this.slide = function (gallery, direction, rightNav, leftNav, imageCount) {
            var visimg = gallery.data('visimg');
            var neximg = direction == 'left' ? visimg + 1 : visimg - 1;
            if (visimg == imageCount) {
                neximg = 1;
            }

            var outbound = $('div.image-wrapper-' + visimg, gallery),
                inbound = $('div.image-wrapper-' + neximg, gallery);


            signsSite.slideReplace(outbound, inbound, {
                direction: direction,
                callback: function (processData) {
                    gallery.data('visimg', neximg);
                    // if (neximg === imageCount) {
                    //  rightNav.hide();
                    //  } else {
                    rightNav.show();
                    // }

                    if (neximg > 1) {
                        leftNav.show();
                    } else {
                        leftNav.hide();
                    }
                }
            });
        };

        return this.each(function (i, val) {
            var ul = $(val);
            if (ul.is('ul,ol')) {
                var parent = ul.parent();
                var gallery = $('<div class="signs-image-gallery">')/*.css('width', width)*/.css('height', height).data('visimg', 1).appendTo(parent);
                var innerWrapper = $('<div class="images-container">').appendTo(gallery);
                var leftNav = $('<div class="circled-chevron gallery-nav left hidden">').appendTo(gallery);
                var rightNav = $('<div class="circled-chevron gallery-nav right hidden">').appendTo(gallery);

                var imageCount = 0;

                $('li', ul).each(function (i, val) {
                    var li = $(val);
                    var attrArray = li.text().split('::');

                    if (attrArray.length > 0) {
                        var img = $('<img>');

                        attrArray.forEach(function (entry) {
                            var attr = entry.split('=');

                            if (attr[0] === 'src') {
                                //this should be removed if we are not using SSL!!
                                attr[1] = attr[1].replace('http://www.signs.com', 'https://www.signs.com');
                            }

                            img.attr(attr[0], attr[1]);
                        });

                        var src = img.attr('src');

                        if (src.length > 0) {
                            imageCount++;
                            var imageWrapper = $('<div class="image-wrapper image-wrapper-' + imageCount + '">').appendTo(innerWrapper);
                            var vertAlign = $('<div class="vertical-align">').appendTo(imageWrapper);
                            //img.attr('src', resizeHost + 'Resizinator/m/' + width + '/' + height + '/?src=' + src),
                            img.attr('src', src),
                                img.appendTo(vertAlign);

                            var title = img.attr('title');
                            var alt = img.attr('alt');

                            if (!alt) {
                                alt = title ? title : 'Image ' + imageCount;
                                img.attr('alt', alt);
                            }

                            if (title) {
                                var titleSpan = $('<div class="image-title">').html(title).appendTo(vertAlign);
                            }

                            if (imageCount > 1) { //can't just use the index because some images may be skipped
                                imageWrapper.hide();
                            }
                        }
                    }
                });

                if (imageCount > 1) {
                    rightNav.show();

                    rightNav.on('click', function (event) {
                        app.slide(gallery, 'left', rightNav, leftNav, imageCount);
                    });

                    leftNav.on('click', function (event) {
                        app.slide(gallery, 'right', rightNav, leftNav, imageCount);
                    });
                }

                ul.remove();
            }
        });
    };

    $.fn.createFAQ = function (options) {
        var app = this;

        this.click = {
            action: function (event) {
                var li = $(event.currentTarget);
                li.isOpen = li.is('.open');

                if (li.isOpen) {
                    li.removeClass('open');
                    li.next().hide(app.click.animateSpeed, function () {
                        app.click.checkCallback(event, li);
                    });
                } else {
                    li.addClass('open');
                    li.next().show(app.click.animateSpeed, function () {
                        app.click.checkCallback(event, li);
                    });
                }
            },
            callback: null,
            checkCallback: function (event, li) {
                if (app.click.callback) {
                    app.click.callback({
                        event: event,
                        ui: {
                            li: li,
                            action: li.isOpen ? 'close' : 'open'
                        }
                    });
                }
            },
            animateSpeed: 200
        };

        this.create = {
            action: function (i, val) {
                var ul = $(val);
                if (ul.is('ul,ol')) {
                    $('li', ul).each(function (i, val) {
                        var li = $(val);
                        var isAnswer = i % 2 > 0;

                        $(li).addClass(isAnswer ? 'signs-faq-a hidden' : 'signs-faq-q');
                    })
                        .on('click', function (event) {
                            app.click.action(event);
                        });

                    ul.removeClass('signs-faq-seed').addClass('signs-faq');

                    app.create.checkCallback(i, val);
                }
            },
            callback: null,
            checkCallback: function (i, val, ul) {
                if (app.create.callback) {
                    app.create.callback(i, val, ul);
                }
            }
        };

        if (options) {
            if (typeof options.clickCallback === 'function') {
                app.click.callback = options.clickCallback;
            }
            if (typeof options.createCallback === 'function') {
                app.create.callback = options.createCallback;
            }
            if (typeof options.animateSpeed === 'number') {
                app.click.animateSpeed = options.animateSpeed;
            }
        }

        return this.each(function (i, val) {
            app.create.action(i, val);
        });
    };
}(jQuery));

//#endregion jQuery Plugins

//#region Classes
function MaterialTypeDimensions(minDimension1, maxDimension1, minDimension2, maxDimension2, defaultWidth, defaultHeight, name) {
    this.materialName = name;
    this.minDimension1 = minDimension1;
    this.maxDimension1 = maxDimension1;
    this.minDimension2 = minDimension2;
    this.maxDimension2 = maxDimension2;
    this.defaultWidth = defaultWidth;
    this.defaultHeight = defaultHeight;
}

function copyToClipboard(element) {
    //var $temp = $("<input>");
    //$("body").append($temp);
    //$temp.val($(element).val()).select();
    element.select();
    document.execCommand("copy");
    // $temp.remove();
}

function buildQuantityDdl(calculatePrice) {
    if ($('.product-quick-start-form-container').length > 0) {
        var form = $('.product-quick-start-form-container')[0];
        //var value = elem.value;
        var configIds;
        var productID = form.className.match(/\d+/)[0];

        var configIds = [];
        $('option:selected', '.optionConfigurationDDL').each(function (i, o) {
            configIds.push($(o).val());
        });


        $.ajax({
            url: '/design/GetSignsOptionQuantitySet_QuickStart',
            type: 'POST',
            async: false,
            data: {
                productID: productID,
                selectedConfigurationIDs: configIds
            },
            success: function (data) {
                if (data.hasSetQuantity == true) {
                    minSetQuantity = data.setQuantities[0].Quantity;
                    setQuantity = data.setQuantities;

                    if ($('input[name="Quantity"]', form).first().val() < minSetQuantity) {

                        var revertVal = globalSource.val();
                        var changeQuantity = confirm("The minimum order quantity for the option you selected is " + minSetQuantity + "; your order quantity will be updated to " + minSetQuantity + ".  Apply these changes?");
                        if (!changeQuantity) {
                            value = revertVal;
                        }
                        else {
                            //Update data attribute so that price will be just price and not price each
                            $(form).data('hassetquantity', 'True');

                            var ddl3 = $('#app-homeStandardQuantity');
                            var dims = $('.default', ddl3).first();
                            var dispStr = '';
                            var valStr = '';

                            ddl3.empty();

                            $.each(setQuantity, function () {
                                valStr = this.Quantity;

                                dispStr = this.Quantity;

                                if ((this.IsDefaultQuantityForMaterial != null && this.IsDefaultQuantityForMaterial == true) || $('input[name="Quantity"]', form).first().val() == this.Quantity) {
                                    ddl3.append('<option value="' + valStr + '" class="default" selected="selected">' + dispStr + '</option>');
                                    quantity = valStr;
                                }
                                else {
                                    ddl3.append('<option value="' + valStr + '">' + dispStr + '</option>');
                                }
                            });

                            ddl3.createDropDown();
                            $(".sizeInput.qty").hide();
                            ddl3.show;
                            $(".product-dropdown-container.Quantity").show();
                        }
                    }
                    else {
                        $(form).data('hassetquantity', 'True');

                        var ddl3 = $('#app-homeStandardQuantity');
                        var dims = $('.default', ddl3).first();
                        var dispStr = '';
                        var valStr = '';

                        ddl3.empty();

                        $.each(setQuantity, function () {
                            valStr = this.Quantity;

                            dispStr = this.Quantity;

                            if ((this.IsDefaultQuantityForMaterial != null && this.IsDefaultQuantityForMaterial == true) || $('input[name="Quantity"]', form).first().val() == this.Quantity) {
                                ddl3.append('<option value="' + valStr + '" class="default" selected="selected">' + dispStr + '</option>');
                            }
                            else {
                                ddl3.append('<option value="' + valStr + '">' + dispStr + '</option>');
                            }
                        });

                        ddl3.createDropDown();
                        $(".sizeInput.qty").hide();
                        ddl3.show;
                        $(".product-dropdown-container.Quantity").show();
                    }
                }

                if (data.hasSetSizes == true) {

                    minSetSizeWidth = data.setSizes[0].InchesWidth;
                    minSetSizeInchesHeight = data.setSizes[0].InchesHeight;

                    defaultWidth = data.defaultWidth;
                    defaultHeight = data.defaultHeight;

                    setSize = data.setSizes;

                    var size = minSetSizeWidth + "x" + minSetSizeInchesHeight

                    var currentSelectedSize = $('#app-homeStandardSizes').val();

                    var isCurrentSizeInSetSizes = false;
                    $.each(setSize, function () {
                        var sizeText = this.InchesWidth + "x" + this.InchesHeight

                        if (sizeText == currentSelectedSize) {
                            isCurrentSizeInSetSizes = true;
                        }
                    })

                    if (!isCurrentSizeInSetSizes) {
                        var revertVal = globalSource.val();
                        var changeSize = confirm("The option you selected will change the size options. Apply these changes?");
                        if (!changeSize) {
                            value = revertVal;
                        }
                        else {
                            var ddl2 = $('#app-homeStandardSizes');

                            $(form).attr('data-hassetsize', true);
                            var w = defaultWidth;
                            var w1 = w;
                            var w2;
                            var h = defaultHeight;
                            var h1 = h;

                            ddl2.empty();

                            var dispStr = '';
                            var valStr = '';
                            var defaultAssign = false;
                            $.each(setSize, function () {
                                valStr = this.InchesWidth + 'x' + this.InchesHeight;
                                var w2 = this.InchesWidth;
                                var h2 = this.InchesHeight;

                                // set default size
                                if (valStr == $('#app-homeStandardSizes').val()) {
                                    defaultWidth = w = this.InchesWidth;
                                    defaultHeight = h = this.InchesHeight;
                                }

                                dispStr = w2 + 'in x ' + h2 + 'in';

                                if (this.FriendlyDescription !== '' && this.FriendlyDescription !== 'StdSize' && this.FriendlyDescription !== null)
                                    dispStr = this.FriendlyDescription;

                                // the innerHTML.length was 0 on the first option always
                                // only set default if height and width match
                                if ((w == w2 && h == h2)) {

                                    ddl2.append('<option value="' + valStr + '" class="default" selected="selected">' + dispStr + '</option>');
                                    $(form).attr('data-setWidth', w2);
                                    $(form).attr('data-setHeight', h2);
                                    defaultAssign = true;

                                }
                                else {
                                    ddl2.append('<option value="' + valStr + '">' + dispStr + '</option>');
                                }

                            })
                            //$('.dropdown.ddl-dynamic-app-standardSizes').hide();
                            var createdDDL = ddl2.createDropDown();

                            // If no set sizes match the default height/width, select the first by default
                            if (!defaultAssign) {
                                createdDDL.children(":first").addClass('default').attr("selected", "selected");
                                createdDDL.createDropDown();

                                //Set defaulth height and width to be passed into GetOptionInfo
                                if (defaultWidth === undefined || defaultHeight === undefined) {
                                    var dimensions = createdDDL.children(":first").val().split('x');
                                    defaultWidth = Number(dimensions[0]);
                                    defaultHeight = Number(dimensions[1]);
                                    $('input[name="Width"]', form).first().val(defaultWidth)
                                    $('input[name="Height"]', form).first().val(defaultHeight);
                                }
                            }

                            createdDDL.val(createdDDL.find('.default').val());

                            $(".sizeInput").hide();
                            ddl2.show;
                        }
                    }
                    else {
                        var ddl2 = $('#app-homeStandardSizes');

                        $(form).attr('data-hassetsize', true);
                        var w = defaultWidth;
                        var w1 = w;
                        var w2;
                        var h = defaultHeight;
                        var h1 = h;

                        var currentDisplayedSize = $('#app-homeStandardSizes').val();
                        ddl2.empty();

                        var dispStr = '';
                        var valStr = '';
                        var defaultAssign = false;
                        $.each(setSize, function () {
                            valStr = this.InchesWidth + 'x' + this.InchesHeight;
                            var w2 = this.InchesWidth;
                            var h2 = this.InchesHeight;

                            // set default size
                            if (valStr == currentDisplayedSize) {
                                defaultWidth = w = this.InchesWidth;
                                defaultHeight = h = this.InchesHeight;
                            }

                            dispStr = w2 + 'in x ' + h2 + 'in';

                            if (this.FriendlyDescription !== '' && this.FriendlyDescription !== 'StdSize' && this.FriendlyDescription !== null)
                                dispStr = this.FriendlyDescription;

                            // the innerHTML.length was 0 on the first option always
                            // only set default if height and width match
                            if ((w == w2 && h == h2)) {

                                ddl2.append('<option value="' + valStr + '" class="default" selected="selected">' + dispStr + '</option>');
                                $(form).attr('data-setWidth', w2);
                                $(form).attr('data-setHeight', h2);
                                defaultAssign = true;

                            }
                            else {
                                ddl2.append('<option value="' + valStr + '">' + dispStr + '</option>');
                            }

                        })
                        //$('.dropdown.ddl-dynamic-app-standardSizes').hide();
                        var createdDDL = ddl2.createDropDown();

                        // If no set sizes match the default height/width, select the first by default
                        if (!defaultAssign) {
                            createdDDL.children(":first").addClass('default').attr("selected", "selected");
                            createdDDL.createDropDown();

                            //Set defaulth height and width to be passed into GetOptionInfo
                            if (defaultWidth == 'undefined' || defaultHeight == 'undefined') {
                                var dimensions = createdDDL.children(":first").val().split('x');
                                defaultWidth = Number(dimensions[0]);
                                defaultHeight = Number(dimensions[1]);
                                $('input[name="Width"]', form).first().val(defaultWidth)
                                $('input[name="Height"]', form).first().val(defaultHeight);
                            }
                        }

                        createdDDL.val(createdDDL.find('.default').val());

                        // $(".sizeInput").hide();
                        ddl2.show;
                    }
                }

            },
            error: function (request, status, error) {
                signsSite.prompts.alert('error');
            }
        });

        if (calculatePrice) {
            var formData = [];
            $('option:selected', 'select.optionConfigurationDDL').each(function (i, o) {
                formData[i] = {};
                formData[i].DesignOptionConfigurationID = $(o).val();
                formData[i].Price = $(o).data('price');
                formData[i].PricePerSquareInch = $(o).data('pricepersquareinch');
                formData[i].PricingMultiplierType = $(o).data('pricingmultipliertype');
                formData[i].CurrentProductBindingID = $(o).data('currentProductBindingID');
                formData[i].PreDiscountBasePrice = $(o).data('preDiscountBasePrice');
            });
            var configIds = [];
            $('option:selected', '.optionConfigurationDDL').each(function (i, o) {
                configIds.push($(o).val());
            });
            var form = $('div.product-quick-start-form-container'),
                widthInput = $('input[name="Width"]', form).first(),
                heightInput = $('input[name="Height"]', form).first();
            width = widthInput.val();
            height = heightInput.val();
            quantity = $('input[name="Quantity"]', form).first().val();

            $.ajax({
                type: 'POST',
                url: '/design/GetOptionInfo/',
                data: {
                    productID: productID,
                    height: height,
                    width: width,
                    includeOptions: true,
                    quantity: quantity,
                    selectedConfigurationIDs: configIds
                },
                success: function (data) {

                    // Update the price once we have the options updated, needed here in case size requirement changed any options
                    var formData = [];
                    $('option:selected', '.optionConfigurationDDL').each(function (i, o) {
                        formData[i] = {};
                        formData[i].DesignOptionConfigurationID = $(o).val();
                        formData[i].Price = $(o).data('price');
                        formData[i].PricePerSquareInch = $(o).data('pricepersquareinch');
                        formData[i].PricingMultiplierType = $(o).data('pricingmultipliertype');
                        formData[i].CurrentProductBindingID = $(o).data('currentProductBindingID');
                        formData[i].PreDiscountBasePrice = $(o).data('preDiscountBasePrice');
                    });

                    if (data.hasSetQuantity == true) {
                        signsSite.productQuickStartFormManager.getDiscountedPrice(productID, quantity, height, width, formData, function (data) {
                            signsSite.productQuickStartFormManager.updateAllFormsForProduct(productID, data.unitPrice, quantity, height, width, true, data.slashedPrice, data.totalPercent, data.showSlashed);
                        });
                    }
                    else {
                        signsSite.productQuickStartFormManager.getDiscountedUnitPrice(productID, quantity, height, width, formData, function (data) {
                            signsSite.productQuickStartFormManager.updateAllFormsForProduct(productID, data.unitPrice, quantity, height, width, false, data.slashedPrice, data.totalPercent, data.showSlashed);
                        });
                    }

                },
                error: function (request, status, error) {
                    signsSite.prompts.alert('error');
                }
            });
        }
    }
}
function MaterialTypeDimensionsManager(displayUnit) {
    this.materialTypeDimensions = {
        addIfNew: function (materialTypeID, minDimension1, maxDimension1, minDimension2, maxDimension2, defaultWidth, defaultHeight, name) {
            if (!this.hasOwnProperty(materialTypeID)) {
                this[materialTypeID] = new MaterialTypeDimensions(minDimension1, maxDimension1, minDimension2, maxDimension2, defaultWidth, defaultHeight, name);
                this.length++;
            }
        },
        length: 0
    };
    this.displayUnit = typeof displayUnit === 'string' ? displayUnit : 'in';

    this.validateDimensions = function (widthInput, heightInput, materialTypeID, productName, priceCalc) {
        console.log("validateDimensions", productName);
        if (productName != "Vinyl Lettering" && productName != "Business Card" && productName != "Canvas Print" && productName != "Retractable Banner" && productName != "Door Hangers"
            && productName != "Stickers" && productName != "Brochures" && productName != "Table Tents") {
            if ($('#Width').val() == 3.5 || $('#Height').val() == 2) {
                $('#Width').val(48).data('inchesvalue', 48).data('revertvalue', 48);
                $('#Height').val(24).data('inchesvalue', 24).data('revertvalue', 24);
                if ($('#PriceSizeInputs').length !== 0) {
                    $("#priceDimensionError").show().delay(5000).fadeOut();
                }
            }
            //$('#Quantity').val(1);
        }
        var dimensions = this.materialTypeDimensions[materialTypeID],
            width = widthInput.val(),
            height = heightInput.val(),
            minDimsOK,
            maxDimsOK;
        //there are no size inputs for this product page so don't need to check the dimensions
        if (dimensions.materialName == "Cut Vinyl"
            || productName == "Business Card"
            || productName == "A-Frame Signs"
            || productName == "Retractable Banner"
            || productName == "Canvas Prints"
            || productName == "X-Stand Banner"
            || productName == "Feather Flag Banner"
            || productName == "Teardrop Flag Banner"
            || productName == "Postcard"
            || productName == "Step and Repeat Banner"
            || productName == "Metal Print"
            || productName == "Wood Photo Print"
            || productName == "Tension Fabric Display"
            || productName == "Pop Up Displays"
            || productName == "Table Throws"
            || productName == "Door Hangers"
            || productName == "Stickers"
            || productName == "Brochures"
        ) {
            minDimsOK = true;
            maxDimsOK = true;
        } else {
            if (height == width) {
                //only need to check one of the dimensions, since they are identical
                minDimsOK = (height >= dimensions.minDimension1 && height >= dimensions.minDimension2);
                maxDimsOK = (height <= dimensions.maxDimension1 && height <= dimensions.maxDimension2);
            } else {
                minDimsOK = (height >= dimensions.minDimension1 && width >= dimensions.minDimension2) || (height >= dimensions.minDimension2 && width >= dimensions.minDimension1);
                maxDimsOK = (height <= dimensions.maxDimension1 && width <= dimensions.maxDimension2) || (height <= dimensions.maxDimension2 && width <= dimensions.maxDimension1);
            }

            //if(!priceCalc){
            // if (!minDimsOK || !maxDimsOK) {
            // if (!minDimsOK) {
            //if (this.displayUnit === 'in') {
            // signsSite.prompts.alert("We're sorry.\r\nYour " + productName + " has to be at least " + dimensions.minDimension1 + " inches by " + dimensions.minDimension2 + " inches.");
            // } else {
            // signsSite.prompts.alert("We're sorry.\r\nYour " + productName + " has to be at least " + signsSite.getRoundedFeet(dimensions.minDimension1) + " feet by " + signsSite.getRoundedFeet(dimensions.minDimension2) + " feet.");
            // }
            // }

            //if (!maxDimsOK) {
            // if (this.displayUnit === 'in') {
            //    signsSite.prompts.alert("We're sorry.\r\nYour " + productName + " cannot be larger than " + dimensions.maxDimension1 + " inches by " + dimensions.maxDimension2 + " inches.");
            //  } else {
            //     signsSite.prompts.alert("We're sorry.\r\nYour " + productName + " cannot be larger than " + signsSite.getRoundedFeet(dimensions.maxDimension1) + " feet by " + signsSite.getRoundedFeet(dimensions.maxDimension2) + " feet.");
            // }
            //   }
            // } else {
            ////store current values hidden so we can restore them if they muck them up again
            //    $(widthInput).data('revertvalue', width);
            //    $(heightInput).data('revertvalue', height);
            // }
            //}
        }

        return minDimsOK && maxDimsOK;
    };

    this.revertSizes = function (widthInput, heightInput, keyupBool, optionWidth, optionHeight, defaultAssign) {

        var revertw = $(widthInput).data('revertvalue'),
            reverth = $(heightInput).data('revertvalue');

        if (defaultAssign) {
            revertw = optionWidth;
            reverth = optionHeight;
        }

        var w = this.displayUnit === 'in' ? revertw : signsSite.getRoundedFeet(revertw),
            h = this.displayUnit === 'in' ? reverth : signsSite.getRoundedFeet(reverth);

        $(widthInput).val(w).data('inchesvalue', revertw);
        $(heightInput).val(h).data('inchesvalue', reverth);
        if ($('.product-quick-start-form-container').length !== 0 && keyupBool) {
            $(".QSDimensionError").dialog({
                buttons: [
                    {
                        text: "Ok",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            }).dialog('open');
        }
    };

}

var spinnerTimer = null;
function showSpinner(options) {
    var spinner = $('#in-progress-overlay');
    var spinnerTextContainer = $('#in-progress-text');
    var delay = 500;
    var timeOut = 30000;
    var text = '';
    var textStyle = "text-align:center;color:White;font-size:20px;width:100%;"

    var textOptions = {
        container: spinner,
        duration: 0,
        offsetTop: -50,
        alignX: false
    };

    if (options) {
        if (options.delay && typeof options.delay == 'number') {
            delay = options.delay > 0 ? options.delay : 1; //delays of less than 1 default to 500ms in jquery so we need to prevent this when setting to 0 or less.
        }
        if (options.timeOut && typeof options.timeOut == 'number') timeOut = options.timeOut;
        if (options.text) text = options.text;
        if (options.textStyle) textStyle = options.textStyle;
        if (options.textOffsetTop) textOptions.offsetTop = options.textOffsetTop;
        if (options.textOffsetLeft) textOptions.offsetLeft = options.textOffsetLeft;
    }

    spinnerTextContainer.attr('style', textStyle).html(text);

    spinner.delay(delay).queue(function () {
        if (options && options.container) {
            options.duration = 0;
            spinner.outerHeight($(options.container).outerHeight());
            spinner.outerWidth($(options.container).outerWidth());
            spinner.align(options);
        }

        $(spinner).show().dequeue();
        spinnerTextContainer.align(textOptions);

        if (timeOut > -1) {
            spinnerTimer = setTimeout(function () {
                $(spinner).hide();
            }, timeOut);
        }

    });
}

function hideSpinner() {
    $('#in-progress-overlay').dequeue().hide();
    clearTimeout(spinnerTimer);
}
function isGuid(value) {
    var regex = /[a-f0-9]{8}(?:-[a-f0-9]{4}){3}-[a-f0-9]{12}/i;
    var match = regex.exec(value);
    return match != null;
}

(function ($) { // Begin jQuery
    $(function () { // DOM ready
        $('#nav-item-products').click(
            function () {
                $('.product-menu').toggleClass('active');
                $('.designToolOuter').toggleClass('active');
                $('#editor').toggleClass('active');
                $('#toolbar').toggleClass('active');
                $('#nav-item-products').toggleClass('active');
            }
        );


        //Add Class when hover from top navigation right
        $('#nav-items-container>li>p:not(:only-child)').on('mouseenter', function () {
            $(this).parent('li').addClass('active');
            $(this).siblings('.nav-child').addClass('active');
        });

        $('#nav-items-container>li').on('mouseleave', function () {
            $(this).removeClass('active');
            $(this).find('.nav-child').removeClass('active');

        });
        //Disable link if #
        $('.product-menu>ul>li>a:not(:only-child)').click(function (e) {
            if ($(this).attr('href') == "#") {
                e.preventDefault();
                e.stopPropagation();
            }

        });
        //Check if Touchable Device
        if ("ontouchstart" in document.documentElement) {
            $(".product-menu>ul>li>a:not(:only-child)").click(function (e) {
                //If parent does not have an active class disable the link
                if (!$(this).parent().hasClass('doubleClick')) {
                    $(".product-menu li").not($(this).parent()).removeClass('doubleClick');
                    $(this).parent().addClass('doubleClick');
                    e.preventDefault();
                    e.stopPropagation();
                } else {
                    return true;
                }
            });
        }
        else {
            // If a link has a dropdown, add sub menu toggle.
            $(".product-menu>ul>li>a:not(:only-child)").parent().on('mouseenter', function () {
                $(this).addClass('active');

            });

            $(".product-menu>ul>li>a:not(:only-child)").parent().on('mouseleave', function () {
                $(this).removeClass('active');
            });
        }


        // Clicking away from dropdown will remove the dropdown class
        $('html').click(function () {
            $('.product-menu>ul>li').removeClass("active");
            $(".product-menu li").removeClass('doubleClick');
            //$('.product-menu-dropdown').hide();

        });
        // Toggle open and close nav styles on click
        $('#nav-toggle').click(function () {
            $('nav ul').slideToggle();
            $('nav ul').slideToggle();
        });
        // Hamburger to X toggle
        $('#nav-toggle').on('click', function () {
            this.classList.toggle('active');
        });

        //Navigation Preview
        $(".dropdown-menu-link").on('mouseenter', function (event) {
            var activeHeader = ($(this).find('h3').length > 0) ? $(this).find('h3').html() : $(this).html();
            var activeImg = $(this).data('image');
            var activeDesc = $(this).siblings(".dropdown-menu-link-description").text();
            $(this).closest('.product-menu-dropdown').find(".dropdown-link-preview img").attr("src", activeImg);
            $(this).closest('.product-menu-dropdown').find(".dropdown-link-preview h2").html(activeHeader);
            $(this).closest('.product-menu-dropdown').find(".dropdown-link-preview p").text(activeDesc);
        });

        $(".product-menu-dropdown").on('mouseleave', function (event) {
            var activeHeader = $(this).children('.most-popular').find('ul>li:first-child>a>h3').text(); console.log(activeHeader);
            var activeImg = $(this).children('.most-popular').find('ul>li:first-child>a').data('image');
            var activeDesc = $(this).children('.most-popular').find('ul>li:first-child>a+p').text();
            $(this).find(".dropdown-link-preview img").attr("src", activeImg);
            $(this).find(".dropdown-link-preview h2").text(activeHeader);
            $(this).find(".dropdown-link-preview p").text(activeDesc);

        });

        // Window Decals 101
        $('.gallery-body-left .gallery-thumbnail').on('click', function () {
            $index = $(this).index() + 1;
            $(".gallery-body-right .content:nth-child(" + $index + ")").siblings().removeClass('galleryShow');
            $(".gallery-body-right .content:nth-child(" + $index + ")").addClass('galleryShow');
        });


        // copy the intl-tel-input's placeholder format to the visible input
        function initMasking(formatterInput, maskedInput) {
            var format = $(formatterInput).attr('placeholder');
            if (format !== undefined) {
                $(maskedInput).attr('placeholder', format);
                $(maskedInput).mask(format.replace(/[1-9]/g, 0));
            }
        }

        // initialize intl-tel-input
        $(".phoneFormatter").intlTelInput({
            initialCountry: "us",
            autoPlaceholder: "aggressive",
            geoIpLookup: function (callback) {
                $.get('https://ipinfo.io', function () { }, "jsonp").always(function (resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            }
        });

        // initialize the mask
        initMasking(".phoneFormatter", "#PhoneNumber, #ShippingPhoneNumber");

        // update the mask format when changing country
        $(".phoneFormatter").on("countrychange", function (e, countryData) {
            $(this).val('');
            $("#PhoneNumber, #ShippingPhoneNumber").val('');

            // update the other intl-tel-input's country
            var country = $(this).intlTelInput("getSelectedCountryData").iso2;
            $(".phoneFormatter").not(this).intlTelInput("setCountry", country);

            // update the mask
            initMasking(this, "#PhoneNumber, #ShippingPhoneNumber");
        });

        // Read a page's GET URL variables and return them as an associative array.
        $.extend({
            getUrlVars: function () {
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for (var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                return vars;
            },
            getUrlVar: function (name) {
                return $.getUrlVars()[name];
            }
        });

        // Get object of URL parameters
        var allVars = $.getUrlVars();

        if (allVars['signin']) {
            $("#headerLoginLink").trigger('click');
        }
        else if (allVars['signup']) {
            $("#createAnAccountLink").trigger('click');
        }

        var prodQueryString= window.location.search,
            prodOrigUrl = $('#mobileFreeDesignHelp').attr('href');
        prodNewUrl = prodOrigUrl + prodQueryString;
        $('#mobileFreeDesignHelp').attr('href', prodNewUrl);

        var prodID = allVars['productID'],
            prodWidth = allVars['width'],
            prodHeight = allVars['height'];


        $('#SignType').val(prodID);
        $('#SignWidth').val(prodWidth);
        $('#SignHeight').val(prodHeight);


        //Check If Standard Size Has More than one option if Yes Show it
        var standardSizes = $("#app-designStandardSizes > option").length;
        if (standardSizes > 1) {

            $("#app-designStandardSizes").parent(".product-dropdown-container").css({ "display": "block", "margin": "10px 0" });
            $(".ddl-dynamic-app-designStandardSizes > dd > ul").css({ "width": "100%" });
            var prodWnH = prodWidth + "x" + prodHeight;
            $("#app-designStandardSizes").val(prodWnH);
        };

        //Change Dropdown value to be same with the select Value
        var standardSize = $("#app-designStandardSizes> option:selected").text();
        var standardSizeDropdownOption = $(".ddl-dynamic-app-designStandardSizes>dd > ul>li");
        standardSizeDropdownOption.each(function (i, item) {
            x = $(item).text()
            if (standardSize === x) {
                $(this).trigger("click");
            }
        });

        $('.tools-Sign-Options').each(function () {
            var optionName = $(this).find('.optionName').text();
            optionName = optionName.replace(/\s/g, "_").toLowerCase();
            $(this).find('.optionConfigurationDDL').val(allVars[optionName]);
        });

        window.getOptionsValue = function() {
            var tempOptions = '';
            $('.tools-Sign-Options').each(function () {
                var optionName = $(this).find('.optionName').text();
                optionName = optionName.replace(/\s/g, "_").toLowerCase();

                var optionValue = $(this).find('.optionConfigurationDDL').val();

                tempOptions += "&" + optionName + "=" + optionValue;

            });

            return tempOptions;
        }


        $('.optionConfigurationDDL').on('change', function () {
            options = getOptionsValue();
        });

    }); // end DOM ready
})(jQuery); // end jQuery

